﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;

namespace Launcher
{
    class Program
    {
        static void Main(string[] args)
        {
            var startInfo = new ProcessStartInfo();
            startInfo.FileName = "WildBot.exe";

            if (Environment.Is64BitOperatingSystem)
            {
                System.Console.WriteLine("x64 system detected, running Wild Bot x64.");
                startInfo.WorkingDirectory = Application.StartupPath + @"\x64";
                Process.Start(startInfo);
            }

            else
            {
                System.Console.WriteLine("x86 system detected, running Wild Bot x86.");
                startInfo.WorkingDirectory = Application.StartupPath + @"\x86";
                Process.Start(startInfo);
            }
        }
    }
}
