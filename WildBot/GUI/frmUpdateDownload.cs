﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WildBot.GUI
{
    public partial class frmUpdateDownload : Form
    {
        public frmUpdateDownload()
        {
            InitializeComponent();
        }

        private void frmUpdateDownload_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
                DialogResult = System.Windows.Forms.DialogResult.Abort;
        }
    }
}
