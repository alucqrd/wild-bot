﻿namespace WildBot.GUI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new System.Windows.Forms.Button();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpStatus = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtWorldID = new System.Windows.Forms.TextBox();
            this.txtFacing = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtPosition = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.apbHP = new WildBot.GUI.AdvProgressBar();
            this.txtLevel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tpSettings = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnEditGrindingSpot = new System.Windows.Forms.Button();
            this.cGrindingSpot = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnCombatSettings = new System.Windows.Forms.Button();
            this.cCombatProfile = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkLineFacing = new System.Windows.Forms.CheckBox();
            this.chkAutoStart = new System.Windows.Forms.CheckBox();
            this.chkAutoFace = new System.Windows.Forms.CheckBox();
            this.chkBackgroundMode = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtMoveToTargetDistance = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFacingAngleTolerance = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAttackRange = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtNameFilters = new System.Windows.Forms.TextBox();
            this.chkWhitelistingMode = new System.Windows.Forms.CheckBox();
            this.tpKeys = new System.Windows.Forms.TabPage();
            this.olvSpells = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.tpModules = new System.Windows.Forms.TabPage();
            this.olvModules = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.worldEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.developerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entityListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tUpdate = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.cmModules = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmModules_GUIs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.enableAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tcMain.SuspendLayout();
            this.tpStatus.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tpSettings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tpKeys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvSpells)).BeginInit();
            this.tpModules.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvModules)).BeginInit();
            this.msMain.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.cmModules.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.Location = new System.Drawing.Point(313, 545);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start bot";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tcMain
            // 
            this.tcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcMain.Controls.Add(this.tpStatus);
            this.tcMain.Controls.Add(this.tpSettings);
            this.tcMain.Controls.Add(this.tpKeys);
            this.tcMain.Controls.Add(this.tpModules);
            this.tcMain.Location = new System.Drawing.Point(0, 27);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(392, 517);
            this.tcMain.TabIndex = 1;
            // 
            // tpStatus
            // 
            this.tpStatus.BackColor = System.Drawing.SystemColors.Control;
            this.tpStatus.Controls.Add(this.groupBox7);
            this.tpStatus.Controls.Add(this.groupBox1);
            this.tpStatus.Location = new System.Drawing.Point(4, 22);
            this.tpStatus.Name = "tpStatus";
            this.tpStatus.Padding = new System.Windows.Forms.Padding(3);
            this.tpStatus.Size = new System.Drawing.Size(384, 491);
            this.tpStatus.TabIndex = 0;
            this.tpStatus.Text = "Status";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.txtLog);
            this.groupBox7.Location = new System.Drawing.Point(6, 141);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(370, 344);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Log";
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtLog.Location = new System.Drawing.Point(6, 19);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(358, 319);
            this.txtLog.TabIndex = 0;
            this.txtLog.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.splitContainer1);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.txtPosition);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.apbHP);
            this.groupBox1.Controls.Add(this.txtLevel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 129);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Character";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(104, 97);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtWorldID);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txtFacing);
            this.splitContainer1.Size = new System.Drawing.Size(258, 26);
            this.splitContainer1.SplitterDistance = 127;
            this.splitContainer1.TabIndex = 11;
            // 
            // txtWorldID
            // 
            this.txtWorldID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWorldID.Location = new System.Drawing.Point(3, 3);
            this.txtWorldID.Name = "txtWorldID";
            this.txtWorldID.ReadOnly = true;
            this.txtWorldID.Size = new System.Drawing.Size(121, 20);
            this.txtWorldID.TabIndex = 9;
            this.txtWorldID.Text = "0";
            this.txtWorldID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtFacing
            // 
            this.txtFacing.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFacing.Location = new System.Drawing.Point(3, 3);
            this.txtFacing.Name = "txtFacing";
            this.txtFacing.ReadOnly = true;
            this.txtFacing.Size = new System.Drawing.Size(121, 20);
            this.txtFacing.TabIndex = 7;
            this.txtFacing.Text = "0";
            this.txtFacing.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "WorldID / Facing:";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Location = new System.Drawing.Point(104, 19);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(209, 20);
            this.txtName.TabIndex = 1;
            this.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPosition
            // 
            this.txtPosition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPosition.Location = new System.Drawing.Point(104, 71);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.ReadOnly = true;
            this.txtPosition.Size = new System.Drawing.Size(258, 20);
            this.txtPosition.TabIndex = 6;
            this.txtPosition.Text = "0, 0, 0";
            this.txtPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Pos / Facing:";
            // 
            // apbHP
            // 
            this.apbHP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.apbHP.Background = System.Drawing.Color.WhiteSmoke;
            this.apbHP.Display = "{1} / {2} ({3}%)";
            this.apbHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.apbHP.Foreground = System.Drawing.Color.Red;
            this.apbHP.Location = new System.Drawing.Point(104, 45);
            this.apbHP.Maximum = ((long)(0));
            this.apbHP.Minimum = ((long)(0));
            this.apbHP.Name = "apbHP";
            this.apbHP.Size = new System.Drawing.Size(258, 20);
            this.apbHP.TabIndex = 4;
            this.apbHP.Value = ((long)(0));
            // 
            // txtLevel
            // 
            this.txtLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLevel.Location = new System.Drawing.Point(319, 19);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(43, 20);
            this.txtLevel.TabIndex = 2;
            this.txtLevel.Text = "0";
            this.txtLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Health:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name / Level:";
            // 
            // tpSettings
            // 
            this.tpSettings.BackColor = System.Drawing.SystemColors.Control;
            this.tpSettings.Controls.Add(this.groupBox2);
            this.tpSettings.Controls.Add(this.groupBox4);
            this.tpSettings.Controls.Add(this.groupBox5);
            this.tpSettings.Controls.Add(this.groupBox3);
            this.tpSettings.Location = new System.Drawing.Point(4, 22);
            this.tpSettings.Name = "tpSettings";
            this.tpSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tpSettings.Size = new System.Drawing.Size(384, 491);
            this.tpSettings.TabIndex = 1;
            this.tpSettings.Text = "Settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnEditGrindingSpot);
            this.groupBox2.Controls.Add(this.cGrindingSpot);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.btnCombatSettings);
            this.groupBox2.Controls.Add(this.cCombatProfile);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 75);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Profile Settings";
            // 
            // btnEditGrindingSpot
            // 
            this.btnEditGrindingSpot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditGrindingSpot.Location = new System.Drawing.Point(301, 45);
            this.btnEditGrindingSpot.Name = "btnEditGrindingSpot";
            this.btnEditGrindingSpot.Size = new System.Drawing.Size(61, 23);
            this.btnEditGrindingSpot.TabIndex = 5;
            this.btnEditGrindingSpot.Text = "Edit";
            this.btnEditGrindingSpot.UseVisualStyleBackColor = true;
            this.btnEditGrindingSpot.Click += new System.EventHandler(this.btnEditGrindingSpot_Click);
            // 
            // cGrindingSpot
            // 
            this.cGrindingSpot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cGrindingSpot.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cGrindingSpot.FormattingEnabled = true;
            this.cGrindingSpot.Items.AddRange(new object[] {
            "None"});
            this.cGrindingSpot.Location = new System.Drawing.Point(89, 46);
            this.cGrindingSpot.Name = "cGrindingSpot";
            this.cGrindingSpot.Size = new System.Drawing.Size(206, 21);
            this.cGrindingSpot.TabIndex = 4;
            this.cGrindingSpot.DropDown += new System.EventHandler(this.cGrindingSpot_DropDown);
            this.cGrindingSpot.SelectedIndexChanged += new System.EventHandler(this.cGrindingSpot_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Grinding spot:";
            // 
            // btnCombatSettings
            // 
            this.btnCombatSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCombatSettings.Location = new System.Drawing.Point(301, 18);
            this.btnCombatSettings.Name = "btnCombatSettings";
            this.btnCombatSettings.Size = new System.Drawing.Size(61, 23);
            this.btnCombatSettings.TabIndex = 2;
            this.btnCombatSettings.Text = "Config";
            this.btnCombatSettings.UseVisualStyleBackColor = true;
            this.btnCombatSettings.Click += new System.EventHandler(this.btnCombatSettings_Click);
            // 
            // cCombatProfile
            // 
            this.cCombatProfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cCombatProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cCombatProfile.FormattingEnabled = true;
            this.cCombatProfile.Items.AddRange(new object[] {
            "None"});
            this.cCombatProfile.Location = new System.Drawing.Point(89, 19);
            this.cCombatProfile.Name = "cCombatProfile";
            this.cCombatProfile.Size = new System.Drawing.Size(206, 21);
            this.cCombatProfile.TabIndex = 1;
            this.cCombatProfile.DropDown += new System.EventHandler(this.cCombatProfile_DropDown);
            this.cCombatProfile.SelectedIndexChanged += new System.EventHandler(this.cCombatProfile_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Combat profile:";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.chkLineFacing);
            this.groupBox4.Controls.Add(this.chkAutoStart);
            this.groupBox4.Controls.Add(this.chkAutoFace);
            this.groupBox4.Controls.Add(this.chkBackgroundMode);
            this.groupBox4.Location = new System.Drawing.Point(4, 87);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(372, 109);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Global settings";
            // 
            // chkLineFacing
            // 
            this.chkLineFacing.AutoSize = true;
            this.chkLineFacing.Location = new System.Drawing.Point(6, 88);
            this.chkLineFacing.Name = "chkLineFacing";
            this.chkLineFacing.Size = new System.Drawing.Size(262, 17);
            this.chkLineFacing.TabIndex = 3;
            this.chkLineFacing.Text = "Use line for facing instead of a triangle in the radar";
            this.chkLineFacing.UseVisualStyleBackColor = true;
            this.chkLineFacing.CheckedChanged += new System.EventHandler(this.chkLineFacing_CheckedChanged);
            // 
            // chkAutoStart
            // 
            this.chkAutoStart.AutoSize = true;
            this.chkAutoStart.Location = new System.Drawing.Point(6, 65);
            this.chkAutoStart.Name = "chkAutoStart";
            this.chkAutoStart.Size = new System.Drawing.Size(222, 17);
            this.chkAutoStart.TabIndex = 2;
            this.chkAutoStart.Text = "Automatically start the bot when logged in";
            this.chkAutoStart.UseVisualStyleBackColor = true;
            // 
            // chkAutoFace
            // 
            this.chkAutoFace.AutoSize = true;
            this.chkAutoFace.Checked = true;
            this.chkAutoFace.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoFace.Location = new System.Drawing.Point(6, 19);
            this.chkAutoFace.Name = "chkAutoFace";
            this.chkAutoFace.Size = new System.Drawing.Size(181, 17);
            this.chkAutoFace.TabIndex = 0;
            this.chkAutoFace.Text = "Use Use game\'s auto face target";
            this.chkAutoFace.UseVisualStyleBackColor = true;
            // 
            // chkBackgroundMode
            // 
            this.chkBackgroundMode.AutoSize = true;
            this.chkBackgroundMode.Location = new System.Drawing.Point(6, 42);
            this.chkBackgroundMode.Name = "chkBackgroundMode";
            this.chkBackgroundMode.Size = new System.Drawing.Size(114, 17);
            this.chkBackgroundMode.TabIndex = 1;
            this.chkBackgroundMode.Text = "Background Mode";
            this.chkBackgroundMode.UseVisualStyleBackColor = true;
            this.chkBackgroundMode.CheckedChanged += new System.EventHandler(this.chkBackgroundMode_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.txtMoveToTargetDistance);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.txtFacingAngleTolerance);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.txtAttackRange);
            this.groupBox5.Location = new System.Drawing.Point(8, 202);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(368, 100);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Selection settings";
            // 
            // txtMoveToTargetDistance
            // 
            this.txtMoveToTargetDistance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMoveToTargetDistance.Location = new System.Drawing.Point(142, 71);
            this.txtMoveToTargetDistance.Name = "txtMoveToTargetDistance";
            this.txtMoveToTargetDistance.Size = new System.Drawing.Size(220, 20);
            this.txtMoveToTargetDistance.TabIndex = 5;
            this.txtMoveToTargetDistance.Text = "30";
            this.txtMoveToTargetDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Move to target distance:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Facing angle +- tolerance:";
            // 
            // txtFacingAngleTolerance
            // 
            this.txtFacingAngleTolerance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFacingAngleTolerance.Location = new System.Drawing.Point(142, 45);
            this.txtFacingAngleTolerance.Name = "txtFacingAngleTolerance";
            this.txtFacingAngleTolerance.Size = new System.Drawing.Size(220, 20);
            this.txtFacingAngleTolerance.TabIndex = 3;
            this.txtFacingAngleTolerance.Text = "0.20";
            this.txtFacingAngleTolerance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Attack Range:";
            // 
            // txtAttackRange
            // 
            this.txtAttackRange.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAttackRange.Location = new System.Drawing.Point(89, 19);
            this.txtAttackRange.Name = "txtAttackRange";
            this.txtAttackRange.Size = new System.Drawing.Size(273, 20);
            this.txtAttackRange.TabIndex = 1;
            this.txtAttackRange.Text = "20";
            this.txtAttackRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.txtNameFilters);
            this.groupBox3.Controls.Add(this.chkWhitelistingMode);
            this.groupBox3.Location = new System.Drawing.Point(8, 308);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(368, 177);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Do not attack these monsters";
            // 
            // txtNameFilters
            // 
            this.txtNameFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNameFilters.Location = new System.Drawing.Point(6, 42);
            this.txtNameFilters.Multiline = true;
            this.txtNameFilters.Name = "txtNameFilters";
            this.txtNameFilters.Size = new System.Drawing.Size(356, 129);
            this.txtNameFilters.TabIndex = 1;
            // 
            // chkWhitelistingMode
            // 
            this.chkWhitelistingMode.AutoSize = true;
            this.chkWhitelistingMode.Location = new System.Drawing.Point(6, 19);
            this.chkWhitelistingMode.Name = "chkWhitelistingMode";
            this.chkWhitelistingMode.Size = new System.Drawing.Size(109, 17);
            this.chkWhitelistingMode.TabIndex = 0;
            this.chkWhitelistingMode.Text = "Whitelisting mode";
            this.chkWhitelistingMode.UseVisualStyleBackColor = true;
            this.chkWhitelistingMode.Click += new System.EventHandler(this.chkWhitelistingMode_CheckedChanged);
            // 
            // tpKeys
            // 
            this.tpKeys.BackColor = System.Drawing.SystemColors.Control;
            this.tpKeys.Controls.Add(this.olvSpells);
            this.tpKeys.Location = new System.Drawing.Point(4, 22);
            this.tpKeys.Name = "tpKeys";
            this.tpKeys.Padding = new System.Windows.Forms.Padding(3);
            this.tpKeys.Size = new System.Drawing.Size(384, 491);
            this.tpKeys.TabIndex = 5;
            this.tpKeys.Text = "Keys";
            // 
            // olvSpells
            // 
            this.olvSpells.AllColumns.Add(this.olvColumn1);
            this.olvSpells.AllColumns.Add(this.olvColumn2);
            this.olvSpells.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.olvSpells.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
            this.olvSpells.CellEditEnterChangesRows = true;
            this.olvSpells.CellEditTabChangesRows = true;
            this.olvSpells.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2});
            this.olvSpells.CopySelectionOnControlC = false;
            this.olvSpells.CopySelectionOnControlCUsesDragSource = false;
            this.olvSpells.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvSpells.FullRowSelect = true;
            this.olvSpells.GridLines = true;
            this.olvSpells.Location = new System.Drawing.Point(3, 3);
            this.olvSpells.MultiSelect = false;
            this.olvSpells.Name = "olvSpells";
            this.olvSpells.SelectAllOnControlA = false;
            this.olvSpells.ShowCommandMenuOnRightClick = true;
            this.olvSpells.Size = new System.Drawing.Size(378, 485);
            this.olvSpells.TabIndex = 0;
            this.olvSpells.UseAlternatingBackColors = true;
            this.olvSpells.UseCompatibleStateImageBehavior = false;
            this.olvSpells.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Name";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn1.Hideable = false;
            this.olvColumn1.IsEditable = false;
            this.olvColumn1.Text = "Name";
            this.olvColumn1.Width = 143;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "Key";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Groupable = false;
            this.olvColumn2.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn2.Hideable = false;
            this.olvColumn2.Text = "Key";
            this.olvColumn2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn2.ToolTipText = "Double click an item to edit";
            this.olvColumn2.Width = 138;
            // 
            // tpModules
            // 
            this.tpModules.BackColor = System.Drawing.SystemColors.Control;
            this.tpModules.Controls.Add(this.olvModules);
            this.tpModules.Location = new System.Drawing.Point(4, 22);
            this.tpModules.Name = "tpModules";
            this.tpModules.Padding = new System.Windows.Forms.Padding(3);
            this.tpModules.Size = new System.Drawing.Size(384, 491);
            this.tpModules.TabIndex = 3;
            this.tpModules.Text = "Modules";
            // 
            // olvModules
            // 
            this.olvModules.AllColumns.Add(this.olvColumn3);
            this.olvModules.AllColumns.Add(this.olvColumn4);
            this.olvModules.AllColumns.Add(this.olvColumn5);
            this.olvModules.AllColumns.Add(this.olvColumn6);
            this.olvModules.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.olvModules.CheckBoxes = true;
            this.olvModules.CheckedAspectName = "Enabled";
            this.olvModules.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumn5,
            this.olvColumn6});
            this.olvModules.CopySelectionOnControlC = false;
            this.olvModules.CopySelectionOnControlCUsesDragSource = false;
            this.olvModules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvModules.FullRowSelect = true;
            this.olvModules.GridLines = true;
            this.olvModules.Location = new System.Drawing.Point(3, 3);
            this.olvModules.MultiSelect = false;
            this.olvModules.Name = "olvModules";
            this.olvModules.SelectAllOnControlA = false;
            this.olvModules.ShowCommandMenuOnRightClick = true;
            this.olvModules.ShowGroups = false;
            this.olvModules.ShowItemToolTips = true;
            this.olvModules.Size = new System.Drawing.Size(378, 485);
            this.olvModules.TabIndex = 0;
            this.olvModules.UseAlternatingBackColors = true;
            this.olvModules.UseCompatibleStateImageBehavior = false;
            this.olvModules.View = System.Windows.Forms.View.Details;
            this.olvModules.ItemsChanged += new System.EventHandler<BrightIdeasSoftware.ItemsChangedEventArgs>(this.olvModules_ItemsChanged);
            this.olvModules.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.olvModules_ItemChecked);
            this.olvModules.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.olvModules_MouseDoubleClick);
            this.olvModules.MouseUp += new System.Windows.Forms.MouseEventHandler(this.olvModules_MouseUp);
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "DisplayName";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.CheckBoxes = true;
            this.olvColumn3.Groupable = false;
            this.olvColumn3.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn3.Hideable = false;
            this.olvColumn3.IsEditable = false;
            this.olvColumn3.Text = "Name";
            this.olvColumn3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn3.Width = 146;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "Comment";
            this.olvColumn4.CellPadding = null;
            this.olvColumn4.Groupable = false;
            this.olvColumn4.Hideable = false;
            this.olvColumn4.IsEditable = false;
            this.olvColumn4.Text = "Comment";
            this.olvColumn4.Width = 117;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "numWindows";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.Groupable = false;
            this.olvColumn5.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn5.Hideable = false;
            this.olvColumn5.Text = "GUIs";
            this.olvColumn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn5.Width = 38;
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "Version";
            this.olvColumn6.CellPadding = null;
            this.olvColumn6.Groupable = false;
            this.olvColumn6.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn6.Hideable = false;
            this.olvColumn6.Text = "Version";
            this.olvColumn6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn6.Width = 47;
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.developerToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(392, 24);
            this.msMain.TabIndex = 0;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.worldEditorToolStripMenuItem,
            this.radarToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // worldEditorToolStripMenuItem
            // 
            this.worldEditorToolStripMenuItem.Name = "worldEditorToolStripMenuItem";
            this.worldEditorToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.worldEditorToolStripMenuItem.Text = "&Waypoint Editor";
            this.worldEditorToolStripMenuItem.Click += new System.EventHandler(this.waypointEditorToolStripMenuItem_Click);
            // 
            // radarToolStripMenuItem
            // 
            this.radarToolStripMenuItem.Name = "radarToolStripMenuItem";
            this.radarToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.radarToolStripMenuItem.Text = "&Radar";
            this.radarToolStripMenuItem.Click += new System.EventHandler(this.radarToolStripMenuItem_Click);
            // 
            // developerToolStripMenuItem
            // 
            this.developerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.entityListToolStripMenuItem});
            this.developerToolStripMenuItem.Name = "developerToolStripMenuItem";
            this.developerToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.developerToolStripMenuItem.Text = "&Developer";
            this.developerToolStripMenuItem.Visible = false;
            // 
            // entityListToolStripMenuItem
            // 
            this.entityListToolStripMenuItem.Name = "entityListToolStripMenuItem";
            this.entityListToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.entityListToolStripMenuItem.Text = "&Entity list";
            this.entityListToolStripMenuItem.Click += new System.EventHandler(this.entityListToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkForUpdateToolStripMenuItem,
            this.toolStripSeparator3,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // checkForUpdateToolStripMenuItem
            // 
            this.checkForUpdateToolStripMenuItem.Name = "checkForUpdateToolStripMenuItem";
            this.checkForUpdateToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.checkForUpdateToolStripMenuItem.Text = "&Check for update";
            this.checkForUpdateToolStripMenuItem.Click += new System.EventHandler(this.checkForUpdateToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(162, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // tUpdate
            // 
            this.tUpdate.Interval = 1000;
            this.tUpdate.Tick += new System.EventHandler(this.tUpdate_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tsStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(392, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Status:";
            // 
            // tsStatus
            // 
            this.tsStatus.Name = "tsStatus";
            this.tsStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // cmModules
            // 
            this.cmModules.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmModules_GUIs,
            this.toolStripSeparator1,
            this.enableAllToolStripMenuItem,
            this.disableAllToolStripMenuItem,
            this.toolStripSeparator2,
            this.refreshToolStripMenuItem});
            this.cmModules.Name = "cmModules";
            this.cmModules.Size = new System.Drawing.Size(128, 104);
            // 
            // cmModules_GUIs
            // 
            this.cmModules_GUIs.Name = "cmModules_GUIs";
            this.cmModules_GUIs.Size = new System.Drawing.Size(127, 22);
            this.cmModules_GUIs.Text = "GUIs";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(124, 6);
            // 
            // enableAllToolStripMenuItem
            // 
            this.enableAllToolStripMenuItem.Name = "enableAllToolStripMenuItem";
            this.enableAllToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.enableAllToolStripMenuItem.Text = "Enable all";
            this.enableAllToolStripMenuItem.Click += new System.EventHandler(this.enableAllToolStripMenuItem_Click);
            // 
            // disableAllToolStripMenuItem
            // 
            this.disableAllToolStripMenuItem.Name = "disableAllToolStripMenuItem";
            this.disableAllToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.disableAllToolStripMenuItem.Text = "Disable all";
            this.disableAllToolStripMenuItem.Click += new System.EventHandler(this.disableAllToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(124, 6);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 593);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.msMain);
            this.MainMenuStrip = this.msMain;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "WildBot";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.tcMain.ResumeLayout(false);
            this.tpStatus.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tpSettings.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tpKeys.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvSpells)).EndInit();
            this.tpModules.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvModules)).EndInit();
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.cmModules.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpStatus;
        private System.Windows.Forms.TabPage tpSettings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.TextBox txtNameFilters;
        public System.Windows.Forms.CheckBox chkWhitelistingMode;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem worldEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem developerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entityListToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.TabPage tpModules;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnEditGrindingSpot;
        public System.Windows.Forms.ComboBox cGrindingSpot;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnCombatSettings;
        public System.Windows.Forms.ComboBox cCombatProfile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.CheckBox chkAutoFace;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtFacingAngleTolerance;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtAttackRange;
        public System.Windows.Forms.TextBox txtMoveToTargetDistance;
        public System.Windows.Forms.CheckBox chkBackgroundMode;
        public System.Windows.Forms.TextBox txtLevel;
        public AdvProgressBar apbHP;
        public System.Windows.Forms.TextBox txtPosition;
        public System.Windows.Forms.TextBox txtName;
        public System.Windows.Forms.TextBox txtFacing;
        public System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        public System.Windows.Forms.ToolStripStatusLabel tsStatus;
        public System.Windows.Forms.Timer tUpdate;
        public System.Windows.Forms.CheckBox chkAutoStart;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox txtWorldID;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabPage tpKeys;
        private BrightIdeasSoftware.ObjectListView olvSpells;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.ObjectListView olvModules;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
        private System.Windows.Forms.ContextMenuStrip cmModules;
        private System.Windows.Forms.ToolStripMenuItem cmModules_GUIs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem enableAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem radarToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkLineFacing;
    }
}

