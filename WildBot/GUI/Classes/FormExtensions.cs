﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace WildBot.GUI
{
    static class FormExtensions
    {
        static Control FindControlName(Control ctrl, string name)
        {
            if (ctrl.Name == name)
                return ctrl;
            else
                foreach (Control c in ctrl.Controls)
                {
                    Control buf = FindControlName(c, name);

                    if (buf != null)
                        return buf;
                }

            return null;
        }

        /// <summary>
        /// Returns the property value from a control, thread-safe
        /// </summary>
        /// <param name="frm">Control will be searched in this Form</param>
        /// <param name="controlName">Name of the Control</param>
        /// <param name="varName">Name of the value to get</param>
        /// <returns>The value got from the Control, on error it will be null</returns>
        public static dynamic GetControlValue(this Form frm, string controlName, string varName)
        {
            if (frm.InvokeRequired)
            {
                dynamic ret = null;
                frm.UIThread(delegate
                {
                    ret = GetControlValue(frm, controlName, varName);
                });
                return ret;
            }
            else
            {
                dynamic ctrl = FindControlName(frm, controlName);

                if (ctrl != null)
                {
                    try
                    {
                        foreach (PropertyInfo pi in ctrl.GetType().GetProperties())
                            if (pi.Name == varName)
                                return pi.GetValue(ctrl);
                    }
                    catch { }
                }
            }

            return null;
        }
    }
}
