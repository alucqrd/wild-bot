﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using WildBot.GameObjects;

namespace WildBot.GUI
{
    public class RadarObject
    {
        /// <summary>
        /// User-defined identifier. Can be anything, not handled during display
        /// </summary>
        public object ID = null;

        /// <summary>
        /// Position where the object is (Actual world position)
        /// </summary>
        public WorldPosition Position = null;

        /// <summary>
        /// Link the object to this position (with a line), null -> disabled
        /// </summary>
        public WorldPosition LinkPosition = null;

        /// <summary>
        ///  Display color
        /// </summary>
        public Color DisplayColor = Color.Yellow;

        /// <summary>
        /// Display size, default is 2
        /// </summary>
        public int DisplaySize = 2;

        /// <summary>
        /// Display shape
        /// </summary>
        public Shapes DisplayShape = Shapes.Circle;

        public enum Shapes { Triangle, Rectangle, Circle }

        /// <summary>
        /// Initialize the object without arguments
        /// </summary>
        public RadarObject()
        {
        }

        /// <summary>
        /// Initialize with all possible arguments
        /// </summary>
        /// <param name="ID">User-defined identifier. Can be anything, not handled during display</param>
        /// <param name="Position">Position where the object is (Actual world position)</param>
        /// <param name="LinkPosition">Link the object to this position (with a line), null -> disabled</param>
        /// <param name="DisplayColor">Display color</param>
        /// <param name="DisplaySize">Display size, default is 2</param>
        /// <param name="DisplayShape">Display shape</param>
        public RadarObject(object ID, WorldPosition Position, WorldPosition LinkPosition, Color DisplayColor, int DisplaySize, Shapes DisplayShape)
        {
            this.ID = ID;
            this.Position = Position;
            this.LinkPosition = LinkPosition;
            this.DisplayColor = DisplayColor;
            this.DisplaySize = DisplaySize;
            this.DisplayShape = DisplayShape;
        }
    }
}
