﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WildBot.GUI
{
    public partial class CustomButton : UserControl
    {
        #region Variables
        private Image imgNormal;
        private Image imgHover;
        private Image imgPressed;

        private bool isHovered = false;
        private bool isPressed = false;
        #endregion

        #region Properities
        [Description("This will be displayed by default"), Category("Images")]
        public Image Normal { set { imgNormal = value; Draw(); } get { return imgNormal; } }
        [Description("This will be displayed on hover"), Category("Images")]
        public Image Hover { set { imgHover = value; Draw(); } get { return imgHover; } }
        [Description("This will be displayed on MouseDown"), Category("Images")]
        public Image Pressed { set { imgPressed = value; Draw(); } get { return imgPressed; } }
        #endregion

        public CustomButton()
        {
            InitializeComponent();
        }

        #region Draw images
        private void Draw()
        {
            Draw(this.CreateGraphics());
        }

        private void Draw(Graphics g)
        {
            if (isPressed && imgPressed != null)
                g.DrawImage(imgPressed, 0, 0, this.Width, this.Height);
            else if (isHovered && imgHover != null)
                g.DrawImage(imgHover, 0, 0, this.Width, this.Height);
            else if (imgNormal != null)
                g.DrawImage(imgNormal, 0, 0, this.Width, this.Height);
            else
                g.Clear(this.BackColor);
        }
        #endregion

        #region Overwrites
        protected override void OnPaint(PaintEventArgs e)
        {
            Draw(e.Graphics);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            isPressed = true;
            Draw();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            isPressed = false;
            Draw();
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);

            isHovered = true;
            Draw();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            isHovered = false;
            Draw();
        }
        #endregion
    }
}
