﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Xml;

using BrightIdeasSoftware;

using WildBot.Tools;

namespace WildBot.GUI
{
    class MassConfigSaver
    {
        /// <summary>
        /// Saves the control and it sub-controls (can be disabled) in the given XML
        /// </summary>
        /// <param name="ctrl">Control to save</param>
        /// <param name="xmlFile">Where to save</param>
        /// <param name="recursive">If true it will save sub-controls too</param>
        /// <param name="arguments">
        /// whitelist: -> will save only only the specified controls /Note: (T) to specify type, (R) to enable regex search/ |
        /// blacklist: -> will NOT save thoose controls, type and regex can be used (see: whitelist) |
        /// no_lvi -> ListViewitems will not be saved |
        /// no_olvi -> ObjectListViewItems will not be saved</param>
        public static void SaveControl(Control ctrl, XmlFile xmlFile, bool recursive = false, string arguments = "")
        {
            ctrl.UIThread(delegate
            {
                // Get the type of the control
                string ctrlType = ctrl.GetType().Name;

                // Handle custom classes
                if (ctrlType.Substring(0, 3) == "frm")
                    ctrlType = "Form";

                // Initialize default skipping
                bool skipControl = false;

                if (arguments.IndexOf("whitelist:") >= 0)
                    skipControl = true;

                // Check if control is on whitelist
                if (arguments.IndexOf("whitelist:") >= 0)
                {
                    // Get the actual whitelist argument
                    string[] parts = Regex.Split(arguments, @"\s+");

                    foreach (string part in parts)
                    {
                        if (part.IndexOf("whitelist:") >= 0)
                        {
                            string[] controls = part.Substring("whitelist:".Length).Split(',');

                            foreach (string wl in controls)
                            {
                                if (ctrl.Name == wl || (wl.Substring(0, 3) == "(R)" && Regex.IsMatch(ctrl.Name, wl.Substring(3))) || (wl.Substring(0, 3) == "(T)" && ctrlType == wl.Substring(3)))
                                {
                                    skipControl = false;
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }

                // Check if control is on blacklist
                if (arguments.IndexOf("blacklist:") >= 0)
                {
                    // Get the actual blacklist argument
                    string[] parts = Regex.Split(arguments, @"\s+");

                    foreach (string part in parts)
                    {
                        if (part.IndexOf("blacklist:") >= 0)
                        {
                            string[] controls = part.Substring("blacklist:".Length).Split(',');

                            foreach (string wl in controls)
                            {
                                if (ctrl.Name == wl || (wl.Substring(0, 3) == "(R)" && Regex.IsMatch(ctrl.Name, wl.Substring(3))) || (wl.Substring(0, 3) == "(T)" && ctrlType == wl.Substring(3)))
                                {
                                    skipControl = true;
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }

                if (!skipControl & ctrl.Name.Length > 0)
                {
                    XmlNode xn = null;

                    bool saved = false;
                    switch (ctrlType)
                    {
                        case "Form":
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            XMLHandler.SetElement(xn, "Top", ((Form)ctrl).Top);
                            XMLHandler.SetElement(xn, "Left", ((Form)ctrl).Left);

                            if (((Form)ctrl).FormBorderStyle == FormBorderStyle.Sizable ||
                                ((Form)ctrl).FormBorderStyle == FormBorderStyle.SizableToolWindow)
                            {
                                XMLHandler.SetElement(xn, "Width", ((Form)ctrl).Width);
                                XMLHandler.SetElement(xn, "Height", ((Form)ctrl).Height);
                            }

                            saved = true;
                            break;
                        case "CheckBox":
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            XMLHandler.SetElement(xn, "Checked", ((CheckBox)ctrl).Checked);

                            saved = true;
                            break;
                        case "RadioButton":
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            XMLHandler.SetElement(xn, "Checked", ((RadioButton)ctrl).Checked);

                            saved = true;
                            break;
                        case "RichTextBox":
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            if (((RichTextBox)ctrl).Multiline)
                            {
                                string[] lines = Regex.Split(((RichTextBox)ctrl).Text, "\r\n");

                                foreach (string line in lines)
                                    XMLHandler.SetElement(xn, "Text", line, false);
                            }
                            else
                                XMLHandler.SetElement(xn, "Text", ((RichTextBox)ctrl).Text);

                            saved = true;
                            break;
                        case "FileTextBox":
                        case "TextBox":
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            if (((TextBox)ctrl).Multiline)
                            {
                                string[] lines = Regex.Split(((TextBox)ctrl).Text, "\r\n");

                                foreach (string line in lines)
                                    XMLHandler.SetElement(xn, "Text", line, false);
                            }
                            else
                                XMLHandler.SetElement(xn, "Text", ((TextBox)ctrl).Text);

                            saved = true;
                            break;
                        case "TrackBar":
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            XMLHandler.SetElement(xn, "Value", ((TrackBar)ctrl).Value);

                            saved = true;
                            break;
                        case "ComboBox":
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            XMLHandler.SetElement(xn, "Selected", ((ComboBox)ctrl).Text);

                            saved = true;
                            break;
                        case "CheckedListBox":
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);

                            for (int i = 0; i < ((CheckedListBox)ctrl).Items.Count; i++)
                                XMLHandler.SetElement(xn, ((CheckedListBox)ctrl).Items[i].ToString(), ((CheckedListBox)ctrl).GetItemChecked(i), false);

                            saved = true;
                            break;
                        case "ObjectListView":
                            ObjectListView olv = (ObjectListView)ctrl;

                            // Save ObjectListView
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            XMLHandler.SetAttribute(xn, "Type", olv.GetType().Name);

                            // Save Columns
                            XmlNode olvColumnList = XMLHandler.SetElement(xn, "ColumnList");
                            for (int column = 0; column < olv.Columns.Count; column++)
                            {
                                XmlNode buf = XMLHandler.SetElement(olvColumnList, "Column", "", false);
                                XMLHandler.SetAttribute(buf, "Index", column);

                                XMLHandler.SetElement(buf, "Text", olv.Columns[column].Text);
                                XMLHandler.SetElement(buf, "Width", olv.Columns[column].Width);
                            }

                            // Save items
                            if (arguments.IndexOf("no_olvi") < 0 && olv.Objects != null)
                            {
                                XmlNode olvItemList = XMLHandler.SetElement(xn, "ItemList");

                                int item = 0;
                                foreach (object o in olv.Objects)
                                {
                                    XmlNode buf = XMLHandler.SetElement(olvItemList, "Item", "", false);
                                    XMLHandler.SetAttribute(buf, "Index", item);
                                    XMLHandler.SetAttribute(buf, "Type", o.GetType().Name);

                                    System.Reflection.FieldInfo[] fields = o.GetType().GetFields();
                                    foreach (System.Reflection.FieldInfo field in fields)
                                        XMLHandler.SetElement(buf, field.Name, field.GetValue(o));

                                    item++;
                                }
                            }
                            break;
                        case "ListView":
                            ListView lv = (ListView)ctrl;

                            // Save ListView
                            xn = XMLHandler.SetElement(xmlFile.doc, "root", ctrl.Name);
                            XMLHandler.SetAttribute(xn, "Type", lv.GetType().Name);

                            // Save ListView headers
                            XmlNode ColumnList = XMLHandler.SetElement(xn, "ColumnList");
                            for (int column = 0; column < lv.Columns.Count; column++)
                            {
                                XmlNode buf = XMLHandler.SetElement(ColumnList, "Column", "", false);
                                XMLHandler.SetAttribute(buf, "Index", column);

                                XMLHandler.SetElement(buf, "Text", lv.Columns[column].Text);
                                XMLHandler.SetElement(buf, "Width", lv.Columns[column].Width);
                            }

                            // Save ListView items
                            if (arguments.IndexOf("no_lvi") < 0)
                            {
                                XmlNode ItemList = XMLHandler.SetElement(xn, "ItemList");
                                for (int item = 0; item < lv.Items.Count; item++)
                                {
                                    XmlNode buf = XMLHandler.SetElement(ItemList, "Item", "", false);
                                    XMLHandler.SetAttribute(buf, "Index", item);

                                    for (int sub = 0; sub < lv.Items[item].SubItems.Count; sub++)
                                    {
                                        XmlNode s = XMLHandler.SetElement(buf, "SubItem", lv.Items[item].SubItems[sub].Text, false);
                                        XMLHandler.SetAttribute(s, "Index", sub);
                                    }
                                }
                            }
                            break;
                    }

                    if (xn != null && saved)
                        XMLHandler.SetAttribute(xn, "Type", ctrlType);
                }

                // Check sub-controls (if recursive)
                if (recursive)
                    foreach (Control c in ctrl.Controls)
                        SaveControl(c, xmlFile, recursive, arguments);

                // After finished save the XML file
                xmlFile.doc.Save(xmlFile.fileName);
            });
        }

        /// <summary>
        /// Will load the Control and it's sub-controls (can be disabled) from the specified XML file
        /// </summary>
        /// <param name="ctrl">Control to load</param>
        /// <param name="xmlFile">XML file to load from</param>
        /// <param name="recursive">If true it will load sub-controls too</param>
        /// <param name="arguments">
        /// whitelist: -> will save only only the specified controls /Note: (T) to specify type, (R) to enable regex search/ |
        /// blacklist: -> will NOT save thoose controls, type and regex can be used (see: whitelist) |
        /// no_lvi -> ListViewitems will not be saved |
        /// no_olvi -> ObjectListViewItems will not be saved |
        /// olv_just_update -> will NOT add ObjectListViewItems to the OLV just override the existing ones</param>
        public static void LoadControl(Control ctrl, XmlFile xmlFile, bool recursive = false, string arguments = "")
        {
            ctrl.UIThread(delegate
            {
                // Get the type of the control
                string ctrlType = ctrl.GetType().Name;

                // Handle custom classes
                if (ctrlType.Substring(0, 3) == "frm")
                    ctrlType = "Form";

                // Initialize default skipping
                bool skipControl = false;

                if (arguments.IndexOf("whitelist:") >= 0)
                    skipControl = true;

                // Check if control is on whitelist
                if (arguments.IndexOf("whitelist:") >= 0)
                {
                    // Get the actual whitelist argument
                    string[] parts = Regex.Split(arguments, @"\s+");

                    foreach (string part in parts)
                    {
                        if (part.IndexOf("whitelist:") >= 0)
                        {
                            string[] controls = part.Substring("whitelist:".Length).Split(',');

                            foreach (string wl in controls)
                            {
                                if (ctrl.Name == wl || (wl.Substring(0, 3) == "(R)" && Regex.IsMatch(ctrl.Name, wl.Substring(3))) || (wl.Substring(0, 3) == "(T)" && ctrlType == wl.Substring(3)))
                                {
                                    skipControl = false;
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }

                // Check if control is on blacklist
                if (arguments.IndexOf("blacklist:") >= 0)
                {
                    // Get the actual blacklist argument
                    string[] parts = Regex.Split(arguments, @"\s+");

                    foreach (string part in parts)
                    {
                        if (part.IndexOf("blacklist:") >= 0)
                        {
                            string[] controls = part.Substring("blacklist:".Length).Split(',');

                            foreach (string wl in controls)
                            {
                                if (ctrl.Name == wl || (wl.Substring(0, 3) == "(R)" && Regex.IsMatch(ctrl.Name, wl.Substring(3))) || (wl.Substring(0, 3) == "(T)" && ctrlType == wl.Substring(3)))
                                {
                                    skipControl = true;
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }

                if (!skipControl)
                {
                    foreach (XmlNode xn in XMLHandler.GetElement(xmlFile.doc, "root"))
                    {
                        if (xn.Name == ctrl.Name)
                        {
                            switch (XMLHandler.GetAttribute(xn, "Type"))
                            {
                                case "Form":
                                    try
                                    {
                                        ((Form)ctrl).Top = int.Parse(XMLHandler.GetElement(xn, "Top").First().InnerText);
                                        ((Form)ctrl).Left = int.Parse(XMLHandler.GetElement(xn, "Left").First().InnerText);

                                        if (((Form)ctrl).FormBorderStyle == FormBorderStyle.Sizable ||
                                            ((Form)ctrl).FormBorderStyle == FormBorderStyle.SizableToolWindow)
                                        {
                                            ((Form)ctrl).Width = int.Parse(XMLHandler.GetElement(xn, "Width").First().InnerText);
                                            ((Form)ctrl).Height = int.Parse(XMLHandler.GetElement(xn, "Height").First().InnerText);
                                        }
                                    }
                                    catch { }
                                    break;
                                case "CheckBox":
                                    try
                                    {
                                        ((CheckBox)ctrl).Checked = XMLHandler.GetElement(xn, "Checked").First().InnerText == "True" ? true : false;
                                    }
                                    catch { }
                                    break;
                                case "RadioButton":
                                    try
                                    {
                                        ((RadioButton)ctrl).Checked = XMLHandler.GetElement(xn, "Checked").First().InnerText == "True" ? true : false;
                                    }
                                    catch { }
                                    break;
                                case "RichTextBox":
                                    try
                                    {
                                        if (((RichTextBox)ctrl).Multiline)
                                        {
                                            ((RichTextBox)ctrl).Text = "";

                                            XmlNode[] nodes = XMLHandler.GetElement(xn, "Text");
                                            foreach (XmlNode node in nodes)
                                            {
                                                ((RichTextBox)ctrl).Text += node.InnerText;

                                                if (node != nodes.Last())
                                                    ((RichTextBox)ctrl).Text += "\r\n";
                                            }
                                        }
                                        else
                                            ((RichTextBox)ctrl).Text = XMLHandler.GetElement(xn, "Text").First().InnerText.Replace(@"\n", "\r\n");
                                    }
                                    catch { }
                                    break;
                                case "FileTextBox":
                                case "TextBox":
                                    try
                                    {
                                        if (((TextBox)ctrl).Multiline)
                                        {
                                            ((TextBox)ctrl).Text = "";

                                            XmlNode[] nodes = XMLHandler.GetElement(xn, "Text");
                                            foreach (XmlNode node in nodes)
                                            {
                                                ((TextBox)ctrl).Text += node.InnerText;

                                                if (node != nodes.Last())
                                                    ((TextBox)ctrl).Text += "\r\n";
                                            }
                                        }
                                        else
                                            ((TextBox)ctrl).Text = XMLHandler.GetElement(xn, "Text").First().InnerText.Replace(@"\n", "\r\n");
                                    }
                                    catch { }
                                    break;
                                case "TrackBar":
                                    try
                                    {
                                        int val;
                                        if (int.TryParse(XMLHandler.GetElement(xn, "Value").First().InnerText, out val))
                                            ((TrackBar)ctrl).Value = val;
                                    }
                                    catch { }
                                    break;
                                case "ComboBox":
                                    try
                                    {
                                        for (int i = 0; i < ((ComboBox)ctrl).Items.Count; i++)
                                        {
                                            if (((ComboBox)ctrl).Items[i].ToString() == XMLHandler.GetElement(xn, "Selected").First().InnerText)
                                            {
                                                ((ComboBox)ctrl).SelectedIndex = i;
                                                break;
                                            }
                                        }
                                    }
                                    catch { }
                                    break;
                                case "CheckedListBox":
                                    try
                                    {
                                        for (int i = 0; i < ((CheckedListBox)ctrl).Items.Count; i++)
                                        {
                                            XmlNode[] nodes = XMLHandler.GetElement(xn, ((CheckedListBox)ctrl).Items[i].ToString());

                                            if (nodes != null && nodes.Length > 0)
                                                ((CheckedListBox)ctrl).SetItemChecked(i, nodes.First().InnerText == "True" ? true : false);
                                        }
                                    }
                                    catch { }
                                    break;
                                case "ObjectListView":
                                    ObjectListView olv = (ObjectListView)ctrl;

                                    // Load columns
                                    XmlNode[] olvColumnList = XMLHandler.GetElement(xn, "ColumnList");

                                    if (olvColumnList != null && olvColumnList.Count() > 0)
                                    {
                                        olvColumnList = XMLHandler.GetElement(olvColumnList.First());

                                        if (olvColumnList != null && olvColumnList.Count() > 0)
                                        {
                                            foreach (OLVColumn ch in olv.Columns)
                                            {
                                                foreach (XmlNode column in olvColumnList)
                                                {
                                                    try
                                                    {
                                                        if (ch.Text == XMLHandler.GetElement(column, "Text").First().InnerText)
                                                        {
                                                            ch.Width = int.Parse(XMLHandler.GetElement(column, "Width").First().InnerText);
                                                            break;
                                                        }
                                                    }
                                                    catch { }
                                                }
                                            }
                                        }
                                    }

                                    // Load items
                                    bool justOverride = false;
                                    if (arguments.IndexOf("olv_just_update") >= 0)
                                        justOverride = true;

                                    if (arguments.IndexOf("no_olvi") < 0)
                                    {
                                        List<object> list = new List<object>();

                                        if (justOverride && olv.Objects != null)
                                            foreach (object o in olv.Objects)
                                                list.Add(o);

                                        if (!justOverride | (justOverride & list.Count > 0))
                                        {
                                            XmlNode[] ItemList = XMLHandler.GetElement(xn, "ItemList");

                                            if (ItemList != null && ItemList.Count() > 0)
                                            {
                                                ItemList = XMLHandler.GetElement(ItemList.First());

                                                if (ItemList != null && ItemList.Count() > 0)
                                                {
                                                    foreach (XmlNode item in ItemList)
                                                    {
                                                        try
                                                        {
                                                            object o = Assembly.GetExecutingAssembly().CreateInstance("WildBot.GUI." + XMLHandler.GetAttribute(item, "Type"));

                                                            XmlNode[] vals = XMLHandler.GetElement(item);
                                                            if (vals != null && vals.Count() > 0)
                                                            {
                                                                System.Reflection.FieldInfo[] fields = o.GetType().GetFields();
                                                                foreach (System.Reflection.FieldInfo field in fields)
                                                                {
                                                                    foreach (XmlNode val in vals)
                                                                    {
                                                                        if (field.Name == val.Name)
                                                                            field.SetValue(o, val.InnerText);
                                                                    }
                                                                }
                                                            }

                                                            if (justOverride)
                                                            {
                                                                for (int i = 0; i < list.Count; i++)
                                                                {
                                                                    if (o.Equals(list[i]))
                                                                    {
                                                                        list.RemoveAt(i);
                                                                        list.Insert(i, o);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                                list.Add(o);
                                                        }
                                                        catch { }
                                                    }
                                                }
                                            }
                                        }

                                        olv.SetObjects(list);
                                    }

                                    break;
                                case "ListView":
                                    // Load ListView headers
                                    XmlNode[] ColumnList = XMLHandler.GetElement(xn, "ColumnList");
                                    if (ColumnList != null && ColumnList.Count() > 0)
                                    {
                                        ColumnList = XMLHandler.GetElement(ColumnList.First());

                                        if (ColumnList != null && ColumnList.Count() > 0)
                                        {
                                            foreach (ColumnHeader ch in ((ListView)ctrl).Columns)
                                            {
                                                foreach (XmlNode column in ColumnList)
                                                {
                                                    try
                                                    {
                                                        if (ch.Text == XMLHandler.GetElement(column, "Text").First().InnerText)
                                                        {
                                                            ch.Width = int.Parse(XMLHandler.GetElement(column, "Width").First().InnerText);
                                                            break;
                                                        }
                                                    }
                                                    catch { }
                                                }
                                            }
                                        }
                                    }

                                    // Load ListView items
                                    if (arguments.IndexOf("no_lvi") < 0)
                                    {
                                        XmlNode[] ItemList = XMLHandler.GetElement(xn, "ItemList");

                                        if (ItemList != null && ItemList.Count() > 0)
                                        {
                                            ItemList = XMLHandler.GetElement(ItemList.First());

                                            if (ItemList != null && ItemList.Count() > 0)
                                            {
                                                ((ListView)ctrl).Items.Clear();

                                                foreach (XmlNode item in ItemList)
                                                {
                                                    SortedDictionary<int, XmlNode> sorter = new SortedDictionary<int, XmlNode>();

                                                    foreach (XmlNode subitem in item.ChildNodes)
                                                    {
                                                        try
                                                        {
                                                            sorter.Add(int.Parse(XMLHandler.GetAttribute(subitem, "Index")), subitem);
                                                        }
                                                        catch { }
                                                    }

                                                    ListViewItem lvi = new ListViewItem(sorter[0].InnerText);
                                                    for (int i = 1; i < sorter.Count; i++)
                                                        lvi.SubItems.Add(sorter[i].InnerText);

                                                    ((ListView)ctrl).Items.Add(lvi);
                                                }
                                            }
                                        }
                                    }
                                    break;
                            }

                            break;
                        }
                    }
                }

                // Check sub-controls (if recursive)
                if (recursive)
                    foreach (Control c in ctrl.Controls)
                        LoadControl(c, xmlFile, recursive, arguments);
            });
        }
    }
}