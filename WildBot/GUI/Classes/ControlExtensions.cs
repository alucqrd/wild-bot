﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WildBot.GUI
{
    // No namespace, so we can use it everywhere without "using WildBot.Tools;"
    // Code is from Pablo Grisafi: http://www.codeproject.com/Articles/37642/Avoiding-InvokeRequired
    static class ControlExtensions
    {
        /// <summary>
        /// Will get the parameter of a control from any thread
        /// </summary>
        /// <param name="control">The control you want to read from</param>
        /// <param name="code"></param>
        static public void UIThread(this Control control, Action code)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(code);
                return;
            }
            code.Invoke();
        }
    }
}