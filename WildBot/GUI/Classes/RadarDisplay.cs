﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WildBot.GameObjects;

namespace WildBot.GUI
{
    public partial class RadarDisplay : UserControl
    {
        #region Variables
        private float magnitification = 3;
        private bool useLineForFacing = false;
        private Bitmap bmp;
        private Graphics grp;
        private Timer tmrIncDec = new Timer();
        private Timer tmrAutoSpeed = new Timer();
        private bool isIncreasing = false;
        private bool isDecreasing = false;
        private bool waitRequired = true;
        #endregion

        #region Properities
        /// <summary>
        /// Layers to display in the Radar Display (Note: Sorted by the Keys, so "A" will be drawn above "B")
        /// </summary>
        [Description("Object to display"), Category("Radar settings")]
        public SortedDictionary<string, List<RadarObject>> Layers = new SortedDictionary<string, List<RadarObject>>();

        /// <summary>
        /// Zoom level, minimum is 1, maximum is 9.9
        /// </summary>
        [Description("Magnitification amount"), Category("Radar settings")]
        public float Magnitification { get { return magnitification; } set { magnitification = (value >= 1 ? (value < 10F ? value : 9.9F) : 1); Draw(); } }

        /// <summary>
        /// True: A whole-screen line will display the facing, False: A little triangle will display the facing
        /// </summary>
        [Description("Use a line for facing instead of the triangle"), Category("Radar settings")]
        public bool UseLineForFacing { get { return useLineForFacing; } set { useLineForFacing = value; Draw(); } }
        #endregion

        public RadarDisplay()
        {
            InitializeComponent();

            // Init the drawing surface
            bmp = new Bitmap(this.Width, this.Height);
            grp = Graphics.FromImage(bmp);

            grp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            grp.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.Default;

            grp.Clear(this.BackColor);

            // Init the auto-redraw
            Timer tmr = new Timer();
            tmr.Tick += tmr_Tick;
            tmr.Interval = 100;
            tmr.Enabled = true;

            // Setup speed inc / dec
            tmrIncDec.Tick += tmrIncDec_Tick;
            tmrAutoSpeed.Tick += tmrAutoSpeed_Tick;
            tmrAutoSpeed.Interval = 1000;
        }

        #region Painting
        public float ParseFacing()
        {
            if (WildBot.AI.BotBrain.GameBase != null && WildBot.AI.BotBrain.GameBase.Player != null)
            {
                float facing = WildBot.AI.BotBrain.GameBase.Player.FacingDirection;

                if (facing < 0)
                    return 3.14F + (3.14F - Math.Abs(facing));
                else if (facing > 0)
                    return facing;

                return 0;
            }

            return -1;
        }

        public void Draw()
        {
            try
            {
                Draw(this.CreateGraphics());
            }
            catch { }
        }

        private void Draw(Graphics g)
        {
            // Clear
            grp.Clear(this.BackColor);

            // Everything is relative to the player, if no player object, skip
            if (WildBot.AI.BotBrain.GameBase != null && WildBot.AI.BotBrain.GameBase.Player != null)
            {
                float size;

                // Draw the objects
                foreach (KeyValuePair<string, List<RadarObject>> kv in Layers.Reverse())
                {
                    foreach (RadarObject ro in kv.Value)
                    {
                        // Skip if there is no position set
                        if (ro.Position == null)
                            continue;

                        size = ro.DisplaySize * magnitification;

                        float X = (this.Width / 2) - ((WildBot.AI.BotBrain.GameBase.Player.WorldX - ro.Position.X) * magnitification);
                        float Y = (this.Height / 2) - ((WildBot.AI.BotBrain.GameBase.Player.WorldY - ro.Position.Y) * magnitification);

                        switch (ro.DisplayShape)
                        {
                            case RadarObject.Shapes.Circle:
                                grp.FillEllipse(new SolidBrush(ro.DisplayColor), X - (size / 2), Y - (size / 2), size, size);
                                break;
                            case RadarObject.Shapes.Rectangle:
                                grp.FillRectangle(new SolidBrush(ro.DisplayColor), X - (size / 2), Y - (size / 2), size, size);
                                break;
                            case RadarObject.Shapes.Triangle:
                                PointF[] points =
                            {
                                new PointF(X, Y - (size / 2)),
                                new PointF(X - (size / 2), Y + (size / 2)),
                                new PointF(X + (size / 2), Y + (size / 2))
                            };

                                grp.FillPolygon(new SolidBrush(ro.DisplayColor), points);
                                break;
                        }

                        // Draw the link
                        if (ro.LinkPosition != null)
                        {
                            float X2 = (this.Width / 2) - ((WildBot.AI.BotBrain.GameBase.Player.WorldX - ro.LinkPosition.X) * magnitification);
                            float Y2 = (this.Height / 2) - ((WildBot.AI.BotBrain.GameBase.Player.WorldY - ro.LinkPosition.Y) * magnitification);

                            grp.DrawLine(new Pen(ro.DisplayColor), X, Y, X2, Y2);
                        }
                    }
                }

                // Draw the Player
                size = 3 * magnitification;
                grp.FillEllipse(Brushes.Blue, (this.Width / 2) - (size / 2), (this.Height / 2) - (size / 2), size, size);

                // Draw the facing
                if (ParseFacing() >= 0)
                {
                    if (useLineForFacing)
                        grp.DrawLine(Pens.Blue, this.Width / 2, this.Height / 2, (float)((this.Width / 2) + (Math.Sin(ParseFacing() + 3.14F) * this.Width)), (float)((this.Height / 2) + (Math.Cos(ParseFacing() + 3.14F) * this.Width)));
                    else
                    {
                        PointF[] points = 
                        {
                            new PointF((float)((this.Width / 2) + (Math.Sin(ParseFacing() + 3.14F) * size)), (float)((this.Height / 2) + (Math.Cos(ParseFacing() + 3.14F) * size))),
                            new PointF((float)((this.Width / 2) + (Math.Sin(ParseFacing() + 3.14F - 1.57F) * (size / 2))), (float)((this.Height / 2) + (Math.Cos(ParseFacing() + 3.14F - 1.57) * (size / 2)))),
                            new PointF((float)((this.Width / 2) + (Math.Sin(ParseFacing() + 3.14F + 1.57F) * (size / 2))), (float)((this.Height / 2) + (Math.Cos(ParseFacing() + 3.14F + 1.57F) * (size / 2))))
                        };

                        grp.FillPolygon(Brushes.Blue, points);
                    }
                }
            }

            // Draw the directions
            grp.DrawString("N", this.Font, Brushes.White, (this.Width / 2) - (this.Font.GetHeight() / 2), 5);
            grp.DrawString("S", this.Font, Brushes.White, (this.Width / 2) - (this.Font.GetHeight() / 2), this.Height - 5 - this.Font.GetHeight());
            grp.DrawString("W", this.Font, Brushes.White, 5, (this.Height / 2) - (this.Font.GetHeight() / 2));
            grp.DrawString("E", this.Font, Brushes.White, this.Width - this.Font.GetHeight() - 5, (this.Height / 2) - (this.Font.GetHeight() / 2));

            // Draw the current magnification
            grp.DrawString(string.Format("{0:0.0}x", Math.Round(magnitification, 1)), this.Font, Brushes.White, btnAdd.Left - 30, btnAdd.Top + (this.Font.GetHeight() / 3));

            g.DrawImage(bmp, 0, 0, this.Width, this.Height);
        }
        #endregion

        #region Events
        void tmr_Tick(object sender, EventArgs e)
        {
            Draw();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (magnitification < 9.9F)
            {
                magnitification += 0.1F;
                Draw();
            }
        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            if (magnitification > 1.1F)
            {
                magnitification -= 0.1F;
                Draw();
            }
        }

        private void btnAdd_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                isIncreasing = true;
                isDecreasing = false;
                waitRequired = true;
                tmrIncDec.Interval = 200;
                tmrIncDec.Enabled = true;
                tmrAutoSpeed.Enabled = true;
            }
        }

        private void btnAdd_MouseUp(object sender, MouseEventArgs e)
        {
            isIncreasing = false;
            isDecreasing = false;
            tmrIncDec.Enabled = false;
            tmrAutoSpeed.Enabled = false;
        }

        private void btnSub_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                isIncreasing = false;
                isDecreasing = true;
                waitRequired = true;
                tmrIncDec.Interval = 200;
                tmrIncDec.Enabled = true;
                tmrAutoSpeed.Enabled = true;
            }
        }

        private void btnSub_MouseUp(object sender, MouseEventArgs e)
        {
            isIncreasing = false;
            isDecreasing = false;
            tmrIncDec.Enabled = false;
            tmrAutoSpeed.Enabled = false;
        }

        void tmrIncDec_Tick(object sender, EventArgs e)
        {
            // Proceed only if required
            if (isIncreasing | isDecreasing)
            {
                // Check if skip is required
                if (waitRequired)
                    waitRequired = false;
                else
                {
                    if (isIncreasing)
                        btnAdd_Click(sender, e);
                    else if (isDecreasing)
                        btnSub_Click(sender, e);
                }
            }
            else
                tmrIncDec.Enabled = false;
        }

        void tmrAutoSpeed_Tick(object sender, EventArgs e)
        {
            if (tmrIncDec.Enabled && tmrIncDec.Interval > 50)
                tmrIncDec.Interval -= 50;
            else
                tmrAutoSpeed.Enabled = false;
        }
        #endregion

        #region Overrides
        protected override void OnPaint(PaintEventArgs e)
        {
            Draw(e.Graphics);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            if (this.Width > 0 && this.Height > 0)
            {
                // Init the drawing surface
                bmp = new Bitmap(this.Width, this.Height);
                grp = Graphics.FromImage(bmp);

                grp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                grp.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.Default;

                grp.Clear(this.BackColor);

                Draw();
            }
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);

            if (e.Delta > 0)
                btnAdd_Click(this, null);
            else if (e.Delta < 0)
                btnSub_Click(this, null);
        }
        #endregion
    }
}
