﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.GUI
{
    public class KeyBinding
    {
        public string Group;
        public string Name;
        public string Key;

        public KeyBinding()
        {
        }

        public KeyBinding(string name, string key)
        {
            Name = name;
            Key = key;
        }

        public KeyBinding(string name, string key, string group)
        {
            Name = name;
            Key = key;
            Group = group;
        }

        public static bool operator ==(KeyBinding a, KeyBinding b)
        {
            if (a.Name == b.Name)
                return true;

            return false;
        }

        public static bool operator !=(KeyBinding a, KeyBinding b)
        {
            if (a.Name != b.Name)
                return true;

            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType().Name == "KeyBinding" && ((KeyBinding)obj).Name == Name)
                return true;

            return false;
        }
    }
}
