﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WildBot.AI;
using WildBot.Modules;

namespace WildBot.GUI
{
    class ModuleItem
    {
        public bool Enabled;
        public string DisplayName;
        public string Name;
        public string Comment;
        public string Version;
        public int numWindows
        {
            get
            {
                int guis = 0;

                foreach (KeyValuePair<frmBase, string> kv in WildBotObject.ModuleGUIs)
                {
                    if (kv.Value == Name.Substring(0, Name.LastIndexOf('.')))
                        guis++;
                }

                return guis;
            }
        }

        public ModuleItem()
        {
        }

        public ModuleItem(string name, string displayname, string comment, string version, bool enabled)
        {
            Enabled = enabled;
            DisplayName = displayname;
            Name = name;
            Comment = comment;
            Version = version;
        }

        public ModuleContainer GetContainer()
        {
            foreach (ModuleContainer mc in BotBrain.ModuleList)
            {
                if (mc.Name == Name)
                    return mc;
            }

            return null;
        }
    }
}
