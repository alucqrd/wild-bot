﻿namespace WildBot.GUI
{
    partial class RadarDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadarDisplay));
            this.btnSub = new WildBot.GUI.CustomButton();
            this.btnAdd = new WildBot.GUI.CustomButton();
            this.SuspendLayout();
            // 
            // btnSub
            // 
            this.btnSub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSub.Hover = ((System.Drawing.Image)(resources.GetObject("btnSub.Hover")));
            this.btnSub.Location = new System.Drawing.Point(232, 3);
            this.btnSub.Name = "btnSub";
            this.btnSub.Normal = ((System.Drawing.Image)(resources.GetObject("btnSub.Normal")));
            this.btnSub.Pressed = ((System.Drawing.Image)(resources.GetObject("btnSub.Pressed")));
            this.btnSub.Size = new System.Drawing.Size(21, 21);
            this.btnSub.TabIndex = 1;
            this.btnSub.Click += new System.EventHandler(this.btnSub_Click);
            this.btnSub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnSub_MouseDown);
            this.btnSub.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnSub_MouseUp);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Hover = global::WildBot.Properties.Resources.btnAdd_hover;
            this.btnAdd.Location = new System.Drawing.Point(205, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Normal = ((System.Drawing.Image)(resources.GetObject("btnAdd.Normal")));
            this.btnAdd.Pressed = ((System.Drawing.Image)(resources.GetObject("btnAdd.Pressed")));
            this.btnAdd.Size = new System.Drawing.Size(21, 21);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnAdd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnAdd_MouseDown);
            this.btnAdd.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnAdd_MouseUp);
            // 
            // RadarDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.btnSub);
            this.Controls.Add(this.btnAdd);
            this.Name = "RadarDisplay";
            this.Size = new System.Drawing.Size(256, 256);
            this.ResumeLayout(false);

        }

        #endregion

        private CustomButton btnAdd;
        private CustomButton btnSub;

    }
}
