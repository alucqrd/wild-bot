﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WildBot.GUI
{
    public class FileTextBox : TextBox
    {
        private bool localModify = false;
        private string _text = "";
        public override string Text
        {
            get
            {
                if (localModify)
                    return base.Text;
                else
                    return _text;
            }

            set
            {
                if (localModify)
                    base.Text = value;
                else
                {
                    _text = value;

                    UpdateText();
                }
            }
        }

        private string GetFileName(string path)
        {
            string[] parts = _text.Split('\\');

            if (parts.Length > 0)
                return parts.Last();

            return path;
        }

        private void UpdateText()
        {
            localModify = true;
            if (Focused)
                Text = _text;
            else
                Text = GetFileName(_text);
            localModify = false;
        }

        protected override void OnGotFocus(EventArgs e)
        {
            UpdateText();

            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            UpdateText();

            base.OnLostFocus(e);
        }
    }
}
