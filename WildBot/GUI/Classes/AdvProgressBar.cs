﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WildBot.GUI
{
    public partial class AdvProgressBar : UserControl
    {
        #region Private variables
        private long min = 0;
        private long max = 100;
        private long val = 50;

        private Color back = Color.WhiteSmoke;
        private Color fore = Color.Green;

        private string text = "";

        private Bitmap bmp_left;
        private Bitmap bmp_right;

        private Graphics grp_left;
        private Graphics grp_right;
        #endregion

        #region Public variables
        /// <summary>
        /// Minimum value
        /// </summary>
        [Description("Minimum value"), Category("AdvProgressBar")]
        public long Minimum
        {
            get { return min; }
            set { min = value; Draw(); }
        }
        /// <summary>
        /// Maximum value
        /// </summary>
        [Description("Maximum value"), Category("AdvProgressBar")]
        public long Maximum
        {
            get { return max; }
            set { max = value; Draw(); }
        }
        /// <summary>
        /// Current value
        /// </summary>
        [Description("Current value"), Category("AdvProgressBar")]
        public long Value
        {
            get { return val; }
            set { val = value; Draw(); }
        }
        /// <summary>
        /// Background color
        /// </summary>
        [Description("Background color"), Category("AdvProgressBar")]
        public Color Background
        {
            get { return back; }
            set { back = value; Draw(); }
        }
        /// <summary>
        /// Foreground color
        /// </summary>
        [Description("Foreground color"), Category("AdvProgressBar")]
        public Color Foreground
        {
            get { return fore; }
            set { fore = value; Draw(); }
        }
        /// <summary>
        /// Display this text
        /// {0} -> Minimum
        /// {1} -> Maximum
        /// {2} -> Value
        /// {3} -> Percentage
        /// </summary>
        [Description("Text to display\n{0}: Min, {1}: Max, {2}: Val, {3}: Percent"), Category("AdvProgressBar")]
        public string Display
        {
            get { return text; }
            set { text = value; Draw(); }
        }
        #endregion

        public AdvProgressBar()
        {
            InitializeComponent();
        }

        #region Drawing
        private void Draw(Graphics g)
        {
            // Get the line where to split the control
            float prc = ((float)val / ((float)max - (float)min));
            int splitPos = (int)(this.Width * prc);

            // Align text to the middle
            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            // Format the string
            string txt = String.Format(text, min, max, val, Math.Round(float.IsNaN(prc) ? 0 : prc, 2) * 100F);

            // Draw the control
            if (splitPos <= 0 || splitPos >= this.Width - 1)
            {
                // Draw the frame
                ProgressBarRenderer.DrawHorizontalBar(g, this.ClientRectangle);

                g.FillRectangle(new SolidBrush(splitPos <= 0 ? back : fore), 1, 1, this.Width - 2, this.Height - 2);

                g.DrawString(txt, this.Font, new SolidBrush(splitPos <= 0 ? fore : back), this.ClientRectangle, sf);
            }
            else
            {
                // Resize the bitmaps
                bmp_left = new Bitmap(splitPos, this.Height);
                bmp_right = new Bitmap(this.Width - splitPos, this.Height);

                grp_left = Graphics.FromImage(bmp_left);
                grp_right = Graphics.FromImage(bmp_right);

                grp_left.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                grp_right.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;

                // Draw the left side
                grp_left.DrawRectangle(new Pen(SystemColors.ActiveBorder), new Rectangle(0, 0, splitPos, this.Height));
                grp_left.FillRectangle(new SolidBrush(fore), 1, 1, splitPos, this.Height - 2);
                grp_left.DrawString(txt, this.Font, new SolidBrush(back), this.ClientRectangle, sf);

                // Draw the right side
                grp_right.DrawRectangle(new Pen(SystemColors.ActiveBorder), new Rectangle(0, 0, this.Width - splitPos, this.Height));
                grp_right.FillRectangle(new SolidBrush(back), 0, 1, this.Width - splitPos - 2, this.Height - 2);
                grp_right.DrawString(txt, this.Font, new SolidBrush(fore), new RectangleF(-splitPos, 0, this.Width, this.Height), sf);

                // Draw sides to the progress bar
                g.DrawImage(bmp_left, 0, 0);
                g.DrawImage(bmp_right, splitPos, 0);
            }
        }

        private void Draw()
        {
            Draw(this.CreateGraphics());
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Draw(e.Graphics);
        }
        #endregion
    }
}