﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WildBot.GUI
{
    partial class frmAbout : Form
    {
        private string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public frmAbout()
        {
            InitializeComponent();

            lName.Text = Application.ProductName + " (v" + Application.ProductVersion + ")";
            lCopyright.Text = AssemblyCopyright;
        }
    }
}
