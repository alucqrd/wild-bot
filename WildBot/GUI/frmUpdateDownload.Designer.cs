﻿namespace WildBot.GUI
{
    partial class frmUpdateDownload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lStatus = new System.Windows.Forms.Label();
            this.apbStatus = new WildBot.GUI.AdvProgressBar();
            this.SuspendLayout();
            // 
            // lStatus
            // 
            this.lStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lStatus.Location = new System.Drawing.Point(12, 9);
            this.lStatus.Name = "lStatus";
            this.lStatus.Size = new System.Drawing.Size(295, 13);
            this.lStatus.TabIndex = 1;
            this.lStatus.Text = "Please wait while downloading the update";
            this.lStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // apbStatus
            // 
            this.apbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.apbStatus.Background = System.Drawing.Color.WhiteSmoke;
            this.apbStatus.Foreground = System.Drawing.Color.Green;
            this.apbStatus.Location = new System.Drawing.Point(12, 31);
            this.apbStatus.Maximum = ((long)(1000));
            this.apbStatus.Minimum = ((long)(0));
            this.apbStatus.Name = "apbStatus";
            this.apbStatus.Size = new System.Drawing.Size(295, 20);
            this.apbStatus.TabIndex = 0;
            this.apbStatus.Value = ((long)(0));
            // 
            // frmUpdateDownload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 64);
            this.Controls.Add(this.lStatus);
            this.Controls.Add(this.apbStatus);
            this.MaximumSize = new System.Drawing.Size(999999, 103);
            this.MinimumSize = new System.Drawing.Size(335, 103);
            this.Name = "frmUpdateDownload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Downloading update";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUpdateDownload_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        public AdvProgressBar apbStatus;
        public System.Windows.Forms.Label lStatus;
    }
}