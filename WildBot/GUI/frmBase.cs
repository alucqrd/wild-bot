﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using WildBot.Tools;

namespace WildBot.GUI
{
    public partial class frmBase : Form
    {
        public XmlFile xmlSettings;
        public bool autoWidth = true;
        public bool autoHeight = true;

        public frmBase()
        {
            InitializeComponent();

            this.ControlAdded += frmBase_ControlAdded;
            this.ControlRemoved += frmBase_ControlRemoved;
        }

        // Load settings when control added
        void frmBase_ControlAdded(object sender, ControlEventArgs e)
        {
            // Load setting
            if (xmlSettings != null)
                MassConfigSaver.LoadControl(e.Control, xmlSettings);

            // Auto save when control lost focus or when validating finished
            e.Control.LostFocus += SaveEvent;
            e.Control.Validated += SaveEvent;

            // Get the dimensions
            int w = -1, h = -1;
            if (autoWidth | autoHeight)
            {
                // Check each control
                foreach (Control ctrl in this.Controls)
                {
                    if ((ctrl.Left + ctrl.Width) > w)
                        w = ctrl.Left + ctrl.Width;

                    if ((ctrl.Top + ctrl.Height) > h)
                        h = ctrl.Top + ctrl.Height;
                }
            }

            if (w > 0)
                this.ClientSize = new Size(w + 12, ClientSize.Height);

            if (h > 0)
                this.ClientSize = new Size(ClientSize.Width, h + 12);
        }

        void frmBase_ControlRemoved(object sender, ControlEventArgs e)
        {
            // Get the dimensions
            int w = -1, h = -1;
            if (autoWidth | autoHeight)
            {
                // Check each control
                foreach (Control ctrl in this.Controls)
                {
                    if ((ctrl.Left + ctrl.Width) > w)
                        w = ctrl.Left + ctrl.Width;

                    if ((ctrl.Top + ctrl.Height) > h)
                        h = ctrl.Top + ctrl.Height;
                }
            }

            if (w > 0)
                this.ClientSize = new Size(w + 12, ClientSize.Height);

            if (h > 0)
                this.ClientSize = new Size(ClientSize.Width, h + 12);
        }

        private void frmBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Save window position
            MassConfigSaver.SaveControl(this, WildBotObject.xmlSettings);

            // Save settings
            if (xmlSettings != null)
                MassConfigSaver.SaveControl(this, xmlSettings, true, "blacklist:(T)Form");

            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void SaveEvent(object sender, EventArgs e)
        {
            // Save settings
            if (xmlSettings != null)
                MassConfigSaver.SaveControl((Control)sender, xmlSettings);
        }

        private void frmBase_Shown(object sender, EventArgs e)
        {
            // Load window position
            MassConfigSaver.LoadControl(this, WildBotObject.xmlSettings);
        }
    }
}
