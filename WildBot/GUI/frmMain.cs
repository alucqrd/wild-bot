﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Xml;

using WildBot.AI;
using WildBot.GameObjects;
using WildBot.Modules;
using WildBot.Modules.CombatModules;
using WildBot.Tools;
using WildBot.GameControllers;

namespace WildBot.GUI
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            // Auto save controls on focus loosing or on valaidating
            this.ControlAdded += ControlAddedEvent;

            InitializeComponent();

            AddLog("System startup");

            // Set WildBotObject.fMain to "this", it is required
            WildBotObject.fMain = this;

            #if DEBUG
                developerToolStripMenuItem.Visible = true;
            #endif

            UpdateCombatHandlerList();
            UpdateGrindingSpotList();
            UpdateModuleList();

            if (cCombatProfile.Text == "" && cCombatProfile.Items.Count > 0)
                cCombatProfile.SelectedIndex = 0;

            if (cGrindingSpot.Text == "" && cGrindingSpot.Items.Count > 0)
                cGrindingSpot.SelectedIndex = 0;

            // Setup grouping of the ObjectListView
            olvColumn1.GroupKeyGetter = delegate(object x)
            {
                if (x.GetType().Name == "KeyBinding")
                    return ((KeyBinding)x).Group;

                return "";
            };

            // Set the Object list of the ObjectListView and fill it up
            List<KeyBinding> objectList = new List<KeyBinding>();
            olvSpells.SetObjects(objectList);

            AddKey("Innate ability key", "R", "Spells");

            for (int i = 1; i <= 8; i++)
                AddKey("Action bar slot key " + i, i.ToString(), "Spells");

            AddKey("Path ability key 1", "", "Spells");
            AddKey("Path ability key 2", "", "Spells");

            AddKey("Potion spell key", "X", "Spells");

            AddKey("Forward", "W", "Movement");
            AddKey("Backward", "S", "Movement");
            AddKey("Left", "A", "Movement");
            AddKey("Right", "D", "Movement");
            AddKey("Strafe Left", "Q", "Movement");
            AddKey("Strafe Right", "E", "Movement");
            AddKey("Jump", "SPACE", "Movement");

            AddKey("Vacuum loot", "V", "Other");

            // Load settings
            MassConfigSaver.LoadControl(this, WildBotObject.xmlSettings, true, "blacklist:txtLog,txtName,txtLevel,txtPosition,txtFacing,txtWorldID,olvModules olv_just_update");
            MassConfigSaver.LoadControl(olvModules, WildBotObject.xmlSettings, false, "no_olvi");
        }

        #region Auto-saving
        void ControlAddedEvent(object sender, ControlEventArgs e)
        {
            // Make sure to apply this event to every control
            AddEvents(e.Control);
        }

        void SaveEvent(object sender, EventArgs e)
        {
            // Save settings
            if (((Control)sender).Name == "olvModules")
                MassConfigSaver.SaveControl((Control)sender, WildBotObject.xmlSettings, false, "no_olvi");
            else
                MassConfigSaver.SaveControl((Control)sender, WildBotObject.xmlSettings, false, "blacklist:txtLog,txtName,txtLevel,txtPosition,txtFacing,txtWorldID");
        }

        void AddEvents(Control ctrl)
        {
            if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                foreach (Control c in ctrl.Controls)
                    AddEvents(c);

            ctrl.ControlAdded += ControlAddedEvent;
            ctrl.Validated += SaveEvent;
            ctrl.LostFocus += SaveEvent;
        }
        #endregion

        #region Menu clicks & Status updates
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Shutting down
            // Save the form
            MassConfigSaver.SaveControl(this, WildBotObject.xmlSettings, true, "blacklist:txtLog,txtName,txtLevel,txtPosition,txtFacing,txtWorldID,olvModules");
            MassConfigSaver.SaveControl(olvModules, WildBotObject.xmlSettings, false, "no_olvi");
            SaveModuleStates();

            // Delete the hook
            KeyboardHook.DeleteHook();

            // Shut down the AI thread
            BotBrain._AILoopThread.Abort();

            // Shut down move thread
            GameController.AbortMoveThread();
        }

        private void checkForUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Updater.IsUpdating())
            {
                if (MessageBox.Show("An update is already running in the background, would you like to stop it?", "Already updating", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    Updater.EndUpdate();
            }
            else
            {
                Updater.UpdateFinished += Updater_UpdateFinished;
                Updater.BeginUpdate();
            }
        }

        void Updater_UpdateFinished(UpdateEventArgs e)
        {
            if (!e.Canceled & e.NewVersion == new Version(Application.ProductVersion))
                MessageBox.Show("There is no update available", "No update");

            Updater.UpdateFinished -= Updater_UpdateFinished;
        }

        private void tUpdate_Tick(object sender, EventArgs e)
        {
            UpdatePlayerData();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout fAbout = new frmAbout();
            fAbout.ShowDialog();
        }

        private void entityListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEntityList fEntityList = new frmEntityList();
            fEntityList.Show(this);
        }

        private void waypointEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmWaypointEditor fWaypointEditor = new frmWaypointEditor();
            fWaypointEditor.Show(this);
        }

        private void radarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!WildBotObject.fRadar.Visible)
                WildBotObject.fRadar.Show(this);

            WildBotObject.fRadar.Activate();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (BotBrain.status >= 0)
            {
                KeySender.ReleaseKeys();

                if (BotBrain.paused)
                {
                    btnStart.Text = "Pause";
                    BotBrain.paused = false;
                    AddLog("Bot started");
                }
                else
                {
                    btnStart.Text = "Start";
                    BotBrain.paused = true;
                    AddLog("Bot paused");
                }
            }
        }
        #endregion

        #region Status tab
        #region Variables
        private string lastLog = "";
        #endregion

        #region Methods
        /// <summary>
        /// Add a log to the main form
        /// </summary>
        /// <param name="msg">Message to add</param>
        /// <param name="logColor">Color (default is Color.Black</param>
        public void AddLog(string msg, Color logColor)
        {
            // If invoke required then do it
            if (this.InvokeRequired)
                this.Invoke(new Action(delegate { AddLog(msg, logColor); }));
            else
            {
                if (lastLog != msg)
                {
                    // If there is already a previous text add a new line
                    if (txtLog.Text.Length > 0)
                        txtLog.AppendText("\n");

                    // Set the color
                    txtLog.Select(txtLog.Text.Length, 0);
                    txtLog.SelectionColor = logColor;

                    // Add the log with DateTime
                    txtLog.AppendText("[" + DateTime.Now.ToString() + "]: ");
                    txtLog.AppendText(msg);

                    // Scroll down if needed
                    txtLog.SelectionStart = txtLog.Text.Length;
                    txtLog.ScrollToCaret();

                    lastLog = msg;
                }
            }
        }

        /// <summary>
        /// Add a log to the main form
        /// </summary>
        /// <param name="msg">Message to add</param>
        public void AddLog(string msg)
        {
            AddLog(msg, Color.Black);
        }

        /// <summary>
        /// Updates the data displayed in the Status tab
        /// </summary>
        public void UpdatePlayerData()
        {
            // If invoke required then do it
            if (this.InvokeRequired)
                this.Invoke(new Action(delegate { UpdatePlayerData(); }));
            else
            {
                if (BotBrain.status >= 0)
                {
                    try
                    {
                        txtName.Text = BotBrain.GameBase.Player.Name;
                        txtLevel.Text = BotBrain.GameBase.Player.Level.ToString();

                        if (apbHP.Maximum != (int)BotBrain.GameBase.Player.LifeMax)
                            apbHP.Maximum = (int)BotBrain.GameBase.Player.LifeMax;

                        if (apbHP.Value != BotBrain.GameBase.Player.Life)
                        {
                            if (BotBrain.GameBase.Player.Life <= BotBrain.GameBase.Player.LifeMax)
                                apbHP.Value = (int)BotBrain.GameBase.Player.Life;
                            else
                                apbHP.Value = apbHP.Maximum;
                        }

                        txtPosition.Text = Math.Round(BotBrain.GameBase.Player.WorldX, 2) + ", " + Math.Round(BotBrain.GameBase.Player.WorldY, 2) + ", " + Math.Round(BotBrain.GameBase.Player.WorldZ, 2);
                        txtFacing.Text = BotBrain.GameBase.Player.FacingDirection.ToString();
                        txtWorldID.Text = BotBrain.GameBase.Player.CurrentWorldId.ToString();

                        if (BotBrain.paused)
                        {
                            tsStatus.Text = "Paused";
                            btnStart.Text = "Start";
                        }
                        else
                        {
                            tsStatus.Text = "Running";
                            btnStart.Text = "Pause";
                        }
                    }
                    catch { }
                }
            }
        }
        #endregion
        #endregion

        #region Settings tab
        #region Variables
        private bool keyHandled = false;
        private string lastHandler = "";
        public static ModuleContainer CurrentCombatHandler = null;
        #endregion

        #region Methods
        private void UpdateCombatHandlerList()
        {
            // Init directory
            SmallExtensions.InitDirectory(Application.StartupPath + @"\..\Modules\CombatModules");

            // Remove invalid CombatHandlers
            for (int i = 0; i < cCombatProfile.Items.Count; )
            {
                if (cCombatProfile.Items[i].ToString() != "None" && !File.Exists(Application.StartupPath + @"\..\Modules\CombatModules\" + cCombatProfile.Items[i]))
                    cCombatProfile.Items.RemoveAt(i);
                else
                    i++;
            }

            // Add CombatModules
            foreach (string path in Directory.GetFiles(Application.StartupPath + @"\..\Modules\CombatModules", "*.cs"))
            {
                string fileName = path.IndexOf('\\') >= 0 ? path.Split('\\').Last() : path;

                bool isExists = false;
                foreach (string cItem in cCombatProfile.Items)
                {
                    if (cItem == fileName)
                        isExists = true;
                }

                if (!isExists)
                    cCombatProfile.Items.Add(fileName);
            }
        }

        private void UpdateGrindingSpotList()
        {
            // Init directory
            SmallExtensions.InitDirectory(Application.StartupPath + @"\..\Grinding Spots");

            // Remove invalid Modules
            for (int i = 0; i < cGrindingSpot.Items.Count; )
            {
                if (cGrindingSpot.Items[i].ToString() != "None" && !File.Exists(Application.StartupPath + @"\..\Grinding Spots\" + cGrindingSpot.Items[i]))
                    cGrindingSpot.Items.RemoveAt(i);
                else
                    i++;
            }

            // Add Paths
            foreach (string path in Directory.GetFiles(Application.StartupPath + @"\..\Grinding Spots", "*.xml"))
            {
                string fileName = path.IndexOf('\\') >= 0 ? path.Split('\\').Last() : path;

                bool isExists = false;
                foreach (string cItem in cGrindingSpot.Items)
                {
                    if (cItem == fileName)
                        isExists = true;
                }

                if (!isExists)
                    cGrindingSpot.Items.Add(fileName);
            }
        }
        #endregion

        #region Events
        private void chkLineFacing_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                WildBotObject.fRadar.rdRadar.UseLineForFacing = true;
            else
                WildBotObject.fRadar.rdRadar.UseLineForFacing = false;
        }

        private void chkWhitelistingMode_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                groupBox3.Text = "Target only these monsters";
            else
                groupBox3.Text = "Do not target these monsters";
        }

        private void txtNameFilters_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && !e.Alt && !e.Shift && e.KeyCode == Keys.A)
            {
                keyHandled = true;
                txtNameFilters.SelectAll();
            }
        }

        private void txtNameFilters_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (keyHandled)
            {
                e.Handled = true;
                keyHandled = false;
            }
        }

        private void btnCombatSettings_Click(object sender, EventArgs e)
        {
            if (cCombatProfile.Text != "None")
            {
                string name = cCombatProfile.Text.Substring(0, cCombatProfile.Text.LastIndexOf('.'));

                foreach (KeyValuePair<frmBase, string> kv in WildBotObject.ModuleGUIs)
                {
                    if (kv.Key.Name == name)
                    {
                        if (!kv.Key.Visible)
                            kv.Key.Show(this);
                        kv.Key.Activate();
                    }
                }
            }
        }

        private void cCombatProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Remove the previous GUI-s
            if (lastHandler.Length > 0)
                foreach (KeyValuePair<frmBase, string> kv in WildBotObject.ModuleGUIs.Reverse())
                {
                    if (kv.Value == lastHandler.Substring(0, lastHandler.LastIndexOf('.')))
                    {
                        WildBotObject.ModuleGUIs.Remove(kv.Key);
                        kv.Key.Dispose();
                    }
                }

            // Set previous module name to the current value
            lastHandler = cCombatProfile.Text;

            // If there is a previous CombatHandler selected then first remove it
            if (CurrentCombatHandler != null)
            {
                foreach (ModuleContainer mc in BotBrain.ModuleList)
                {
                    if (mc.Name == CurrentCombatHandler.Name)
                    {
                        BotBrain.ModuleList.Remove(mc);
                        break;
                    }
                }
            }

            if (cCombatProfile.Text == "None")
                return;

            // Load the script
            CurrentCombatHandler = new ModuleContainer(cCombatProfile.Text, true, ScriptHandler.LoadScript(Application.StartupPath + @"\..\Modules\CombatModules\" + cCombatProfile.Text));

            // If script loaded succesfully add it to the bot brain
            if (CurrentCombatHandler != null)
            {
                // Add the script to the SubBrain list
                BotBrain.ModuleList.Add(CurrentCombatHandler);
            }
            else if (MessageBox.Show("There is an error in the script file (\"" + cCombatProfile.Text + "\"). Do you want to open the log file?", "Error", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                Process.Start(Application.StartupPath + @"\..\Logs\" + cCombatProfile.Text.Substring(0, cCombatProfile.Text.LastIndexOf(".")) + ".log");
        }

        private void LoadHelper(WaypointList wl, XmlNode[] nodes)
        {
            if (nodes != null && nodes.Length > 0 && nodes.First().ChildNodes != null)
            {
                // Load waypoints
                foreach (XmlNode xn in XMLHandler.GetElement(nodes.First(), "WorldPosition"))
                {
                    float x, y, z;

                    if (float.TryParse(XMLHandler.GetAttribute(xn, "X"), out x) &&
                        float.TryParse(XMLHandler.GetAttribute(xn, "Y"), out y) &&
                        float.TryParse(XMLHandler.GetAttribute(xn, "Z"), out z))
                    {
                        wl.Positions.Add(new WorldPosition(x, y, z));
                    }
                }

                // Load if waypoints are circle or not
                XmlNode[] buf = XMLHandler.GetElement(nodes.First(), "Circle");

                if (buf.Length > 0 && buf.First().InnerText == "True")
                    wl.IsCircle = true;
                else
                    wl.IsCircle = false;
            }
        }

        private void cGrindingSpot_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cGrindingSpot.Text == "None")
            {
                StatusInformerObject.SelectedGrindingSpot = new GrindingSpot(-1);
                return;
            }

            XmlFile xmlWaypoints = new XmlFile(Application.StartupPath + @"\..\Grinding Spots\" + cGrindingSpot.Text, new XmlDocument());

            // Load XML file if it is exists
            if (File.Exists(xmlWaypoints.fileName))
                xmlWaypoints.doc.Load(xmlWaypoints.fileName);
            else
            {
                XmlDeclaration xDec = xmlWaypoints.doc.CreateXmlDeclaration("1.0", "utf-8", null);

                XmlElement rootNode = xmlWaypoints.doc.CreateElement("root");
                xmlWaypoints.doc.InsertAfter(xDec, xmlWaypoints.doc.DocumentElement);
                xmlWaypoints.doc.AppendChild(rootNode);
            }

            XmlNode[] nodes = XMLHandler.GetElement(xmlWaypoints.doc, "root", "WorldID");

            int worldID = -1;
            if (nodes != null && nodes.Length > 0)
                int.TryParse(nodes.First().InnerText, out worldID);

            StatusInformerObject.SelectedGrindingSpot = new GameObjects.GrindingSpot(worldID);

            // Load Grinding
            LoadHelper(StatusInformerObject.SelectedGrindingSpot.GrindingPath, XMLHandler.GetElement(xmlWaypoints.doc, "root", "Grinding"));

            // Load Holocrypt
            LoadHelper(StatusInformerObject.SelectedGrindingSpot.HolocryptPath, XMLHandler.GetElement(xmlWaypoints.doc, "root", "Holocrypt"));

            // Load Vendor
            LoadHelper(StatusInformerObject.SelectedGrindingSpot.VendorPath, XMLHandler.GetElement(xmlWaypoints.doc, "root", "Vendor"));
        }

        private void cCombatProfile_DropDown(object sender, EventArgs e)
        {
            UpdateCombatHandlerList();
        }

        private void chkBackgroundMode_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                KeySender.BackGroundMode = true;
            else
                KeySender.BackGroundMode = false;
        }

        private void cGrindingSpot_DropDown(object sender, EventArgs e)
        {
            UpdateGrindingSpotList();
        }

        private void btnEditGrindingSpot_Click(object sender, EventArgs e)
        {
            frmWaypointEditor fWaypointEditor = null;
            if (cGrindingSpot.Text.Length > 0 & cGrindingSpot.Text != "None")
                fWaypointEditor = new frmWaypointEditor(SmallExtensions.InitDirectory(Application.StartupPath + @"\..\Grinding Spots\" + cGrindingSpot.Text));
            else
                fWaypointEditor = new frmWaypointEditor();

            fWaypointEditor.Show(this);
        }
        #endregion
        #endregion

        #region Key tab
        /// <summary>
        /// Add a key to the Keys tab
        /// </summary>
        /// <param name="kb">Holds the Name, Value and Group of the key</param>
        public void AddKey(KeyBinding kb)
        {
            if (this.InvokeRequired)
                this.Invoke(new Action(delegate { AddKey(kb); }));
            else if (olvSpells.Objects != null)
            {
                bool addIt = true;
                foreach (object o in olvSpells.Objects)
                {
                    if (kb == (KeyBinding)o)
                    {
                        ((KeyBinding)o).Key = kb.Key;
                        addIt = false;
                        break;
                    }
                }

                if (addIt)
                    olvSpells.AddObject(kb);
            }
        }

        /// <summary>
        /// Add a key to the Keys tab
        /// </summary>
        /// <param name="name">Name of the key (Shown on the left)</param>
        /// <param name="key">Key (Shown on the right)</param>
        /// <param name="group">Group where it will be placed</param>
        public void AddKey(string name, string key, string group = "")
        {
            AddKey(new KeyBinding(name, key, group));
        }

        /// <summary>
        /// Delete key from the Keys tab
        /// </summary>
        /// <param name="name">Name of the key (Shown on the left)</param>
        public void DelKey(string name)
        {
            if (this.InvokeRequired)
                this.Invoke(new Action(delegate { DelKey(name); }));
            else if (olvSpells.Objects != null)
            {
                foreach (object o in olvSpells.Objects)
                {
                    if (((KeyBinding)o).Name == name)
                    {
                        olvSpells.RemoveObject(o);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Get key from the Keys tab
        /// </summary>
        /// <param name="name">Name of the key (Shown on the left)</param>
        /// <returns>Key (shown on the right)</returns>
        public string GetKey(string name)
        {
            if (this.InvokeRequired)
            {
                string key = "";
                this.UIThread(delegate
                {
                    key = GetKey(name);
                });
                return key;
            }

            foreach (object o in olvSpells.Objects)
            {
                if (((KeyBinding)o).Name == name)
                    return ((KeyBinding)o).Key;
            }

            return "";
        }
        #endregion

        #region Modules tab
        #region Methods
        private void SaveModuleStates()
        {
            if (olvModules.Objects != null)
            {
                XmlNode xn = XMLHandler.SetElement(WildBotObject.xmlSettings.doc, "root", "ModuleList");

                foreach (object o in olvModules.Objects)
                {
                    ModuleItem mi = (ModuleItem)o;
                    XMLHandler.SetElement(xn, mi.Name, mi.Enabled);
                }
            }
        }

        private void LoadModuleStates()
        {
            if (olvModules.Objects != null)
            {
                XmlNode[] nodes = XMLHandler.GetElement(WildBotObject.xmlSettings.doc, "root", "ModuleList");

                if (nodes != null && nodes.Count() > 0)
                    nodes = XMLHandler.GetElement(nodes.First());

                foreach (object o in olvModules.Objects)
                {
                    ModuleItem mi = (ModuleItem)o;

                    bool enabled = true;

                    if (nodes != null)
                    {
                        foreach (XmlNode node in nodes)
                        {
                            if (node.Name == mi.Name)
                            {
                                enabled = node.InnerText == "True" ? true : false;
                                break;
                            }
                        }
                    }

                    mi.Enabled = enabled;
                    olvModules.UpdateObject(o);
                }

                UpdateModuleStates();
            }
        }

        private void UpdateModuleStates()
        {
            // Disable the event to prevent infinite-loops
            olvModules.ItemsChanged -= olvModules_ItemsChanged;

            if (olvModules.Objects != null)
            {
                foreach (object o in olvModules.Objects)
                {
                    ModuleItem mi = (ModuleItem)o;

                    // Enable / Disable module if it is already loaded
                    bool addIt = true;

                    ModuleContainer mc = mi.GetContainer();
                    if (mc != null)
                    {
                        // Run the Enable / Disable method
                        if (mc.Enabled & !mi.Enabled)
                            mc.Value.OnDisable();
                        else if (!mc.Enabled & mi.Enabled)
                            mc.Value.OnEnable();

                        // Set it's status
                        mc.Enabled = mi.Enabled;
                        addIt = false;
                    }

                    // If it is not loaded, load it
                    if (addIt && mi.Enabled)
                    {
                        // Compile the script
                        Module module = ScriptHandler.LoadScript(Application.StartupPath + @"\..\Modules\" + mi.Name);

                        // If there was no error then add it to the SubBrain list
                        if (module != null)
                            BotBrain.ModuleList.Add(new ModuleContainer(mi.Name, true, module));
                        else
                        {
                            // Couldn't compile the script, un-check it
                            mi.Enabled = false;

                            // Display that there was an error
                            if (MessageBox.Show("There is an error in the script file (\"" + mi.DisplayName + "\"). Do you want to open the log file?", "Error", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                Process.Start(Application.StartupPath + @"\..\Logs\" + mi.Name.Substring(0, mi.Name.LastIndexOf(".")) + ".log");
                        }
                    }
                }

                // Refresh the display
                olvModules.SetObjects(olvModules.Objects);
            }

            // Add back the event
            olvModules.ItemsChanged += olvModules_ItemsChanged;
        }

        private string GetModuleInfo(string file, string info)
        {
            if (File.Exists(file))
            {
                string[] lines = File.ReadAllLines(file);

                if (lines.Count() > 0 && lines[0] == "// Module informations")
                {
                    foreach (string line in lines)
                    {
                        if (line.IndexOf(info + ":") >= 0)
                        {
                            return line.Substring(line.IndexOf(info + ":") + info.Length + 1).Trim();
                        }
                    }
                }
            }

            return "";
        }

        private void UpdateModuleList()
        {
            List<ModuleItem> items = new List<ModuleItem>();

            foreach (string path in Directory.GetFiles(Application.StartupPath + @"\..\Modules", "*.cs"))
            {
                ModuleItem mi = new ModuleItem();

                mi.Enabled = false;
                mi.DisplayName = GetModuleInfo(path, "Name");
                mi.Name = path.Substring(path.LastIndexOf('\\') + 1);
                mi.Comment = GetModuleInfo(path, "Comment");
                mi.Version = GetModuleInfo(path, "Version");

                if (mi.DisplayName.Length <= 0)
                    mi.DisplayName = mi.Name;

                items.Add(mi);
            }

            // Remove non-existant scripts from BotBrain
            foreach (ModuleContainer mc in BotBrain.ModuleList.Reverse<ModuleContainer>())
            {
                bool found = false;
                foreach (ModuleItem mi in items)
                {
                    if (mc.Name == mi.Name)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                    BotBrain.ModuleList.Remove(mc);
            }

            // Add items to the OLV
            olvModules.SetObjects(items);
            
            // Load module states
            LoadModuleStates();
        }
        #endregion

        #region Events
        private void olvModules_ItemsChanged(object sender, BrightIdeasSoftware.ItemsChangedEventArgs e)
        {
            UpdateModuleStates();
        }

        private void olvModules_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            SaveModuleStates();
            UpdateModuleStates();
        }

        private void olvModules_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                cmModules_GUIs.DropDownItems.Clear();

                if (olvModules.SelectedIndex >= 0)
                {
                    ModuleItem mi = (ModuleItem)olvModules.SelectedObject;

                    foreach (KeyValuePair<frmBase, string> kv in WildBotObject.ModuleGUIs)
                    {
                        if (kv.Value == mi.Name.Substring(0, mi.Name.LastIndexOf('.')))
                        {
                            ToolStripMenuItem cms = new ToolStripMenuItem();
                            cms.Text = kv.Key.Text;
                            cms.Tag = kv.Key;
                            cms.Click += guisToolStripMenuItem_Click;
                            cmModules_GUIs.DropDownItems.Add(cms);
                        }
                    }

                    ModuleContainer mc = mi.GetContainer();
                    if (mc != null)
                        refreshToolStripMenuItem.Text = "Reload";
                    else
                        refreshToolStripMenuItem.Text = "Load";
                }
                else
                    refreshToolStripMenuItem.Text = "Refresh";

                if (cmModules_GUIs.DropDownItems.Count > 0)
                    cmModules_GUIs.Enabled = true;
                else
                    cmModules_GUIs.Enabled = false;

                cmModules.Show((Control)sender, e.X, e.Y);
            }
        }

        private void guisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)sender).Tag != null)
            {
                if (!((Form)((ToolStripMenuItem)sender).Tag).Visible)
                    ((Form)((ToolStripMenuItem)sender).Tag).Show(this);
                ((Form)((ToolStripMenuItem)sender).Tag).Activate();
            }
        }

        private void enableAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (olvModules.Objects != null)
            {
                foreach (object o in olvModules.Objects)
                    ((ModuleItem)o).Enabled = true;

                UpdateModuleStates();
                SaveModuleStates();
            }
        }

        private void disableAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (olvModules.Objects != null)
            {
                foreach (object o in olvModules.Objects)
                    ((ModuleItem)o).Enabled = false;

                UpdateModuleStates();
                SaveModuleStates();
            }
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (olvModules.SelectedIndex < 0)
                UpdateModuleList();
            else
            {
                ModuleItem mi = null;
                if (olvModules.SelectedObject != null)
                    mi = (ModuleItem)olvModules.SelectedObject;

                if (mi != null)
                {
                    // Compile the script
                    Module module = ScriptHandler.LoadScript(Application.StartupPath + @"\..\Modules\" + mi.Name);

                    ModuleContainer mc = mi.GetContainer();

                    // If there was no error then update in BotBrain
                    if (module != null)
                    {
                        if (mc != null)
                            mc.Value = module;
                        else
                            BotBrain.ModuleList.Add(new ModuleContainer(mi.Name, mi.Enabled, module));
                    }
                    else
                    {
                        // Display that there was an error
                        if (MessageBox.Show("There is an error in the script file (\"" + mi.DisplayName + "\"). Do you want to open the log file?\n(Note: The currenlty loaded script is still available.)", "Error", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                            Process.Start(Application.StartupPath + @"\..\Logs\" + mi.Name.Substring(0, mi.Name.LastIndexOf(".")) + ".log");
                    }
                }

                UpdateModuleStates();
            }
        }

        private void olvModules_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModuleItem mi = null;
            if (olvModules.SelectedObject != null)
                mi = (ModuleItem)olvModules.SelectedObject;

            if (mi != null)
            {
                foreach (KeyValuePair<frmBase, string> kv in WildBotObject.ModuleGUIs)
                {
                    string name = mi.Name.Substring(0, mi.Name.LastIndexOf('.'));
                    if (kv.Value == name && kv.Key.Name == name)
                    {
                        if (!kv.Key.Visible)
                            kv.Key.Show(this);
                        kv.Key.Activate();
                        break;
                    }
                }
            }
        }
        #endregion
        #endregion
    }
}
