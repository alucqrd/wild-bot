﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using WildBot.AI;

namespace WildBot.GUI
{
    public partial class frmEntityList : Form
    {
        private ListViewColumnSorter lvwColumnSorter;

        public frmEntityList()
        {
            InitializeComponent();

            // Initialize column shorter
            lvwColumnSorter = new ListViewColumnSorter();
            lvEntities.ListViewItemSorter = lvwColumnSorter;

            // Load settings
            MassConfigSaver.LoadControl(this, WildBotObject.xmlSettings, true, "no_lvi");

            // Setup events
            lvEntities.ColumnWidthChanged += lvEntities_ColumnWidthChanged;
            txtUpdateInterval.TextChanged += txtUpdateInterval_TextChanged;
            txtFilter.TextChanged += SaveEvent;
            chkAutoUpdate.Click += SaveEvent;
        }

        /// <summary>
        /// Add item to the lvEntities
        /// </summary>
        /// <param name="items">It holds all the column datas</param>
        private void AddItem(dynamic[] items)
        {
            ListViewItem lvi = new ListViewItem();

            foreach (dynamic d in items)
            {
                string str = string.Format("{0}", d);

                if (lvi.Text == "")
                    lvi = new ListViewItem(str);
                else
                    lvi.SubItems.Add(str);
            }

            lvEntities.Items.Add(lvi);
        }

        /// <summary>
        /// Update the Entity list
        /// </summary>
        private void UpdateList()
        {
            lvEntities.Items.Clear();

            if (BotBrain.status >= 0)
            {
                // Pause the bot thread for to get the entity list
                BotBrain._AILoopThread.Suspend();

                foreach (KeyValuePair<uint, WildBot.GameObjects.Entity> entity in BotBrain.GameBase.EntityList)
                {
                    List<dynamic> items = new List<dynamic>(); ;
                    items.Add(entity.Value.Name);
                    items.Add(entity.Value.GUID);
                    items.Add(entity.Value.WorldX);
                    items.Add(entity.Value.WorldY);
                    items.Add(entity.Value.WorldZ);
                    items.Add(entity.Value.ComputeDistanceFromEntity(BotBrain.GameBase.Player));
                    items.Add(BotBrain.GameBase.Player.ComputeDirection(entity.Value.Position));
                    items.Add(entity.Value.FacingDirection);

                    // Filtering
                    bool addIt = true;

                    // Filtering enabled
                    if (txtFilter.Text.Length > 0)
                    {
                        // Split on each '&', white spaces doesn't matter
                        string[] filters = Regex.Split(txtFilter.Text, @"\s*&\s*");

                        // Check each filter
                        foreach (string filter in filters)
                        {
                            string trimmed = filter.Trim();

                            // Check operators
                            int pType = -1;
                            string[] parts = new string[1];
                            for (int i = 0; i <= 6; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        parts = Regex.Split(trimmed, @"\s*==\s*");

                                        break;
                                    case 1:
                                        parts = Regex.Split(trimmed, @"\s*!=\s*");

                                        break;
                                    case 2:
                                        parts = Regex.Split(trimmed, @"\s*>\s*");

                                        break;
                                    case 3:
                                        parts = Regex.Split(trimmed, @"\s*>=\s*");

                                        break;
                                    case 4:
                                        parts = Regex.Split(trimmed, @"\s*<\s*");

                                        break;
                                    case 5:
                                        parts = Regex.Split(trimmed, @"\s*<=\s*");

                                        break;
                                    case 6: // Regex check
                                        parts = Regex.Split(trimmed, @"\s*R=\s*");
                                        
                                        break;
                                }

                                if (parts.Length == 2)
                                {
                                    pType = i;
                                    break;
                                }
                            }

                            // Skip if one of the 2 side is empty or there is no operator
                            if (pType < 0)
                                break;

                            // Check each header
                            foreach (ColumnHeader ch in lvEntities.Columns)
                            {
                                if (ch.Text == parts[0].Trim())
                                {
                                    // Get as string
                                    string val = string.Format("{0}", items[ch.Index]);

                                    // Try to parse to int, if success then handle it as number
                                    int valA, valB;
                                    if (int.TryParse(val, out valA) && int.TryParse(parts[1].Trim(), out valB))
                                    {
                                        // If they are not equal filter failed, do not add this item
                                        switch (pType)
                                        {
                                            case 0: // Equal (==)
                                                if (valA != valB)
                                                    addIt = false;
                                                break;
                                            case 1: // Not equal (!=)
                                                if (valA == valB)
                                                    addIt = false;
                                                break;
                                            case 2: // Bigger than (>)
                                                if (valA <= valB)
                                                    addIt = false;
                                                break;
                                            case 3: // Bigger or equal (>=)
                                                if (valA < valB)
                                                    addIt = false;
                                                break;
                                            case 4: // Lesser than (<)
                                                if (valA >= valB)
                                                    addIt = false;
                                                break;
                                            case 5: // Lesser or equal (<=)
                                                if (valA > valB)
                                                    addIt = false;
                                                break;

                                        }
                                    }
                                    // Handle it as string
                                    else
                                    {
                                        switch (pType)
                                        {
                                            case 0: // Equal (==)
                                                if (val != parts[1].Trim())
                                                    addIt = false;
                                                break;
                                            case 1: // Not equal (!=)
                                                if (val == parts[1].Trim())
                                                    addIt = false;
                                                break;
                                            case 6: // Regex check
                                                if (!Regex.IsMatch(val, parts[1].Trim()))
                                                    addIt = false;
                                                break;
                                        }
                                    }
                                }
                            }

                            if (!addIt)
                                break;
                        }
                    }
                    
                    if (addIt)
                        AddItem(items.ToArray());
                 }

                // Resume the bot thread
                BotBrain._AILoopThread.Resume();
            }
        }

        #region Events
        // Sort when column click
        void lvEntities_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            lvEntities.Sort();
        }

        // Enable / Disable showing column
        void tsi_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsi = (ToolStripMenuItem)sender;

            foreach (ColumnHeader ch in lvEntities.Columns)
            {
                if (tsi.Text == ch.Text)
                {
                    if (tsi.Checked)
                        ch.Width = 60;
                    else
                        ch.Width = 0;

                    break;
                }
            }
        }

        // Enable / Disable update timer
        private void chkAutoUpdate_CheckedChanged(object sender, EventArgs e)
        {
            tmrRefresh.Enabled = chkAutoUpdate.Checked;
        }

        // On refresh button / update timer tick update the entity list
        private void tmrRefresh_Tick(object sender, EventArgs e)
        {
            UpdateList();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateList();
        }

        // Save the column width on change
        private void lvEntities_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            MassConfigSaver.SaveControl(lvEntities, WildBotObject.xmlSettings, false, "no_lvi");
        }

        private void txtFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
                UpdateList();
        }

        private void txtUpdateInterval_TextChanged(object sender, EventArgs e)
        {
            int interval;

            if (int.TryParse(txtUpdateInterval.Text, out interval))
                tmrRefresh.Interval = interval;

            MassConfigSaver.SaveControl(txtUpdateInterval, WildBotObject.xmlSettings);
        }

        private void SaveEvent(object sender, EventArgs e)
        {
            MassConfigSaver.SaveControl((Control)sender, WildBotObject.xmlSettings, false, "no_lvi");
        }

        private void frmEntityList_FormClosing(object sender, FormClosingEventArgs e)
        {
            MassConfigSaver.SaveControl(this, WildBotObject.xmlSettings);
        }
        #endregion
    }
}
