﻿namespace WildBot.GUI
{
    partial class frmWaypointEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lvWaypoints = new System.Windows.Forms.ListView();
            this.chX = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chY = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chZ = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmWaypointEditor = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.pullUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pushDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAdd = new System.Windows.Forms.Button();
            this.gbEdit = new System.Windows.Forms.GroupBox();
            this.chkAutoAdd = new System.Windows.Forms.CheckBox();
            this.btnGetCur = new System.Windows.Forms.Button();
            this.txtZ = new System.Windows.Forms.TextBox();
            this.lZ = new System.Windows.Forms.Label();
            this.lY = new System.Windows.Forms.Label();
            this.txtY = new System.Windows.Forms.TextBox();
            this.txtX = new System.Windows.Forms.TextBox();
            this.lX = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.chkCircle = new System.Windows.Forms.CheckBox();
            this.cType = new System.Windows.Forms.ComboBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tmrAutoAdd = new System.Windows.Forms.Timer(this.components);
            this.txtFile = new WildBot.GUI.FileTextBox();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtWorldID = new System.Windows.Forms.TextBox();
            this.cmWaypointEditor.SuspendLayout();
            this.gbEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvWaypoints
            // 
            this.lvWaypoints.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvWaypoints.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chX,
            this.chY,
            this.chZ});
            this.lvWaypoints.FullRowSelect = true;
            this.lvWaypoints.GridLines = true;
            this.lvWaypoints.Location = new System.Drawing.Point(18, 39);
            this.lvWaypoints.Name = "lvWaypoints";
            this.lvWaypoints.Size = new System.Drawing.Size(470, 303);
            this.lvWaypoints.TabIndex = 1;
            this.lvWaypoints.UseCompatibleStateImageBehavior = false;
            this.lvWaypoints.View = System.Windows.Forms.View.Details;
            this.lvWaypoints.DoubleClick += new System.EventHandler(this.lvWaypoints_DoubleClick);
            this.lvWaypoints.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvWaypoints_KeyDown);
            this.lvWaypoints.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lvWaypoints_KeyPress);
            this.lvWaypoints.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lvWaypoints_MouseUp);
            // 
            // chX
            // 
            this.chX.Text = "X";
            this.chX.Width = 148;
            // 
            // chY
            // 
            this.chY.Text = "Y";
            this.chY.Width = 150;
            // 
            // chZ
            // 
            this.chZ.Text = "Z";
            this.chZ.Width = 146;
            // 
            // cmWaypointEditor
            // 
            this.cmWaypointEditor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.toolStripSeparator1,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator3,
            this.pullUpToolStripMenuItem,
            this.pushDownToolStripMenuItem,
            this.toolStripSeparator2,
            this.deleteToolStripMenuItem});
            this.cmWaypointEditor.Name = "cmWaypointEditor";
            this.cmWaypointEditor.Size = new System.Drawing.Size(138, 176);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.selectAllToolStripMenuItem.Text = "Select all";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(134, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(134, 6);
            // 
            // pullUpToolStripMenuItem
            // 
            this.pullUpToolStripMenuItem.Name = "pullUpToolStripMenuItem";
            this.pullUpToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.pullUpToolStripMenuItem.Text = "Move up";
            this.pullUpToolStripMenuItem.Click += new System.EventHandler(this.pullUpToolStripMenuItem_Click);
            // 
            // pushDownToolStripMenuItem
            // 
            this.pushDownToolStripMenuItem.Name = "pushDownToolStripMenuItem";
            this.pushDownToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.pushDownToolStripMenuItem.Text = "Move down";
            this.pushDownToolStripMenuItem.Click += new System.EventHandler(this.pushDownToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(134, 6);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(408, 58);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(56, 23);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // gbEdit
            // 
            this.gbEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbEdit.Controls.Add(this.chkAutoAdd);
            this.gbEdit.Controls.Add(this.btnGetCur);
            this.gbEdit.Controls.Add(this.txtZ);
            this.gbEdit.Controls.Add(this.lZ);
            this.gbEdit.Controls.Add(this.lY);
            this.gbEdit.Controls.Add(this.txtY);
            this.gbEdit.Controls.Add(this.txtX);
            this.gbEdit.Controls.Add(this.lX);
            this.gbEdit.Controls.Add(this.btnAdd);
            this.gbEdit.Location = new System.Drawing.Point(12, 348);
            this.gbEdit.Name = "gbEdit";
            this.gbEdit.Size = new System.Drawing.Size(470, 87);
            this.gbEdit.TabIndex = 2;
            this.gbEdit.TabStop = false;
            this.gbEdit.Text = "Add / Edit";
            // 
            // chkAutoAdd
            // 
            this.chkAutoAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkAutoAdd.AutoSize = true;
            this.chkAutoAdd.Location = new System.Drawing.Point(6, 62);
            this.chkAutoAdd.Name = "chkAutoAdd";
            this.chkAutoAdd.Size = new System.Drawing.Size(81, 17);
            this.chkAutoAdd.TabIndex = 6;
            this.chkAutoAdd.Text = "Auto record";
            this.chkAutoAdd.UseVisualStyleBackColor = true;
            this.chkAutoAdd.CheckedChanged += new System.EventHandler(this.chkAutoAdd_CheckedChanged);
            // 
            // btnGetCur
            // 
            this.btnGetCur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetCur.Location = new System.Drawing.Point(282, 58);
            this.btnGetCur.Name = "btnGetCur";
            this.btnGetCur.Size = new System.Drawing.Size(120, 23);
            this.btnGetCur.TabIndex = 7;
            this.btnGetCur.Text = "Get current position";
            this.btnGetCur.UseVisualStyleBackColor = true;
            this.btnGetCur.Click += new System.EventHandler(this.btnGetCur_Click);
            // 
            // txtZ
            // 
            this.txtZ.Location = new System.Drawing.Point(315, 32);
            this.txtZ.Name = "txtZ";
            this.txtZ.Size = new System.Drawing.Size(149, 20);
            this.txtZ.TabIndex = 4;
            this.txtZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lZ
            // 
            this.lZ.AutoSize = true;
            this.lZ.Location = new System.Drawing.Point(378, 16);
            this.lZ.Name = "lZ";
            this.lZ.Size = new System.Drawing.Size(14, 13);
            this.lZ.TabIndex = 5;
            this.lZ.Text = "Z";
            // 
            // lY
            // 
            this.lY.AutoSize = true;
            this.lY.Location = new System.Drawing.Point(226, 16);
            this.lY.Name = "lY";
            this.lY.Size = new System.Drawing.Size(14, 13);
            this.lY.TabIndex = 3;
            this.lY.Text = "Y";
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(157, 32);
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(152, 20);
            this.txtY.TabIndex = 2;
            this.txtY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(6, 32);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(145, 20);
            this.txtX.TabIndex = 0;
            this.txtX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lX
            // 
            this.lX.AutoSize = true;
            this.lX.Location = new System.Drawing.Point(73, 16);
            this.lX.Name = "lX";
            this.lX.Size = new System.Drawing.Size(14, 13);
            this.lX.TabIndex = 1;
            this.lX.Text = "X";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(408, 441);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // chkCircle
            // 
            this.chkCircle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkCircle.AutoSize = true;
            this.chkCircle.Location = new System.Drawing.Point(119, 446);
            this.chkCircle.Name = "chkCircle";
            this.chkCircle.Size = new System.Drawing.Size(76, 17);
            this.chkCircle.TabIndex = 3;
            this.chkCircle.Text = "Circle path";
            this.chkCircle.UseVisualStyleBackColor = true;
            this.chkCircle.CheckedChanged += new System.EventHandler(this.chkCircle_CheckedChanged);
            // 
            // cType
            // 
            this.cType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cType.FormattingEnabled = true;
            this.cType.Items.AddRange(new object[] {
            "Grinding",
            "Holocrypt",
            "Vendor"});
            this.cType.Location = new System.Drawing.Point(381, 11);
            this.cType.Name = "cType";
            this.cType.Size = new System.Drawing.Size(107, 21);
            this.cType.TabIndex = 0;
            this.cType.SelectedIndexChanged += new System.EventHandler(this.cType_SelectedIndexChanged);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.Location = new System.Drawing.Point(235, 10);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(35, 23);
            this.btnBrowse.TabIndex = 9;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "File:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(297, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Waypoint type:";
            // 
            // tmrAutoAdd
            // 
            this.tmrAutoAdd.Tick += new System.EventHandler(this.tmrAutoAdd_Tick);
            // 
            // txtFile
            // 
            this.txtFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFile.Location = new System.Drawing.Point(46, 12);
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(183, 20);
            this.txtFile.TabIndex = 8;
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAs.Location = new System.Drawing.Point(327, 441);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAs.TabIndex = 5;
            this.btnSaveAs.Text = "Save As";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(246, 441);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 447);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "WorldID";
            // 
            // txtWorldID
            // 
            this.txtWorldID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtWorldID.Location = new System.Drawing.Point(64, 444);
            this.txtWorldID.Name = "txtWorldID";
            this.txtWorldID.Size = new System.Drawing.Size(49, 20);
            this.txtWorldID.TabIndex = 10;
            this.txtWorldID.Text = "-1";
            this.txtWorldID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // frmWaypointEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 476);
            this.Controls.Add(this.txtWorldID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.cType);
            this.Controls.Add(this.chkCircle);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gbEdit);
            this.Controls.Add(this.lvWaypoints);
            this.Name = "frmWaypointEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Waypoint Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWaypointEditor_FormClosing);
            this.Resize += new System.EventHandler(this.frmWaypointEditor_Resize);
            this.cmWaypointEditor.ResumeLayout(false);
            this.gbEdit.ResumeLayout(false);
            this.gbEdit.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvWaypoints;
        private System.Windows.Forms.ColumnHeader chX;
        private System.Windows.Forms.ColumnHeader chY;
        private System.Windows.Forms.ColumnHeader chZ;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox gbEdit;
        private System.Windows.Forms.TextBox txtZ;
        private System.Windows.Forms.Label lZ;
        private System.Windows.Forms.Label lY;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.Label lX;
        private System.Windows.Forms.Button btnGetCur;
        private System.Windows.Forms.ContextMenuStrip cmWaypointEditor;
        private System.Windows.Forms.ToolStripMenuItem pullUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pushDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.CheckBox chkCircle;
        private System.Windows.Forms.ComboBox cType;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkAutoAdd;
        private System.Windows.Forms.Timer tmrAutoAdd;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public FileTextBox txtFile;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtWorldID;
    }
}