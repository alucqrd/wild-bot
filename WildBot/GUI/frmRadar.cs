﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

using WildBot.Tools;

namespace WildBot.GUI
{
    public partial class frmRadar : Form
    {
        public frmRadar()
        {
            InitializeComponent();

            // Load window position and size
            MassConfigSaver.LoadControl(this, WildBotObject.xmlSettings);

            // Load magnitification
            XmlNode[] nodes = XMLHandler.GetElement(WildBotObject.xmlSettings.doc, "root", "RadarMagnification");

            if (nodes != null && nodes.Length > 0)
            {
                float buf;
                if (float.TryParse(nodes.First().InnerText, out buf))
                    rdRadar.Magnitification = buf;
            }
        }

        private void frmRadar_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }

            // Save window position and size
            MassConfigSaver.SaveControl(this, WildBotObject.xmlSettings);

            // Save magnification
            XMLHandler.SetElement(WildBotObject.xmlSettings.doc, "root", "RadarMagnification", rdRadar.Magnitification);
        }
    }
}
