﻿namespace WildBot.GUI
{
    partial class frmRadar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdRadar = new WildBot.GUI.RadarDisplay();
            this.SuspendLayout();
            // 
            // rdRadar
            // 
            this.rdRadar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdRadar.BackColor = System.Drawing.Color.Black;
            this.rdRadar.Location = new System.Drawing.Point(12, 12);
            this.rdRadar.Magnitification = 3F;
            this.rdRadar.Name = "rdRadar";
            this.rdRadar.Size = new System.Drawing.Size(262, 248);
            this.rdRadar.TabIndex = 0;
            // 
            // frmRadar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 272);
            this.Controls.Add(this.rdRadar);
            this.Name = "frmRadar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Radar";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRadar_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        public RadarDisplay rdRadar;

    }
}