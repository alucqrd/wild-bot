﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;

using WildBot.AI;
using WildBot.GameObjects;
using WildBot.Tools;

namespace WildBot.GUI
{
    public partial class frmWaypointEditor : Form
    {
        #region Variables
        private GrindingSpot grindingSpot = new GrindingSpot(-1);

        private XmlFile xmlWaypoints;
        #endregion

        public frmWaypointEditor()
        {
            InitializeComponent();

            // Load config
            MassConfigSaver.LoadControl(this, WildBotObject.xmlSettings, true);

            // If there is no type selection select the first by default
            if (cType.Text == "")
                cType.SelectedIndex = 0;

            // If txtFile is not empty load it's content
            LoadFile(txtFile.Text);
        }

        public frmWaypointEditor(string file)
        {
            InitializeComponent();

            // Load config
            MassConfigSaver.LoadControl(this, WildBotObject.xmlSettings, true);

            // If there is no type selection select the first by default
            if (cType.Text == "")
                cType.SelectedIndex = 0;

            // Force set the file
            txtFile.Text = file;

            // If txtFile is not empty load it's content
            LoadFile(txtFile.Text);
        }

        #region Other
        private void DisplaySelected()
        {
            lvWaypoints.Items.Clear();

            WaypointList selected = null;
            switch (cType.Text)
            {
                case "Holocrypt":
                    selected = grindingSpot.HolocryptPath;
                    break;
                case "Grinding":
                    selected = grindingSpot.GrindingPath;
                    break;
                case "Vendor":
                    selected = grindingSpot.VendorPath;
                    break;
            }

            if (selected != null)
            {
                foreach (WorldPosition wp in selected.Positions)
                {
                    // Add to the ListView
                    ListViewItem lvi = new ListViewItem(wp.X.ToString());

                    lvi.SubItems.Add(wp.Y.ToString());
                    lvi.SubItems.Add(wp.Z.ToString());

                    lvWaypoints.Items.Add(lvi);
                }

                chkCircle.Checked = selected.IsCircle;
            }
        }

        private ListViewItem AddCoord(WorldPosition pos)
        {
            // Create the ListViewItem
            ListViewItem lvi = new ListViewItem();

            lvi.Text = pos.X.ToString();
            lvi.SubItems.Add(pos.Y.ToString());
            lvi.SubItems.Add(pos.Z.ToString());

            // Add it to the ListView
            lvWaypoints.Items.Add(lvi);

            // Add the waypoint to the list
            switch (cType.Items[cType.SelectedIndex].ToString())
            {
                case "Holocrypt":
                    grindingSpot.HolocryptPath.Positions.Add(pos);
                    break;
                case "Grinding":
                    grindingSpot.GrindingPath.Positions.Add(pos);
                    break;
                case "Vendor":
                    grindingSpot.VendorPath.Positions.Add(pos);
                    break;
            }

            return lvi;
        }

        private ListViewItem AddCoord(float X, float Y, float Z)
        {
            return AddCoord(new WorldPosition(X,Y,Z));
        }
        #endregion

        #region LoadFile and SaveFile
        private void LoadFile(string file)
        {
            if (file.Length > 0)
            {
                if (xmlWaypoints == null || xmlWaypoints.fileName != file)
                {
                    // Init waypoints file
                    xmlWaypoints = new XmlFile(file, new XmlDocument());

                    // Load XML file if it is exists
                    if (File.Exists(xmlWaypoints.fileName))
                        xmlWaypoints.doc.Load(xmlWaypoints.fileName);
                    else
                        return;
                }

                // Clear all lists
                grindingSpot.HolocryptPath.Positions.Clear();
                grindingSpot.GrindingPath.Positions.Clear();
                grindingSpot.VendorPath.Positions.Clear();

                // Load Holocrypt
                LoadHelper(grindingSpot.HolocryptPath, XMLHandler.GetElement(xmlWaypoints.doc, "root", "Holocrypt"));

                // Load Grinding
                LoadHelper(grindingSpot.GrindingPath, XMLHandler.GetElement(xmlWaypoints.doc, "root", "Grinding"), true);

                // Load Vendor
                LoadHelper(grindingSpot.VendorPath, XMLHandler.GetElement(xmlWaypoints.doc, "root", "Vendor"));

                // Load WorldID
                XmlNode[] nodes = XMLHandler.GetElement(xmlWaypoints.doc, "root", "WorldID");

                if (nodes != null && nodes.Count() > 0)
                    txtWorldID.Text = nodes.First().InnerText;

                // Display loaded waypoints
                DisplaySelected();
            }
        }

        private void LoadHelper(WaypointList wl, XmlNode[] nodes, bool canBeCircle = false)
        {
            if (nodes != null && nodes.Length > 0 && nodes.First().ChildNodes != null)
            {
                // Load waypoints
                foreach (XmlNode xn in XMLHandler.GetElement(nodes.First(), "WorldPosition"))
                {
                    float x, y, z;

                    if (float.TryParse(XMLHandler.GetAttribute(xn, "X"), out x) &&
                        float.TryParse(XMLHandler.GetAttribute(xn, "Y"), out y) &&
                        float.TryParse(XMLHandler.GetAttribute(xn, "Z"), out z))
                    {
                        wl.Positions.Add(new WorldPosition(x, y, z));
                    }
                }

                // Load if waypoints are circle or not
                XmlNode[] buf = XMLHandler.GetElement(nodes.First(), "Circle");

                if (buf.Length > 0 && buf.First().InnerText == "True")
                    wl.IsCircle = true;
                else
                    wl.IsCircle = false;
            }
        }

        private void SaveFile(string file)
        {
            if (file.Length > 0)
            {
                if (xmlWaypoints == null || xmlWaypoints.fileName != file)
                {
                    // Init waypoints file
                    xmlWaypoints = new XmlFile(file, new XmlDocument());

                    // Delete the file if it is already exists
                    if (File.Exists(xmlWaypoints.fileName))
                        File.Delete(xmlWaypoints.fileName);

                    XmlDeclaration xDec = xmlWaypoints.doc.CreateXmlDeclaration("1.0", "utf-8", null);

                    XmlElement rootNode = xmlWaypoints.doc.CreateElement("root");
                    xmlWaypoints.doc.InsertAfter(xDec, xmlWaypoints.doc.DocumentElement);
                    xmlWaypoints.doc.AppendChild(rootNode);
                }

                // Load Holocrypt
                SaveHelper(grindingSpot.HolocryptPath, "Holocrypt");

                // Load Grinding
                SaveHelper(grindingSpot.GrindingPath, "Grinding", true);

                // Load Vendor
                SaveHelper(grindingSpot.VendorPath, "Vendor");

                // Add the WorldID
                if (txtWorldID.Text == "-1")
                {
                    if (BotBrain.GameBase != null)
                        XMLHandler.SetElement(xmlWaypoints.doc, "root", "WorldID", BotBrain.GameBase.Player.CurrentWorldId);
                    else
                        XMLHandler.SetElement(xmlWaypoints.doc, "root", "WorldID", -1);
                }
                else
                    XMLHandler.SetElement(xmlWaypoints.doc, "root", "WorldID", txtWorldID.Text);

                // Save the modification
                xmlWaypoints.doc.Save(xmlWaypoints.fileName);
            }
        }

        private void SaveHelper(WaypointList wl, string name, bool canBeCircle = false)
        {
            XmlNode xn = XMLHandler.SetElement(xmlWaypoints.doc, "root", name);
            foreach (WorldPosition wp in wl.Positions)
            {
                XmlNode node = XMLHandler.SetElement(xn, "WorldPosition", null, false);
                XMLHandler.SetAttribute(node, "X", wp.X.ToString());
                XMLHandler.SetAttribute(node, "Y", wp.Y.ToString());
                XMLHandler.SetAttribute(node, "Z", wp.Z.ToString());
            }

            if (canBeCircle)
                XMLHandler.SetElement(xn, "Circle", wl.IsCircle);
        }
        #endregion

        #region Events
        private void frmWaypointEditor_Resize(object sender, EventArgs e)
        {
            // Resize the text messages
            int width = (gbEdit.Width - 22) / 3;

            txtX.Width = width;

            txtY.Left = txtX.Left + width + 5;
            txtY.Width = width;

            txtZ.Left = txtY.Left + width + 5;
            txtZ.Width = width;

            // Re positioning labels
            lX.Left = txtX.Left + (txtX.Width / 2) - (lX.Width / 2);
            lY.Left = txtY.Left + (txtY.Width / 2) - (lY.Width / 2);
            lZ.Left = txtZ.Left + (txtZ.Width / 2) - (lZ.Width / 2);
        }

        private void lvWaypoints_DoubleClick(object sender, EventArgs e)
        {
            if (lvWaypoints.SelectedItems.Count > 0)
            {
                ListViewItem lvi = lvWaypoints.SelectedItems[0];

                txtX.Text = lvi.SubItems[0].Text;
                txtY.Text = lvi.SubItems[1].Text;
                txtZ.Text = lvi.SubItems[2].Text;
            }
        }

        private void btnGetCur_Click(object sender, EventArgs e)
        {
            if (BotBrain.status >= 0)
            {
                txtX.Text = BotBrain.GameBase.Player.WorldX.ToString();
                txtY.Text = BotBrain.GameBase.Player.WorldY.ToString();
                txtZ.Text = BotBrain.GameBase.Player.WorldZ.ToString();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtX.Text.Length > 0 && txtY.Text.Length > 0 && txtZ.Text.Length > 0)
            {
                float x, y, z;

                if (float.TryParse(txtX.Text, out x) &&
                    float.TryParse(txtY.Text, out y) &&
                    float.TryParse(txtZ.Text, out z))
                {
                    AddCoord(x, y, z);

                    txtX.Text = "";
                    txtY.Text = "";
                    txtZ.Text = "";
                }
                else
                {
                    MessageBox.Show("Invalid number", "Error");
                    return;
                }
            }
            else if (BotBrain.status >= 0)
                AddCoord(BotBrain.GameBase.Player.WorldX, BotBrain.GameBase.Player.WorldY, BotBrain.GameBase.Player.WorldZ);
            else
                MessageBox.Show("Couldn't get the coordinates, please manually input one or login", "Error");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // If no file selected then select one
            if (txtFile.Text.Length <= 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Title = "Select where to save the waypoint XML file";
                sfd.InitialDirectory = SmallExtensions.InitDirectory(Application.StartupPath + @"\..\Grinding Spots");
                sfd.AddExtension = true;
                sfd.Filter = "XML files|*.xml";
                sfd.OverwritePrompt = true;

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    txtFile.Text = sfd.FileName;
                else
                    return;
            }

            MassConfigSaver.SaveControl(this, WildBotObject.xmlSettings, true, "no_lvi whitelist:(T)Form,(T)ListView");
            SaveFile(txtFile.Text);
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Select where to save the waypoint XML file";
            sfd.InitialDirectory = SmallExtensions.InitDirectory(Application.StartupPath + @"\..\Grinding Spots");
            sfd.AddExtension = true;
            sfd.Filter = "XML files|*.xml";
            sfd.OverwritePrompt = true;

            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtFile.Text = sfd.FileName;
            else
                return;

            MassConfigSaver.SaveControl(this, WildBotObject.xmlSettings, true, "no_lvi whitelist:(T)Form,(T)ListView");
            SaveFile(txtFile.Text);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            grindingSpot = new GrindingSpot(-1);
            lvWaypoints.Items.Clear();

            txtX.Text = "";
            txtY.Text = "";
            txtZ.Text = "";
            chkAutoAdd.Checked = false;
            chkCircle.Checked = false;
        }

        private void frmWaypointEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            MassConfigSaver.SaveControl(this, WildBotObject.xmlSettings);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select a waypoint XML file";
            ofd.InitialDirectory = SmallExtensions.InitDirectory(Application.StartupPath + @"\..\Grinding Spots");
            ofd.CheckFileExists = true;
            ofd.Filter = "XML files|*.xml";

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (File.Exists(ofd.FileName))
                {
                    txtFile.Text = ofd.FileName;
                    LoadFile(ofd.FileName);
                }
            }
        }

        private void cType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cType.Text == "Grinding")
                chkCircle.Visible = true;
            else
                chkCircle.Visible = false;

            DisplaySelected();
        }

        private void chkCircle_CheckedChanged(object sender, EventArgs e)
        {
            switch (cType.Text)
            {
                case "Holocrypt":
                    grindingSpot.HolocryptPath.IsCircle = chkCircle.Checked;
                    break;
                case "Grinding":
                    grindingSpot.GrindingPath.IsCircle = chkCircle.Checked;
                    break;
                case "Vendor":
                    grindingSpot.VendorPath.IsCircle = chkCircle.Checked;
                    break;
            }
        }

        private void chkAutoAdd_CheckedChanged(object sender, EventArgs e)
        {
            tmrAutoAdd.Enabled = ((CheckBox)sender).Checked;
        }

        WorldPosition lastPos = null;
        private void tmrAutoAdd_Tick(object sender, EventArgs e)
        {
            if (BotBrain.GameBase.Player != null)
            {
                if (lastPos == null || BotBrain.GameBase.Player.ComputeDistanceFromPosition(lastPos) > 10)
                {
                    AddCoord(BotBrain.GameBase.Player.WorldX, BotBrain.GameBase.Player.WorldY, BotBrain.GameBase.Player.WorldZ);
                    lastPos = new WorldPosition(BotBrain.GameBase.Player.WorldX, BotBrain.GameBase.Player.WorldY, BotBrain.GameBase.Player.WorldZ);
                }
            }
        }

        private void lvWaypoints_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (lvWaypoints.SelectedItems.Count > 0)
                {
                    cutToolStripMenuItem.Enabled = true;
                    copyToolStripMenuItem.Enabled = true;
                    pushDownToolStripMenuItem.Enabled = true;
                    pullUpToolStripMenuItem.Enabled = true;
                    deleteToolStripMenuItem.Enabled = true;
                }
                else
                {
                    cutToolStripMenuItem.Enabled = false;
                    copyToolStripMenuItem.Enabled = false;
                    pushDownToolStripMenuItem.Enabled = false;
                    pullUpToolStripMenuItem.Enabled = false;
                    deleteToolStripMenuItem.Enabled = false;
                }

                if (Clipboard.GetText().Length > 0 && Regex.IsMatch(Clipboard.GetText(), @".*([\-\+]?[0-9]+([\,\.][0-9]+)?)\s*;\s*([\-\+]?[0-9]+([\,\.][0-9]+)?)\s*;\s*([\-\+]?[0-9]+([\,\.][0-9]+)?).*"))
                    pasteToolStripMenuItem.Enabled = true;
                else
                    pasteToolStripMenuItem.Enabled = false;

                if (lvWaypoints.Items.Count > 0)
                    selectAllToolStripMenuItem.Enabled = true;
                else
                    selectAllToolStripMenuItem.Enabled = false;

                cmWaypointEditor.Show((Control)sender, e.X, e.Y);
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in lvWaypoints.Items)
                lvi.Selected = true;
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyToolStripMenuItem_Click(sender, e);
            deleteToolStripMenuItem_Click(sender, e);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string clip = "";
            foreach (ListViewItem lvi in lvWaypoints.SelectedItems)
            {
                clip += lvi.SubItems[0].Text; // X
                clip += ";";
                clip += lvi.SubItems[1].Text; // Y
                clip += ";";
                clip += lvi.SubItems[2].Text; // Z
                clip += "|";
            }
            clip = clip.TrimEnd('|');

            Clipboard.Clear();
            Clipboard.SetText(clip);
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lvWaypoints.SelectedItems.Clear();

            string clip = Clipboard.GetText();

            foreach (Match m in Regex.Matches(clip, @"([\-\+]?[0-9]+([\,\.][0-9]+)?)\s*;\s*([\-\+]?[0-9]+([\,\.][0-9]+)?)\s*;\s*([\-\+]?[0-9]+([\,\.][0-9]+)?)"))
            {
                string[] coords = Regex.Split(m.Value, @"\s*;\s*");

                float x, y, z;

                if (float.TryParse(coords[0], out x) &&
                    float.TryParse(coords[1], out y) &&
                    float.TryParse(coords[2], out z))
                {
                    AddCoord(x, y, z).Selected = true;
                }
            }
        }

        private void pullUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lvWaypoints.SelectedItems.Count > 0 && lvWaypoints.SelectedItems[0].Index > 0)
            {
                for (int i = 0; i < lvWaypoints.SelectedItems.Count; i++)
                {
                    ListViewItem lvi = lvWaypoints.SelectedItems[i];
                    int index = lvi.Index - 1;

                    lvWaypoints.Items.Remove(lvi);
                    lvWaypoints.Items.Insert(index, lvi);
                }
            }
        }

        private void pushDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lvWaypoints.SelectedItems.Count > 0 && lvWaypoints.SelectedItems[lvWaypoints.SelectedItems.Count - 1].Index < lvWaypoints.Items.Count - 1)
            {
                for (int i = lvWaypoints.SelectedItems.Count - 1; i >= 0; i--)
                {
                    ListViewItem lvi = lvWaypoints.SelectedItems[i];
                    int index = lvi.Index + 1;

                    lvWaypoints.Items.Remove(lvi);
                    lvWaypoints.Items.Insert(index, lvi);
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in lvWaypoints.SelectedItems)
            {
                WaypointList selected = null;
                switch (cType.Text)
                {
                    case "Holocrypt":
                        selected = grindingSpot.HolocryptPath;
                        break;
                    case "Grinding":
                        selected = grindingSpot.GrindingPath;
                        break;
                    case "Vendor":
                        selected = grindingSpot.VendorPath;
                        break;
                }

                if (selected != null)
                    selected.Positions.RemoveAt(lvi.Index);

                lvWaypoints.Items.Remove(lvi);
            }
        }

        bool keyHandled = false;
        private void lvWaypoints_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (keyHandled)
            {
                e.Handled = true;
                keyHandled = false;
            }
        }

        private void lvWaypoints_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && !e.Alt && !e.Shift)
            {
                keyHandled = true;

                switch (e.KeyCode)
                {
                    case Keys.A:
                        selectAllToolStripMenuItem_Click(sender, null);
                        break;
                    case Keys.X:
                        cutToolStripMenuItem_Click(sender, null);
                        break;
                    case Keys.C:
                        copyToolStripMenuItem_Click(sender, null);
                        break;
                    case Keys.V:
                        pasteToolStripMenuItem_Click(sender, null);
                        break;
                }
            }
        }
        #endregion
    }
}