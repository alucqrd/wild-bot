﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WildBot.AI;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Data altered by PathFinder static functions after computing path to reach a position.
    /// </summary>
    public class TravelData
    {
        public int CurrentIndex { get; set; }
        public bool ForwardDirection { get; set; }

        private List<State> _statesGenerated = new List<State>();
        public List<State> StatesGenerated { get { return _statesGenerated; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        public TravelData()
        {
            CurrentIndex = -1;
            ForwardDirection = true;
        }

        /// <summary>
        /// Set the priority of all previously generated states.
        /// </summary>
        /// <param name="priority"></param>
        public void SetStatesPriority(float priority)
        {
            foreach(State state in _statesGenerated)
                state.Priority = priority;
        }

        /// <summary>
        /// Add a state to the states generated list.
        /// </summary>
        /// <param name="state"></param>
        public void AddState(State state)
        {
            _statesGenerated.Add(state);
        }

        /// <summary>
        /// Reinit index of the current waypoint to -1.
        /// </summary>
        public void ResetIndex()
        {
            CurrentIndex = -1;
        }
    }
}
