﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Grinding spot list representation.
    /// </summary>
    public class GrindingSpot
    {
        private int _worldID;
        /// <summary>
        /// Id of the zone containing the Grinding spot.
        /// </summary>
        public int WorldID { get { return _worldID; } }
        /// <summary>
        /// Grinding path waypoint list.
        /// </summary>
        public WaypointList GrindingPath = new WaypointList();
        /// <summary>
        /// Holocrypt path waypoint list.
        /// </summary>
        public WaypointList HolocryptPath = new WaypointList();
        /// <summary>
        /// Vendor path waypoint list.
        /// </summary>
        public WaypointList VendorPath = new WaypointList();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="worldID"></param>
        public GrindingSpot(int worldID)
        {
            _worldID = worldID;
        }
    }

    /// <summary>
    /// Waypoint list representation. 
    /// </summary>
    public class WaypointList
    {
        /// <summary>
        /// Setting to define if path should be runned in circle or not.
        /// </summary>
        public bool IsCircle = false;
        /// <summary>
        /// List of positions.
        /// </summary>
        public List<WorldPosition> Positions = new List<WorldPosition>();

        /// <summary>
        /// Returns closest waypoint from entity passed.
        /// </summary>
        /// <param name="entity">Entity to compute closest waypoint.</param>
        /// <returns>Closest waypoint.</returns>
        public int ComputeClosestWaypointFromEntity(Entity entity)
        {
            // Compute closest waypoint
            int closestWPIndex = -1;
            float closestDistance = -1;

            for (int i = 0; i < Positions.Count; i++)
            {
                float wpDistance = entity.ComputeDistanceFromPosition(Positions[i]);

                if ((closestDistance != -1 && wpDistance < closestDistance) || closestDistance == -1)
                {
                    closestWPIndex = i;
                    closestDistance = wpDistance;
                }
            }

            return closestWPIndex;
        }
    }
}
