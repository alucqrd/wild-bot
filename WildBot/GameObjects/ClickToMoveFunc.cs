﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Experimental click to move memory writing moving function (untested).
    /// </summary>
    public class ClickToMoveFunc
    {
        #region OFFSETS
        private static uint IS_ACTIVATED_OFFSET = 0x0;
        private static uint MOVING_OFFSET = 0x4;
        private static uint X_DEST_OFFSET = 0x20;
        private static uint Y_DEST_OFFSET = 0x28;
        private static uint Z_DEST_OFFSET = 0x24;
        #endregion

        private uint _address;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="address">Function object address.</param>
        public ClickToMoveFunc(uint address)
        {
            _address = address;
        }

        /// <summary>
        /// Function to call once object has been initialized to make char move to position.
        /// </summary>
        /// <param name="X">X coord</param>
        /// <param name="Y">Y coord</param>
        /// <param name="Z">Z coord</param>
        public void ClickToMoveOnCoordinates(float X, float Y, float Z)
        {
            // Write destination coordinates in memory
            WildBotObject.MemoryReader.Write<int>(_address + X_DEST_OFFSET, X);
            WildBotObject.MemoryReader.Write<int>(_address + Y_DEST_OFFSET, Y);
            WildBotObject.MemoryReader.Write<int>(_address + Z_DEST_OFFSET, Z);

            // Run toward position
            WildBotObject.MemoryReader.Write<int>(_address + MOVING_OFFSET, 0);
        }
    }
}
