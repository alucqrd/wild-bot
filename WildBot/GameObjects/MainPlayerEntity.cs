﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WildBot.Tools;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Inherited Entity for player unit handling.
    /// </summary>
    public class MainPlayerEntity : Entity
    {
        #region Offsets
        // World Id offset from game base
        #if x86
        private static int[] WORLD_ID_OFFSETS = { 0x5ef4, 0x0 };
        private static int FIRST_SKILL_OFFSET = 0xbc4;
        private static int DISPOSITION_INFORMER_OBJECT_OFFSET = 0x8;
        private static int[] DISPOSITION_TABLE_OFFSETS = { 0xC, 0x4 };
        private static int DISPOSITION_TABLE_SUPERIOR_OFFSET = 0x8;
        private static int DISPOSITION_TABLE_INFERIOR_OFFSET = 0xC;
        private static int DISPOSITION_VALUE_COMPARED_OFFSET = 0x10;
        private static int[] DISPOSITION_TOWARD_FAMILY_OFFSETS = { 0x14, 0xC };
        #else
        private static int[] WORLD_ID_OFFSETS = { 0x5ff4, 0x0 };
        private static int FIRST_SKILL_OFFSET = 0x1028;
        private static int DISPOSITION_INFORMER_OBJECT_OFFSET = 0x10;
        private static int[] DISPOSITION_TABLE_OFFSETS = { 0x18, 0x8 };
        private static int DISPOSITION_TABLE_SUPERIOR_OFFSET = 0x10;
        private static int DISPOSITION_TABLE_INFERIOR_OFFSET = 0x18;
        private static int DISPOSITION_VALUE_COMPARED_OFFSET = 0x20;
        private static int[] DISPOSITION_TOWARD_FAMILY_OFFSETS = { 0x28, 0xC };
        #endif
        #endregion

        private uint _currentWorldId;
        /// <summary>
        /// Id of the zone where is located the player.
        /// </summary>
        public uint CurrentWorldId { get { return _currentWorldId; } }

        private PlayerInventory _inventory;
        /// <summary>
        /// Player's inventory.
        /// </summary>
        public PlayerInventory Inventory { get { return _inventory; } }

        private Dictionary<uint, Spell> _actionBar = new Dictionary<uint, Spell>();
        /// <summary>
        /// Returns dictionnary containing all spells from Action Bar. Key is the index of action bar slot (1-xxx). Value is the Spell Object.
        /// </summary>
        public Dictionary<uint, Spell> ActionBar { get { return _actionBar; } }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="address">Entity address from memory.</param>
        public MainPlayerEntity(long address)
            : base(address)
        {
        }

        public override void Update()
        {
            // Call Entity Update() method
            base.Update();

            #if x86
            string exeName = "WildStar32.exe";
            #else
            string exeName = "WildStar64.exe";
            #endif
            long gameBaseAddress = WildBotObject.MemoryReader.ReadPointer(WildBotObject.MemoryReader.GetBaseAddress(exeName) + GameBaseObject.BASE_OFFSET);

            // Update world id
            _currentWorldId = WildBotObject.MemoryReader.Read<uint>(gameBaseAddress, WORLD_ID_OFFSETS);

            // Update inventory
            _inventory = new PlayerInventory(gameBaseAddress);

            for(uint i = 0; i < 13; i++)
            {
                long currentSkillAddress = WildBotObject.MemoryReader.ReadPointer(gameBaseAddress + FIRST_SKILL_OFFSET + i * IntPtr.Size);
                if (currentSkillAddress != 0)
                {
                    if (!_actionBar.ContainsKey(i))
                        _actionBar.Add(i, new Spell(currentSkillAddress, ""));
                    else
                    {
                        if(_actionBar[i].AddressChanged(currentSkillAddress))
                        {
                            _actionBar.Remove(i);
                            _actionBar.Add(i, new Spell(currentSkillAddress, ""));
                        }

                        else
                        {
                            _actionBar[i].Update();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns angle to face entity passed (same value as the game)
        /// </summary>
        /// <param name="position">Position you want to have direction</param>
        public float ComputeDirection(WorldPosition position)
        {
            float XDiff = position.X - _worldX;
            float YDiff = position.Y - _worldY;

            float angle = -1;

            if (XDiff > 0 && YDiff < 0)
                angle = (float)Math.Atan((position.X - _worldX) / (position.Y - _worldY)) % (float)Math.PI;

            else if (XDiff > 0 && YDiff > 0)
                angle = (float)-(Math.PI - Math.Atan((position.X - _worldX) / (position.Y - _worldY))) % (float)Math.PI;

            else if (XDiff < 0 && YDiff > 0)
                angle = (float)(Math.PI / 2 - Math.Atan((position.Y - _worldY) / (position.X - _worldX))) % (float)Math.PI;

            else if (XDiff < 0 && YDiff < 0)
                angle = (float)Math.Atan((_worldX - position.X) / (_worldY - position.Y)) % (float)Math.PI; ;

            return angle;
        }

        /// <summary>
        /// Compute if we have to go left or right to face an entity, or if we already are facing it.
        /// </summary>
        /// <param name="position">Position you want to face.</param>
        /// <param name="toleranceAngle">Tolerance value.</param>
        /// <returns>Returns 1 for right and 2 for left, 0 for none</returns>
        public int IsFacingPosition(WorldPosition position, float toleranceAngle)
        {
            // Compute direction to entity
            float positionDirection = ComputeDirection(position);

            if (_facingDirection > positionDirection + toleranceAngle || _facingDirection < positionDirection - toleranceAngle)
            {
                bool facingDirectionPositive = true;
                bool entityDirectionPositive = true;

                if (_facingDirection < 0)
                    facingDirectionPositive = false;

                if (positionDirection < 0)
                    entityDirectionPositive = false;

                if ((facingDirectionPositive && entityDirectionPositive) || (!facingDirectionPositive && !entityDirectionPositive))
                {
                    if (_facingDirection > positionDirection)
                        return 1;
                    else
                        return 2;
                }

                else
                {
                    if (_facingDirection > positionDirection)
                    {
                        if (_facingDirection - positionDirection < Math.PI)
                            return 1;
                        else
                            return 2;
                    }

                    else
                    {
                        if (positionDirection - _facingDirection > Math.PI)
                            return 1;
                        else
                            return 2;
                    }
                }
            }

            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns the disposition of player toward other entity.
        /// </summary>
        /// <param name="entity">Entity that you want to know disposition toward player. 0 for hostile, 1 for neutral, 2 for friendly.</param>
        /// <returns>0 for hostile, 1 for neutral, 2 for friendly.</returns>
        public int GetDispostionTowardEntity(Entity entity)
        {
            int dispositionValue = GetDispositionValueTowardEntity(entity);

            if (dispositionValue >= 0 && dispositionValue < 3)
                return 0;
            else if (dispositionValue > 7 && dispositionValue < 11)
                return 2;

            return 1;
        }

        private int GetDispositionValueTowardEntity(Entity entity)
        {
            int dispositionValue = -1;

            if (entity.IsValid())
            {
                long entityDispositionInformerAdr = WildBotObject.MemoryReader.ReadPointer(entity.DispositionObject + DISPOSITION_INFORMER_OBJECT_OFFSET);
                while (entityDispositionInformerAdr != 0)
                {
                    // Read checked entity disposition value
                    int[] familyValueOffsets = { 0x0, 0x0 };
                    long familyValueEntity = WildBotObject.MemoryReader.Read<int>(entityDispositionInformerAdr, familyValueOffsets);

                    // Find the disposition in the various array from this entity
                    long currentDispositionInformerAddr = WildBotObject.MemoryReader.ReadPointer(DispositionObject + DISPOSITION_INFORMER_OBJECT_OFFSET);
                    while (currentDispositionInformerAddr != 0)
                    {
                        long familyTableAddr = WildBotObject.MemoryReader.ReadPointer(currentDispositionInformerAddr, DISPOSITION_TABLE_OFFSETS);
                        long currentNodeAddr = familyTableAddr;
                        while (currentNodeAddr != 0)
                        {
                            int valueCompared = WildBotObject.MemoryReader.Read<int>(currentNodeAddr + DISPOSITION_VALUE_COMPARED_OFFSET);
                            int offset;

                            // Check if we found entity value
                            if (valueCompared == familyValueEntity)
                            {
                                dispositionValue = WildBotObject.MemoryReader.Read<int>(currentNodeAddr, DISPOSITION_TOWARD_FAMILY_OFFSETS);
                                return dispositionValue;
                            }

                            // Keep searching in the tree
                            else if (valueCompared < familyValueEntity)
                                offset = DISPOSITION_TABLE_INFERIOR_OFFSET;
                            else
                                offset = DISPOSITION_TABLE_SUPERIOR_OFFSET;

                            long nextValueAddr = WildBotObject.MemoryReader.ReadPointer(currentNodeAddr + offset);

                            // If next value is different than zero, iterate of go next disposition informer
                            if (nextValueAddr != 0)
                                currentNodeAddr = nextValueAddr;
                            else
                            {
                                currentDispositionInformerAddr = WildBotObject.MemoryReader.ReadPointer(currentDispositionInformerAddr + IntPtr.Size);
                                break;
                            }
                        }
                    }

                    entityDispositionInformerAdr = WildBotObject.MemoryReader.Read<uint>(entityDispositionInformerAdr + IntPtr.Size);
                }
            }

            return dispositionValue;
        }
    }
}
