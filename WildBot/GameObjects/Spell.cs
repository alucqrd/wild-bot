﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using WildBot.Tools;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Spell modelization directly read from game informations.
    /// </summary>
    public class Spell
    {
        private static Dictionary<int, Spell> LoadedSpells = new Dictionary<int, Spell>();

#if x86
        private static uint COOLDOWN_MS_OFFSET = 0x68;
        private static uint ID_OFFSET = 0xD4;
        private static uint SPELLBASE_ID_OFFSET = 0xD8;
        private static uint IS_TRIGGERED_OFFSET = 0xB4;
        private static uint ICON_NAME_PTR_OFFSET = 0x24;
#else
        private static uint COOLDOWN_MS_OFFSET = 0x98;
        private static uint ID_OFFSET = 0x104;
        private static uint SPELLBASE_ID_OFFSET = 0x108;
        private static uint IS_TRIGGERED_OFFSET = 0xe8;
        private static uint ICON_NAME_PTR_OFFSET = 0x18;
#endif
        private long _address;
        private long _iconStrAddress;
        private long IconStrAddress { get { return _iconStrAddress; } }
        /// <summary>
        /// Key bound to cast the spell.
        /// </summary>
        public string KeyBound;

        private int _id = -1;
        private int _SpellBaseId = -1;

        private string _name = "error";
        public string Name { get { return _name; } }

        private int _tier = -1;
        public int Tier { get { return _tier; } }

        private int _castTime = -1;
        public int CastTime { get { return _castTime; } }

        private int _duration = -1;
        public int Duration { get { return _duration; } }

        private int _spellTimerMilliseconds;
        public int CooldownRemainingMilliseconds { get { return _spellTimerMilliseconds; } }
        public int CastTimeRemainingMilliseconds { get { return _spellTimerMilliseconds; } }

        private int _spellCooldown = -1;
        public int SpellCooldown { get { return _spellCooldown; } }

        private float _minRange = -1;
        public float MinRange { get { return _minRange; } }

        private float _maxRange = -1;
        public float MaxRange { get { return _maxRange; } }

        private bool _triggered = false;
        public bool Triggered { get { return _triggered; } }
        /// <summary>
        /// Summoned flag, only useful for pet summoning spells.
        /// </summary>
        public bool Summoned = false;

        public Spell(long address, string key)
        {
            _address = address;
            KeyBound = key;

            Update();
        }

        public void Update()
        {
            _id = WildBotObject.MemoryReader.Read<int>(_address + ID_OFFSET);
            _SpellBaseId = WildBotObject.MemoryReader.Read<int>(_address + SPELLBASE_ID_OFFSET);
            _spellTimerMilliseconds = WildBotObject.MemoryReader.Read<int>(_address + COOLDOWN_MS_OFFSET);
            _iconStrAddress = WildBotObject.MemoryReader.ReadPointer(_address + ICON_NAME_PTR_OFFSET); ;
            int triggered = WildBotObject.MemoryReader.Read<int>(_address + IS_TRIGGERED_OFFSET);
            if (triggered != 0)
                _triggered = true;
            else
                _triggered = false;


            // Load spell from database if needed
            LoadSpell();
        }

        private void LoadSpell()
        {
            // If we already loaded the spell from database
            if (LoadedSpells.ContainsKey(_id))
            {
                Spell loadedSpell = LoadedSpells[_id];
                _name = loadedSpell.Name;
                _castTime = loadedSpell._castTime;
                _duration = loadedSpell.Duration;
                _spellCooldown = loadedSpell._spellCooldown;
                _minRange = loadedSpell.MinRange;
                _maxRange = loadedSpell.MaxRange;
                _tier = loadedSpell.Tier;
            }

            // Or retrieve spell from database
            else
            {
                // Query in Spell4 db
                String query = "select ID, description, tierIndex, castTime, spellDuration, spellCoolDown, targetMinRange, targetMaxRange, localizedTextIdActionBarTooltip, localizedTextIdPrimaryTargetIconSpellText, localizedTextIdCasterIconSpellText ";
                query += "from Spell4 ";
                query += "where ID = " + _id + ";";
                DataTable dt = DatabaseController.Query(query);
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    string[] description = dt.Rows[0]["description"].ToString().Split('-');
                    string spellClass = description[0].Trim();
                    _name = description[1].Trim();
                    string tierBase = description[2].Trim();

                    _castTime = int.Parse(dt.Rows[0]["castTime"].ToString());
                    _duration = int.Parse(dt.Rows[0]["spellDuration"].ToString());
                    _spellCooldown = int.Parse(dt.Rows[0]["spellCoolDown"].ToString());
                    _minRange = float.Parse(dt.Rows[0]["targetMinRange"].ToString());
                    _maxRange = float.Parse(dt.Rows[0]["targetMaxRange"].ToString());
                    _tier = int.Parse(dt.Rows[0]["tierIndex"].ToString());
                }

                // Retrieve spell's english name
                query = "select ID, localizedTextIdName ";
                query += "from Spell4Base ";
                query += "where ID = " + _SpellBaseId + ";";
                dt = DatabaseController.Query(query);
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    int translationId = int.Parse(dt.Rows[0]["localizedTextIdName"].ToString());

                    // Query in english text db
                    query = "select ID, localizedText ";
                    query += "from TEXT_US ";
                    query += "where ID = " + translationId + ";";
                    DataTable dt2 = DatabaseController.Query(query);
                    if (dt2.Rows.Count > 0)
                    {
                        _name = dt2.Rows[0]["localizedText"].ToString();
                    }
                }

                // Add this reference to loaded spells
                LoadedSpells.Add(_id, this);
            }
        }

        /// <summary>
        /// Return false if spell is under cooldown or being casted.
        /// </summary>
        /// <returns></returns>
        public bool IsCastable()
        {
            if (_spellTimerMilliseconds > 0)
                return false;

            return true;
        }

        /// <summary>
        /// Used to check if spell address changed.
        /// </summary>
        /// <param name="testedAddress"></param>
        /// <returns></returns>
        public bool AddressChanged(long testedAddress)
        {
            if (_address != testedAddress)
                return true;
            else
                return false;
        }
    }
}