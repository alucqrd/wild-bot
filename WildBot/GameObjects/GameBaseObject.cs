﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Game base object modelized.
    /// </summary>
    public class GameBaseObject
    {
        #region OFFSETS
        #if x86
        public static int BASE_OFFSET = 0x8762F0;
        private static int PLAYER_OFFSET = 0x3c;
        private static int FIRST_ENTITY_OFFSET = 0x5ca8;
        private static int CAN_VACUUM_OFFSET = 0x577c;
        #else
        public static int BASE_OFFSET = 0xAB7B48;
        private static int PLAYER_OFFSET = 0x58;
        private static int FIRST_ENTITY_OFFSET = 0x73a8;
        private static int CAN_VACUUM_OFFSET = 0x6b54;
#endif
        #endregion

        private long _address;

        /// <summary>
        /// Return game base address.
        /// </summary>
        public long Address { get { return _address; } }
        Entity _firstEntityFromList;
        
        private MainPlayerEntity _player = null;
        /// <summary>
        /// Returns main player. Null if player is not loaded yet.
        /// </summary>
        public MainPlayerEntity Player { get { return _player; } }

        private Dictionary<uint, Entity> _entityList = new Dictionary<uint, Entity>();
        /// <summary>
        /// Loaded entity list.
        /// </summary>
        public Dictionary<uint, Entity> EntityList { get { return _entityList; } }

        // private ClickToMoveFunc _clickToMove;
        // public ClickToMoveFunc ClickToMove { get { return _clickToMove; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        public GameBaseObject()
        {
            #if x86
            string exeName = "WildStar32.exe";
            #else
            string exeName = "WildStar64.exe";
            #endif
            _address = WildBotObject.MemoryReader.ReadPointer(WildBotObject.MemoryReader.GetBaseAddress(exeName) + BASE_OFFSET);

            // Init click to move func (not used atm)
            // _clickToMove = new ClickToMoveFunc(_address + CLICK_TO_MOVE_OFFSET);

            // Update Game base object
            Update();
        }

        /// <summary>
        /// Update the entities linked list
        /// </summary>
        public void Update()
        {
            // Update player
            // Check if player address changed
            long playerAddress = WildBotObject.MemoryReader.ReadPointer(_address + PLAYER_OFFSET);
            if (_player == null || playerAddress != _player.Address)
                _player = new MainPlayerEntity(WildBotObject.MemoryReader.ReadPointer(_address + PLAYER_OFFSET));
            else
                _player.Update();
            
            // First clear entity list
            _entityList.Clear();

            // Read first entity from list's address
            _firstEntityFromList = new Entity(WildBotObject.MemoryReader.ReadPointer(_address + FIRST_ENTITY_OFFSET));
             
            // Read linked list from first to end
            Entity currentEntity = _firstEntityFromList;
            while(currentEntity!=null)
            {
                if (!_entityList.ContainsKey(currentEntity.GUID))
                    _entityList.Add(currentEntity.GUID, currentEntity);
                currentEntity = currentEntity.GetNextEntity();
            }
        }

        /// <summary>
        /// Return true if there are items to vacuum.
        /// </summary>
        /// <returns></returns>
        public bool CanVacuum()
        {
            return WildBotObject.MemoryReader.Read<bool>(_address + CAN_VACUUM_OFFSET);
        }
    }
}
