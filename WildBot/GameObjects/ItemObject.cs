﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Item representation.
    /// </summary>
    public class ItemObject
    {
        #if x86
        private static uint INDEX_OFFSET = 0x24;
        private static uint STACK_SIZE_OFFSET = 0x58;
        #else
        private static uint INDEX_OFFSET = 0x24;
        private static uint STACK_SIZE_OFFSET = 0x68;
        #endif
        private long _address;

        private uint _index;
        public uint Index { get { return _index; } } 

        private uint _stackSize;
        public uint StackSize { get { return _stackSize; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="address">Item address from memory.</param>
        public ItemObject(long address)
        {
            _address = address;
            Update();
        }

        /// <summary>
        /// Update data method.
        /// </summary>
        public void Update()
        {
            _index = WildBotObject.MemoryReader.Read<uint>(_address + INDEX_OFFSET);
            _stackSize = WildBotObject.MemoryReader.Read<uint>(_address + STACK_SIZE_OFFSET);
        }
    }
}
