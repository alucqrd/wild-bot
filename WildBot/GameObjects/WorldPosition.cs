﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.GameObjects
{
    /// <summary>
    /// World Position representation
    /// </summary>
    public class WorldPosition
    {
        public float X;
        public float Y;
        public float Z;

        public WorldPosition()
        {
        }

        public WorldPosition(float X, float Y, float Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        /// <summary>
        /// Returns distance from position.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public float Distance(WorldPosition position)
        {
            return (float)Math.Sqrt(Math.Pow(position.X - this.X, 2) + Math.Pow(position.Y - this.Y, 2) + Math.Pow(position.Z - this.Z, 2));
        }

        /// <summary>
        /// Returns distance from a position defined by X, Y and Z coords.
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Z"></param>
        /// <returns></returns>
        public float Distance(float X, float Y, float Z)
        {
            return (float)Math.Sqrt(Math.Pow(X - this.X, 2) + Math.Pow(Y - this.Y, 2) + Math.Pow(Z - this.Z, 2));
        }
    }
}
