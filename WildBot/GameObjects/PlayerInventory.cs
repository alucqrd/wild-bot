﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Player inventory's representation.
    /// </summary>
    public class PlayerInventory
    {
        #region OFFSETS
        #if x86
        private static uint INVENTORY_SIZE_OFFSET = 0x70;
        private static uint INVENTORY_OFFSET = 0x74;
        #else
        private static uint INVENTORY_SIZE_OFFSET = 0xa4;
        private static uint INVENTORY_OFFSET = 0xa8;
        #endif
        #endregion

        private long _gameBaseAddress;
        private List<ItemObject> _itemList = new List<ItemObject>();
        private uint _size;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="gameBaseAddress"></param>
        public PlayerInventory(long gameBaseAddress)
        {
            _gameBaseAddress = gameBaseAddress;

            Update();
        }

        public void Update()
        {
            // Read size from game base
            _size = WildBotObject.MemoryReader.Read<uint>(_gameBaseAddress + INVENTORY_SIZE_OFFSET);
            
            // Read inventory
            long inventoryAddress = WildBotObject.MemoryReader.ReadPointer(_gameBaseAddress + INVENTORY_OFFSET);
            for(uint i = 0; i < _size; i++)
            {
                long itemptr = WildBotObject.MemoryReader.ReadPointer(inventoryAddress + i * IntPtr.Size);
                if (itemptr != 0)
                    _itemList.Add(new ItemObject(itemptr));
            }
        }

        /// <summary>
        /// Return true if player's inventory is full, else return false.
        /// </summary>
        /// <returns>True if player's inventory is full, else return false.</returns>
        public bool IsFull()
        {
            if (_itemList.Count == _size)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Returns remaining slots from inventory.
        /// </summary>
        /// <returns>Remaining slots from inventory.</returns>
        public int RemainingSlots()
        {
            return (int)(_size - _itemList.Count);
        }
    }
}
