﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WildBot.AI;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Game Unit class model with some utility function (distance and direction computing).
    /// </summary>
    public class Entity
    {
        #region Offsets
        #if x86
        private static uint GUID_OFFSET = 0x4;
        private static uint NAME_OFFSET = 0x8;
        private static uint LEVEL_OFFSET = 0x28;
        private static uint LIFE_OFFSET = 0x30;
        private static uint MAX_LIFE_OFFSET = 0x3d4;
        private static uint SHIELD_OFFSET = 0x38;
        private static uint CLASS_RESSOURCE_OFFSET = 0x184;
        private static uint NEXT_ENTITY_OFFSET = 0x48;
        private static uint UNIT_TYPE_OFFSET = 0x58;
        private static uint TARGET_GUID_OFFSET = 0xb0;
        private static uint DISPOSITION_OBJECT_OFFSET = 0xB8;
        private static uint RACE_ID_OFFSET = 0x8c;
        private static uint CLASS_ID_OFFSET = 0x90;
        private static uint OWNER_GUID_OFFSET = 0x1f0;
        private static uint IS_IN_COMBAT_OFFSET = 0x1ec;
        private static uint IS_CASTING_OFFSET = 0x126C;
        private static uint IS_DEAD_OFFSET = 0x1a4;
        private static uint CCTABLE_OFFSET = 0x204;
        private static uint FACING_DIRECTION_OFFSET = 0xf50;
        private static uint WORLD_X_OFFSET = 0xec0;
        private static uint WORLD_Y_OFFSET = 0xec8;
        private static uint WORLD_Z_OFFSET = 0xec4;
        #else
        private static uint GUID_OFFSET = 0x8;
        private static uint NAME_OFFSET = 0x10;
        private static uint LEVEL_OFFSET = 0x38;
        private static uint LIFE_OFFSET = 0x40;
        private static uint MAX_LIFE_OFFSET = 0x5b0;
        private static uint SHIELD_OFFSET = 0x48;
        private static uint CLASS_RESSOURCE_OFFSET = 0x214;
        private static uint NEXT_ENTITY_OFFSET = 0x60;
        private static uint UNIT_TYPE_OFFSET = 0x80;
        private static uint TARGET_GUID_OFFSET = 0x108;
        private static uint DISPOSITION_OBJECT_OFFSET = 0x110;
        private static uint RACE_ID_OFFSET = 0xd8;
        private static uint CLASS_ID_OFFSET = 0xdc;
        private static uint OWNER_GUID_OFFSET = 0x2a0;
        private static uint IS_IN_COMBAT_OFFSET = 0x29c;
        private static uint IS_CASTING_OFFSET = 0x1628;
        private static uint IS_DEAD_OFFSET = 0x234;
        private static uint CCTABLE_OFFSET = 0x2C8;
        private static uint FACING_DIRECTION_OFFSET = 0xff0;
        private static uint WORLD_X_OFFSET = 0xec0;
        private static uint WORLD_Y_OFFSET = 0xec8;
        private static uint WORLD_Z_OFFSET = 0xec4;
#endif
        #endregion

        #region Enums
        //public enum ClassId { Warrior = 1, Engineer, Esper, Medic, Stalker, Spellslinger }
        #endregion

        #region Variables

        protected long _address;
        public long Address { get { return _address; } }

        protected uint _GUID;
        public uint GUID { get { return _GUID; } }
        protected string _name;
        public string Name { get { return _name; } }

        protected float _worldX;
        public float WorldX { get { return _worldX; } }
        protected float _worldY;
        public float WorldY { get { return _worldY; } }
        protected float _worldZ;
        public float WorldZ { get { return _worldZ; } }
        public WorldPosition Position { get { return new WorldPosition(WorldX, WorldY, WorldZ); } }

        protected uint _level;
        public uint Level { get { return _level; } }
        protected uint _life;
        public uint Life { get { return _life; } }
        protected uint _shield;
        public uint Shield { get { return _shield; } }
        protected float _lifeMax;
        public float LifeMax { get { return _lifeMax; } }
        protected uint _mana;
        protected float _classResource;
        public float ClassResource { get { return _classResource; } }
        protected uint _unitType;
        public uint UnitType { get { return _unitType; } }
        protected uint _raceID;
        protected uint _classID;
        public uint ClassID { get { return _classID; } }
        protected uint _targetGUID;
        public uint TargetGUID { get { return _targetGUID; } }
        protected uint _ownerGUID;
        public uint OwnerGUID { get { return _ownerGUID; } }

        protected bool _isDead;
        public bool IsDead { get { return _isDead; } }
        protected bool _isInCombat;
        public bool IsInCombat { get { return _isInCombat; } }
        protected bool _isCasting;
        public bool IsCasting { get { return _isCasting; } }

        protected float _facingDirection;
        public float FacingDirection { get { return _facingDirection; } }

        private uint _petSummonedNumber = 0;
        public uint PetSummonedNumber { get { return _petSummonedNumber; } }

        protected CCStateTableObject _ccStateTable;
        /// <summary>
        /// Crowd Control states affecting the entity.
        /// </summary>
        public CCStateTableObject CCStateTable { get { return _ccStateTable; } }

        private long _dispositionObject;
        public long DispositionObject { get { return _dispositionObject; } }

        protected long _pointerToEntityAddress;
        protected long _nextEntityAddr;

        #endregion

        #region Member functions
        /// <summary>
        /// Entity constructor
        /// </summary>
        /// <param name="address">Address of the entity in memory</param>
        public Entity(long address)
        {
            _address = address;

            Update();
        }

        /// <summary>
        /// Update infos by reading memory.
        /// </summary>
        public virtual void Update()
        {
            // Read GUID
            _GUID = WildBotObject.MemoryReader.Read<uint>(_address + GUID_OFFSET);

            // Read name
            long nameAddr = WildBotObject.MemoryReader.ReadPointer(_address + NAME_OFFSET);
            _name = WildBotObject.MemoryReader.ReadString(nameAddr);

            // Read Level
            _level = WildBotObject.MemoryReader.Read<uint>(_address + LEVEL_OFFSET);

            // Read Life
            _life = WildBotObject.MemoryReader.Read<uint>(_address + LIFE_OFFSET);

            // Read Max Life
            _lifeMax = WildBotObject.MemoryReader.Read<float>(_address + MAX_LIFE_OFFSET);

            // Read Shield
            _shield = WildBotObject.MemoryReader.Read<uint>(_address + SHIELD_OFFSET);

            // Read Mana
            _mana = WildBotObject.MemoryReader.Read<uint>(_address + 0x188);

            // Read class resource
            _classResource = WildBotObject.MemoryReader.Read<float>(_address + CLASS_RESSOURCE_OFFSET);

            // Read unit type
            _unitType = WildBotObject.MemoryReader.Read<uint>(_address + UNIT_TYPE_OFFSET);

            // Read Target GUID
            _targetGUID = WildBotObject.MemoryReader.Read<uint>(_address + TARGET_GUID_OFFSET);

            // Read race id
            _raceID = WildBotObject.MemoryReader.Read<uint>(_address + RACE_ID_OFFSET);

            // Read class id
            _classID = WildBotObject.MemoryReader.Read<uint>(_address + CLASS_ID_OFFSET);

            // Read owner guid (for minion / pet)
            _ownerGUID = WildBotObject.MemoryReader.Read<uint>(_address + OWNER_GUID_OFFSET);

            // Read if entity is dead
            uint isDeadByte = WildBotObject.MemoryReader.Read<uint>(_address + IS_DEAD_OFFSET);
            if (isDeadByte == 0)
                _isDead = false;
            else
                _isDead = true;

            // Read if entity is in combat
            byte isInCombatByte = WildBotObject.MemoryReader.Read<byte>(_address + IS_IN_COMBAT_OFFSET);
            if (isInCombatByte == 0)
                _isInCombat = false;
            else
                _isInCombat = true;

            // Read if entity is casting
            uint isCastingValue = WildBotObject.MemoryReader.Read<uint>(_address + IS_CASTING_OFFSET);
            if (isCastingValue == 0)
                _isCasting = false;
            else
                _isCasting = true;

            // Read world coordinates
            _worldX = WildBotObject.MemoryReader.Read<float>(_address + WORLD_X_OFFSET);
            _worldY = WildBotObject.MemoryReader.Read<float>(_address + WORLD_Y_OFFSET);
            _worldZ = WildBotObject.MemoryReader.Read<float>(_address + WORLD_Z_OFFSET);

            // Fill State Table
            _ccStateTable = new CCStateTableObject(_address + CCTABLE_OFFSET);

            // Read parent container
            // _pointerToEntityAddress = WildBotObject.MemoryReader.ReadPointer(_address + 0x50, new uint[] { 0 });

            // Read next entity
            _nextEntityAddr = WildBotObject.MemoryReader.ReadPointer(_address + NEXT_ENTITY_OFFSET);

            // Read Disposition object address
            _dispositionObject = WildBotObject.MemoryReader.ReadPointer(_address + DISPOSITION_OBJECT_OFFSET);
        }

        /// <summary>
        /// Update Pet Summoned number (0 by default).
        /// </summary>
        public void UpdatePetSummoned()
        {
            _petSummonedNumber = 0;

            Dictionary<uint, Entity> entityList = BotBrain.GameBase.EntityList;
            foreach (KeyValuePair<uint, Entity> entity in entityList)
            {
                if (entity.Value.OwnerGUID == GUID)
                    _petSummonedNumber++;
            }
        }

        /// <summary>
        /// Returns next Entity from linked list.
        /// </summary>
        /// <returns>null if no entity is linked.</returns>
        public Entity GetNextEntity()
        {
            Entity nextEntity;
            if (_nextEntityAddr != 0)
                nextEntity = new Entity(_nextEntityAddr);
            else
                nextEntity = null;

            return nextEntity;
        }

        /// <summary>
        /// Compute distance between the entity and another one.
        /// </summary>
        /// <param name="entity">Other entity to compute distance from.</param>
        /// <returns>Distance between the two entities</returns>
        public float ComputeDistanceFromEntity(Entity entity)
        {
            if (entity == null)
                return -1;
            float distance = (float)Math.Sqrt((Math.Pow(_worldX - entity._worldX, 2) + Math.Pow(_worldY - entity._worldY, 2) + Math.Pow(_worldZ - entity._worldZ, 2)));
            return distance;
        }

        /// <summary>
        /// Compute distance between the entity and a position in the world.
        /// </summary>
        /// <param name="wp">Position in the world.</param>
        /// <returns>Distance between entity and position.</returns>
        public float ComputeDistanceFromPosition(WorldPosition wp)
        {
            float distance = (float)Math.Sqrt((Math.Pow(_worldX - wp.X, 2) + Math.Pow(_worldY - wp.Y, 2) + Math.Pow(_worldZ - wp.Z, 2)));
            return distance;
        }

        /// <summary>
        /// Checks if entity is close from a path, depending of the distance specified on argument.
        /// </summary>
        /// <param name="path">Path to check.</param>
        /// <param name="maxDistance">Distance to check from each waypoint of the path.</param>
        /// <returns></returns>
        public bool IsCloseFromPath(WaypointList path, float maxDistance)
        {
            int closestWPIndex = path.ComputeClosestWaypointFromEntity(this);
            float distance = ComputeDistanceFromPosition(path.Positions[closestWPIndex]);
            if (distance < maxDistance)
                return true;
            else
                return false;
        }

        public void UpdateFacingDirection()
        {
            _facingDirection = WildBotObject.MemoryReader.Read<float>(_address + FACING_DIRECTION_OFFSET);
        }

        /// <summary>
        /// Verify that entity is still valid in memory.
        /// </summary>
        /// <returns>True if entity is still valid. False if not.</returns>
        public bool IsValid()
        {
            if(WildBotObject.MemoryReader.Read<uint>(_address + GUID_OFFSET) == _GUID)
                return true;
            else
                return false;
        }
        #endregion
    }
}