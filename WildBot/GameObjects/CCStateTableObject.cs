﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.GameObjects
{
    /// <summary>
    /// Object containing all the Crowd Control states of an entity.
    /// </summary>
    public class CCStateTableObject
    {
        private long _address;
        public bool Stun;
        public bool Sleep;
        public bool Root;
        public bool Disarm;
        public bool Silence;
        public bool Polymorph;
        public bool Fear;
        public bool Hold;
        public bool Knockdown;
        public bool Vulnerability;
        public bool VulnerabilityWithAct;
        public bool Disorient;
        public bool Disable;
        public bool Taunt;
        public bool DeTaunt;
        public bool Blind;
        public bool Knockback;
        public bool Pushback;
        public bool Pull;
        public bool PositionSwitch;
        public bool Tether;
        public bool Snare;
        public bool Interrupt;
        public bool Daze;
        public bool Subdue;
        public bool Grounded;
        public bool DisableCinematic;
        public bool AbilityRestriction;

        public CCStateTableObject(long address)
        {
            _address = address;
            Update();
        }

        public void Update()
        {
            long currentAddress = _address;
            Stun = GetState(currentAddress);
            currentAddress += 8;
            Sleep = GetState(currentAddress);
            currentAddress += 8;
            Root = GetState(currentAddress);
            currentAddress += 8;
            Disarm = GetState(currentAddress);
            currentAddress += 8;
            Silence = GetState(currentAddress);
            currentAddress += 8;
            Polymorph = GetState(currentAddress);
            currentAddress += 8;
            Fear = GetState(currentAddress);
            currentAddress += 8;
            Hold = GetState(currentAddress);
            currentAddress += 8;
            Knockdown = GetState(currentAddress);
            currentAddress += 8;
            Vulnerability = GetState(currentAddress);
            currentAddress += 8;
            VulnerabilityWithAct = GetState(currentAddress);
            currentAddress += 8;
            Disorient = GetState(currentAddress);
            currentAddress += 8;
            Disable = GetState(currentAddress);
            currentAddress += 8;
            Taunt = GetState(currentAddress);
            currentAddress += 8;
            DeTaunt = GetState(currentAddress);
            currentAddress += 8;
            Blind = GetState(currentAddress);
            currentAddress += 8;
            Knockback = GetState(currentAddress);
            currentAddress += 8;
            Pushback = GetState(currentAddress);
            currentAddress += 8;
            Pull = GetState(currentAddress);
            currentAddress += 8;
            PositionSwitch = GetState(currentAddress);
            currentAddress += 8;
            Tether = GetState(currentAddress);
            currentAddress += 8;
            Snare = GetState(currentAddress);
            currentAddress += 8;
            Interrupt = GetState(currentAddress);
            currentAddress += 8;
            Daze = GetState(currentAddress);
            currentAddress += 8;
            Subdue = GetState(currentAddress);
            currentAddress += 8;
            Grounded = GetState(currentAddress);
            currentAddress += 8;
            DisableCinematic = GetState(currentAddress);
            currentAddress += 8;
            AbilityRestriction = GetState(currentAddress);
        }

        private bool GetState(long address)
        {
            if (WildBotObject.MemoryReader.Read<byte>(address) == 0)
                return false;
            else
                return true;
        }
    }
}
