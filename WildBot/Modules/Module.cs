﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using System.Diagnostics;

using WildBot.GUI;
using WildBot.GameObjects;
using WildBot.Tools;

namespace WildBot.Modules
{
    /// <summary>
    /// This is the class that needs to be inherited by any user defined script (in folder Modules). 
    /// To make a module, user has to override Run method and put inside the code that needs to be executed each AI Loop.
    /// Class implements a lot of method to make script developper's life easier while working on GUI stuff.
    /// To have some example of how to use this class while scripting, you can check GrindingModule.cs or VendoringModule.cs files from Modules folder.
    /// </summary>
    public abstract class Module
    {
        // Logger initialization
        /// <summary>
        /// Logger that can be used to print message in the console or log files.
        /// </summary>
        protected Logger log = new Logger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// If set to true, module will be runned even if bot is paused (false by default).
        /// </summary>
        public bool RunIfBotPaused = false;

        public bool Activated = false;

        /// <summary>
        /// Method that is runned each AI loop.
        /// This method needs to be overriden in any child class.
        /// </summary>
        public abstract void Run();

        /// <summary>
        /// Will run this when the module is getting enabled from the Module tab
        /// </summary>
        public virtual void OnEnable()
        {
        }

        /// <summary>
        /// Will run this when the module is getting disabled from the Module tab
        /// </summary>
        public virtual void OnDisable()
        {
        }

        #region APIs
        #region GUI
        /// <summary>
        /// Get the form from name
        /// </summary>
        /// <param name="name">Name of the form</param>
        /// <returns>The form</returns>
        public frmBase GetWindow(string name = "")
        {
            if (name.Length <= 0)
                name = this.GetType().Name;

            foreach (KeyValuePair<frmBase, string> kv in WildBotObject.ModuleGUIs)
            {
                if (kv.Key.Name == name)
                    return kv.Key;
            }

            return null;
        }

        /// <summary>
        /// Will create a form
        /// </summary>
        /// <param name="config">Config file for the GUI to save and load from, returned from InitializeConfig</param>
        /// <param name="name">Name of the GUI, leave empty ("") to auto name it</param>
        /// <param name="title">Caption of the window, leave empty ("") to auto name it</param>
        /// <param name="width">Width of the window (client size), -1 for auto size</param>
        /// <param name="height">Height of the window (client size), -1 for auto size</param>
        /// <param name="left">Position of the window (left side), -1 for auto center</param>
        /// <param name="top">Position of the window (top side), -1 for auto center</param>
        public frmBase CreateGUI(XmlFile config = null, string name = "", string title = "", int width = -1, int height = -1, int left = -1, int top = -1)
        {
            // If name or caption is empty auto name them
            if (name.Length <= 0)
                name = this.GetType().Name;

            if (title.Length <= 0)
                title = this.GetType().Name;

            // Delete previously created GUI with the same name
            foreach (KeyValuePair<frmBase, string> kv in WildBotObject.ModuleGUIs.Reverse())
            {
                if (kv.Key.Name == name)
                {
                    WildBotObject.ModuleGUIs.Remove(kv.Key);
                    kv.Key.Dispose();
                }
            }

            // Create the GUI
            frmBase Window = new frmBase();

            // If config is given then load it to the GUI
            if (config != null)
                Window.xmlSettings = config;

            // Set the title and name
            Window.Name = name;
            Window.Text = title;

            // Set window position or auto load from settings
            if (left >= 0 || top >= 0)
            {
                Window.StartPosition = FormStartPosition.Manual;

                Window.Left = left;
                Window.Top = top;
            }

            // Set window width or set auto size
            if (width > 0)
            {
                Window.Width = width;
                Window.autoWidth = false;
            }
            else
                Window.autoWidth = true;

            // Set window height or set auto size
            if (height > 0)
            {
                Window.Height = height;
                Window.autoHeight = false;
            }
            else
                Window.autoHeight = true;

            // Add window to the window list
            WildBotObject.ModuleGUIs.Add(Window, this.GetType().Name);

            // Return the newly created window
            return Window;
        }

        private string GetCommand(string str, string cmd)
        {
            int index = str.ToLower().IndexOf(cmd.ToLower());
            if (index >= 0)
            {
                bool waitForQuiteMark = false;
                int start = -1, end = -1;
                for (int i = index + cmd.Length + 1; i < str.Length; i++)
                {
                    if (str[i] != ' ')
                    {
                        if (start < 0)
                        {
                            if (str[i] == '\"')
                            {
                                waitForQuiteMark = true;
                                i++;
                            }

                            start = i;
                        }
                    }
                    
                    if (start > 0 && (str[i] == ' ' | str[i] == '\"'))
                    {
                        if ((waitForQuiteMark && str[i] == '\"') | (!waitForQuiteMark))
                        {
                            end = i;
                            break;
                        }
                    }
                }

                if (end < 0)
                    end = str.Length;

                if (start > 0)
                    return str.Substring(start, end - start);
            }

            return "";
        }

        /// <summary>
        /// Creates a control to the given window, may auto-name, size and place it
        /// </summary>
        /// <typeparam name="T">Type of the Control, like TextBox or Label</typeparam>
        /// <param name="gui">The form to add the control to</param>
        /// <param name="name">Name of the control, leave empty for auto naming, auto name will be like textBox1, textBox2 etc</param>
        /// <param name="value">Values of the Control, for TextBox and Label it's just the Text, but for TrackBar and ProgressBar you can specify the min and the max value like that: Minimum:100 Maximum:1000 Value:100</param>
        /// <param name="width">Width of the Control, auto size will be based on the Text parameter</param>
        /// <param name="height">Height of the Control, auto size will be based on the font's height</param>
        /// <param name="left">Position of the Control, auto placement: Labels will be placed in new line and each other Control will follow each other in that line</param>
        /// <param name="top">Position of the Control, auto placement: Labels will be placed in new line and each other Control will follow each other in that line</param>
        /// <returns>The control created</returns>
        public dynamic Create<T>(Form gui, string name = "", string value = "", int width = -1, int height = -1, int left = -1, int top = -1) where T : new()
        {
            // Create the control
            dynamic ret = new T();

            if (ret != null)
            {
                if (name.Length <= 0)
                {
                    try
                    {
                        string buf = ret.GetType().Name;

                        name = buf.Substring(0, 1).ToLower();
                        name += buf.Substring(1);
                    }
                    catch
                    {
                        name = "unknown";
                    }

                    int num = 1;
                    foreach (Control ctrl in gui.Controls)
                    {
                        if (ctrl.Name.Length >= name.Length)
                        {
                            if (ctrl.Name.Substring(0, name.Length) == name)
                                num++;
                        }
                    }

                    name += num;
                }

                ret.Name = name;

                switch ((string)ret.GetType().Name)
                {
                    case "ComboBox":
                        string sel = GetCommand(value, "selected");
                        string[] items = GetCommand(value, "items").Split('|');

                        foreach (string str in items)
                            ret.Items.Add(str);

                        for (int i = 0; i < ret.Items.Count; i++)
                        {
                            if (ret.Items[i].ToString() == sel)
                            {
                                ret.SelectedIndex = i;
                                break;
                            }
                        }

                        switch (GetCommand(value, "dropdownstyle").ToLower())
                        {
                            case "dropdownlist":
                                ret.DropDownStyle = ComboBoxStyle.DropDownList;
                                break;
                            case "dropdown":
                                ret.DropDownStyle = ComboBoxStyle.DropDown;
                                break;
                            case "simple":
                                ret.DropDownStyle = ComboBoxStyle.Simple;
                                break;
                        }
                        break;
                    case "RadioButton":
                    case "CheckBox":
                        ret.Checked = value == "true" ? true : false;
                        break;
                    case "ProgressBar":
                    case "TrackBar":
                    case "NumericUpDown":
                        int min, max, val;

                        int.TryParse(GetCommand(value, "minimum"), out min);
                        int.TryParse(GetCommand(value, "maximum"), out max);
                        int.TryParse(GetCommand(value, "value"), out val);

                        ret.Minimum = min;
                        ret.Maximum = max;
                        ret.Value = val;
                        break;
                    default:
                        ret.Text = value;
                        break;
                }

                // Change width or auto size it
                if (width > 0)
                {
                    if (ret.GetType().Name == "Label")
                        ret.AutoSize = false;

                    ret.Width = width;
                }
                else
                {
                    if (ret.GetType().Name == "Label")
                        ret.AutoSize = true;
                    else
                    {
                        try
                        {
                            if ((int)ret.Text.Length > 0)
                            {
                                Graphics g = ret.CreateGraphics();
                                ret.Width = (int)Math.Ceiling(g.MeasureString(ret.Text, ret.Font).Width + 10);
                                g.Dispose();
                            }
                            else
                                ret.Width = 100;
                        }
                        catch
                        {
                            ret.Width = 100;
                        }
                    }
                }

                // Change height or auto size it
                if (height > 0)
                {
                    if (ret.GetType().Name == "Label")
                        ret.AutoSize = false;

                    ret.Height = height;
                }
                else
                {
                    switch ((string)ret.GetType().Name)
                    {
                        case "Label":
                            ret.AutoSize = true;
                            break;
                        case "Button":
                            ret.Height = 22;
                            break;
                        default:
                            ret.Height = 20;
                            break;
                    }
                }

                // Setup position or auto position it
                if (left >= 0)
                    ret.Left = left;
                else
                {
                    if (gui.Controls.Count > 0 && ret.GetType().Name != "Label")
                    {
                        Control buf = gui.Controls[gui.Controls.Count - 1];
                        ret.Left = buf.Left + buf.Width + 6;
                    }
                    else
                        ret.Left = 12;
                }

                if (top >= 0)
                    ret.Top = top;
                else
                {
                    if (gui.Controls.Count > 0)
                    {
                        Control buf = gui.Controls[gui.Controls.Count - 1];

                        if (ret.GetType().Name == "Label")
                            ret.Top = buf.Top + buf.Height + 6;
                        else
                            ret.Top = buf.Top;
                    }
                    else
                        ret.Top = 12;
                }

                // Add the control to the GUI
                gui.Controls.Add(ret);
            }

            // Return the control
            return ret;
        }

        /// <summary>
        /// Creates a control to the default form
        /// </summary>
        /// <typeparam name="T">Type of the Control, like TextBox or Label</typeparam>
        /// <param name="name">Name of the control, leave empty for auto naming, auto name will be like textBox1, textBox2 etc</param>
        /// <param name="value">Values of the Control, for TextBox and Label it's just the Text, but for TrackBar and ProgressBar you can specify the min and the max value like that: Minimum:100 Maximum:1000 Value:100</param>
        /// <param name="width">Width of the Control, auto size will be based on the Text parameter</param>
        /// <param name="height">Height of the Control, auto size will be based on the font's height</param>
        /// <param name="left">Position of the Control, auto placement: Labels will be placed in new line and each other Control will follow each other in that line</param>
        /// <param name="top">Position of the Control, auto placement: Labels will be placed in new line and each other Control will follow each other in that line</param>
        /// <returns>The control created</returns>
        public dynamic Create<T>(string name = "", string value = "", int width = -1, int height = -1, int left = -1, int top = -1) where T : new()
        {
            Form frm = GetWindow(this.GetType().Name);

            if (frm == null)
                return null;

            return Create<T>(frm, name, value, width, height, left, top);
        }

        /// <summary>
        /// Returns the property value from a control, thread-safe
        /// </summary>
        /// <param name="gui">Control will be searched in this Form</param>
        /// <param name="controlName">Name of the Control</param>
        /// <param name="varName">Name of the value to get</param>
        /// <returns>The value got from the Control, on error it will be null</returns>
        public dynamic GetControlValue(Form gui, string controlName, string varName = "Text")
        {
            return gui.GetControlValue(controlName, varName);
        }

        /// <summary>
        /// Returns the property value from a control, thread-safe
        /// </summary>
        /// <param name="controlName">Name of the Control</param>
        /// <param name="varName">Name of the value to get</param>
        /// <returns>The value got from the Control, on error it will be null</returns>
        public dynamic GetControlValue(string controlName, string varName = "Text")
        {
            Form frm = GetWindow(this.GetType().Name);

            if (frm == null)
                return null;

            return frm.GetControlValue(controlName, varName);
        }

        /// <summary>
        /// Invoke the code in the control
        /// </summary>
        /// <param name="control">Control where to invoke</param>
        /// <param name="code">Code to invoke</param>
        public void UIThread(Control control, Action code)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(code);
                return;
            }
            code.Invoke();
        }

        /// <summary>
        /// Invoke the code in the default window of the module
        /// </summary>
        /// <param name="code">Code to invoke</param>
        public void UIThread(Action code)
        {
            Form frm = GetWindow(this.GetType().Name);

            if (frm == null)
                return;

            UIThread(frm, code);
        }
        #endregion

        #region Radar
        /// <summary>
        /// Gets the Radar from the Main Form (Tools->Radar)
        /// </summary>
        /// <returns>The radar control from the Main form (don't forget to use UIThread for thread-safety)</returns>
        public RadarDisplay GetRadar()
        {
            RadarDisplay ret = null;

            WildBotObject.fRadar.UIThread(delegate
            {
                try
                {
                    ret = WildBotObject.fRadar.rdRadar;
                }
                catch { }
            });

            return ret;
        }
        #endregion

        #region Settings
        /// <summary>
        /// Initalize the config file
        /// </summary>
        /// <param name="path">Path of the XML file</param>
        /// <returns>Returns the create config</returns>
        public XmlFile InitializeConfig(string path = "", bool forceReset = false)
        {
            if (path.Length <= 0)
                path = Application.StartupPath + @"\..\Settings\" + this.GetType().Name + ".xml";

            // Init settings
            XmlFile ret = new XmlFile(SmallExtensions.InitDirectory(path), new XmlDocument());

            // Create XML file if it does not exists
            if (!File.Exists(ret.fileName) | forceReset)
            {
                XmlDeclaration xDec = ret.doc.CreateXmlDeclaration("1.0", "utf-8", null);

                XmlElement rootNode = ret.doc.CreateElement("root");
                ret.doc.InsertAfter(xDec, ret.doc.DocumentElement);
                ret.doc.AppendChild(rootNode);
            }
            else
                ret.doc.Load(ret.fileName);

            return ret;
        }

        /// <summary>
        /// Saves the setting into the given XML file
        /// </summary>
        /// <param name="xml">The XML file returned by the InitializeConfig function</param>
        /// <param name="key">The name of the config</param>
        /// <param name="value">The value of the config</param>
        /// <param name="unique">If unique then only 1 setting can be with that name, else you can give multiple settings with the same name</param>
        public void SetConfig(XmlFile xml, string key, string value = "", bool unique = true)
        {
            XMLHandler.SetElement(xml.doc, "root", key, value, unique);

            // Save the XML
            xml.doc.Save(xml.fileName);
        }

        /// <summary>
        /// Gets a settings from the XML file (unique)
        /// </summary>
        /// <param name="xml">The XML file returned by the InitializeConfig function</param>
        /// <param name="key">The name of the config</param>
        /// <returns>Setting is unique, so return a string with the setting, if settings doesn't exists then empty string ("") will be returned</returns>
        public string GetConfig(XmlFile xml, string key)
        {
            XmlNode[] nodes = XMLHandler.GetElement(xml.doc, "root", key);

            if (nodes != null && nodes.Length > 0)
                return nodes.First().InnerText;

            return "";
        }

        /// <summary>
        /// Gets the settings from the XML file (non-unique)
        /// </summary>
        /// <param name="xml">The XML file returned by the InitializeConfig function</param>
        /// <param name="key">The name of the config</param>
        /// <param name="unique">If unique then only 1 setting can be with that name, else you can give multiple settings with the same name</param>
        /// <returns>Setting can be non-unique, so a string array will be returned, if the setting doesn't exists null will be returned</returns>
        public string[] GetConfig(XmlFile xml, string key, bool unique)
        {
            if (!unique)
                return new string[] { GetConfig(xml, key) };
            else
            {
                XmlNode[] nodes = XMLHandler.GetElement(xml.doc, "root", key);

                if (nodes != null)
                {
                    List<string> buf = new List<string>();

                    foreach (XmlNode xn in nodes)
                        buf.Add(xn.InnerText);

                    return buf.ToArray();
                }
            }

            return null;
        }
        #endregion
        #endregion
    }
}
