﻿// Module informations
// Name: VendoringModule
// Comment: Default vendoring module. Made by Alk
// Version: 1.1

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WildBot.Modules;
using WildBot.GameObjects;
using WildBot.AI;
using WildBot.Tools;
using WildBot.GameControllers;

namespace WildBot.Script
{
    public class VendoringModule : Module
    {
        private TravelData _vendoringTravelData = new TravelData();
        private bool _selling = false;

        public VendoringModule()
        {
            log.LogConsole("Vendoring module loaded succesfully", System.Drawing.Color.Green);
        }

        public override void Run()
        {
            if (BotBrain.IsLoggedIn())
            {
                GameBaseObject gameBase = BotBrain.GameBase;
                MainPlayerEntity player = gameBase.Player;
                WaypointList vendorPath = StatusInformerObject.SelectedGrindingSpot.VendorPath;

                // If need to sell items
                if (player.Inventory.RemainingSlots() < 3 && vendorPath.Positions.Count > 0)
                    GoToSellItems();

                // Go back to grinding spot
                else if (_selling)
                {
                    WaypointList grindingPath = StatusInformerObject.SelectedGrindingSpot.GrindingPath;
                    if (vendorPath.Positions.Count > 0 && player.IsCloseFromPath(vendorPath, 10) && !player.IsCloseFromPath(grindingPath, 10) && !player.IsInCombat)
                    {
                        PathFinder.ReachWaypointFromPath(vendorPath, 0, _vendoringTravelData);
                        _vendoringTravelData.SetStatesPriority((float)State.Priorities.SELLING);
                    }

                    // If player is close from grinding path, reset selling boolean
                    else if (player.IsCloseFromPath(grindingPath, 10))
                        _selling = false;
                }
            }
        }

        /// <summary>
        /// Follow paths to reach vendor and press F when vendor is reached. Please note that you will need a Wild Star plugin like junkIt (http://www.curse.com/ws-addons/wildstar/220002-junkit) to make it work. Be careful, for the moment, vendor needs to be accessible from last position of vendor path.
        /// </summary>
        /// <returns>The state generated and added to the state list by the function.</returns>
        protected State GoToSellItems()
        {
            _selling = true;
            MainPlayerEntity player = BotBrain.GameBase.Player;
            WaypointList grindingWPList = StatusInformerObject.SelectedGrindingSpot.GrindingPath;
            WaypointList vendorWPList = StatusInformerObject.SelectedGrindingSpot.VendorPath;
            
            // Find closest path
            int closestGrindingPathIndex = grindingWPList.ComputeClosestWaypointFromEntity(player);
            float distanceFromGrindingPath = player.ComputeDistanceFromPosition(grindingWPList.Positions[closestGrindingPathIndex]);

            // Then search for closest waypoint from player in vendor path
            int closestVendorPathIndex = vendorWPList.ComputeClosestWaypointFromEntity(player);
            float distanceFromVendorPath = player.ComputeDistanceFromPosition(vendorWPList.Positions[closestVendorPathIndex]);

            // Init returned state
            State returnedState = null;

            // Load vendor position
            List<WorldPosition> vendorPath = StatusInformerObject.SelectedGrindingSpot.VendorPath.Positions;
            WorldPosition vendorPosition = vendorPath[vendorPath.Count - 1];
            // Compute distance from player to vendor
            float distanceFromVendor = player.ComputeDistanceFromPosition(vendorPosition);

            // Follow waypoints to find vendor (if vendor is too far away)
            if (distanceFromVendor > 10)
            {
                // If we are too far from vendor path, follow grinding path until first position
                if (distanceFromVendorPath > 10)
                {
                    PathFinder.ReachWaypointFromPath(grindingWPList, 0, _vendoringTravelData);
                    _vendoringTravelData.SetStatesPriority((float)State.Priorities.SELLING);
                    return returnedState;
                }

                // If we are close enough from vendor path, follow it toward vendor (last position)  
                else
                {
                    PathFinder.ReachWaypointFromPath(vendorWPList, vendorPath.Count - 1, _vendoringTravelData);
                    _vendoringTravelData.SetStatesPriority((float)State.Priorities.SELLING);
                    return returnedState;
                }
            }

            // If we already are in front of vendor, press F to talk to him
            else
            {
                // Reset travel data
                _vendoringTravelData = new TravelData();

                // Create state that will press f
                returnedState = new State();
                returnedState.AddCustomAction(delegate
                {
                    KeySender.Send("f", 0);
                });

                returnedState.Priority = (float)State.Priorities.SELLING;
                BotBrain.AddState(returnedState);
            }

            return returnedState;
        }
    }
}
