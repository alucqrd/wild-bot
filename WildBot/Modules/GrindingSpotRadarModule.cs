﻿// Module informations
// Name: GrindingSpotRadarModule
// Comment: Default GrindingSpot Radar module. Made by BlackSun
// Version: 1.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using WildBot.AI;
using WildBot.GameObjects;
using WildBot.GUI;
using WildBot.Tools;
using WildBot.Modules;

namespace WildBot.Script
{
    class GrindingSpotRadarModule : Module
    {
        // Create the Layer holders
        List<RadarObject> GrindingObjects = new List<RadarObject>();
        List<RadarObject> HolocryptObjects = new List<RadarObject>();
        List<RadarObject> VendorObjects = new List<RadarObject>();

        public GrindingSpotRadarModule()
        {
            // Run even if bot is paused
            RunIfBotPaused = true;

            // Display message that the module is loaded
            log.LogConsole("GrindingSpot Radar module loaded succesfully", Color.Green);
        }

        // Override the OnDisable to clear the entities from the Radar when user disabled the module
        public override void OnDisable()
        {
            // Get the radar display (don't forget it's from an other thread, use UIThread for safe calls)
            RadarDisplay radar = GetRadar();

            if (radar != null)
            {
                // UIThread required for thread-safe call (invoke)
                UIThread(radar, delegate
                {
                    // Clear the layers from the RadarDisplay
                    if (radar.Layers.ContainsKey("Grinding"))
                        radar.Layers.Remove("Grinding");

                    if (radar.Layers.ContainsKey("Holocrypt"))
                        radar.Layers.Remove("Holocrypt");

                    if (radar.Layers.ContainsKey("Vendor"))
                        radar.Layers.Remove("Vendor");

                    // Remove the layer objects
                    GrindingObjects.Clear();
                    HolocryptObjects.Clear();
                    VendorObjects.Clear();

                    // Update the display, not neccessery, but it is possible to do it any time to force an update
                    radar.Draw();
                });
            }
        }

        /// <summary>
        /// Fill the "dispList" with WorldPositions from "wpList" (With the Color of "color")
        /// </summary>
        /// <param name="dispList">List to fill up</param>
        /// <param name="wpList">WaypointList to get the positions from</param>
        /// <param name="color">Display color</param>
        public void DisplayWaypointList(List<RadarObject> dispList, WaypointList wpList, Color color)
        {
            RadarDisplay radar = GetRadar();

            if (radar != null && BotBrain.IsLoggedIn())
            {
                // Check if need update or not
                bool found = false;
                foreach (RadarObject ro in dispList)
                {
                    foreach (WorldPosition pos in wpList.Positions)
                    {
                        if (ro.Position == pos)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        // If update required then clear the list
                        dispList.Clear();
                        break;
                    }
                }

                // If update required and there are positions in the "wlList" then fill the "dispList"
                if (!found && wpList.Positions.Count > 0)
                {
                    WorldPosition lastPos = null;
                    foreach (WorldPosition pos in wpList.Positions)
                    {
                        // Create the RadarObject
                        RadarObject ro = new RadarObject();

                        // Setup the displays
                        ro.DisplayShape = RadarObject.Shapes.Rectangle;
                        ro.DisplaySize = 2;
                        ro.DisplayColor = color;

                        // Setup the position and link position (the line between the two position)
                        ro.Position = pos;

                        if (lastPos != null)
                            ro.LinkPosition = lastPos;
                        else if (wpList.IsCircle && pos == wpList.Positions.First())
                            ro.LinkPosition = wpList.Positions.Last();

                        // Save the current position
                        lastPos = pos;

                        // Add the object to the list
                        dispList.Add(ro);
                    }
                }
            }
        }

        public override void Run()
        {
            GrindingSpot gs = StatusInformerObject.SelectedGrindingSpot;
            RadarDisplay radar = GetRadar();

            if (radar != null && BotBrain.IsLoggedIn())
            {
                // UIThread required for thread-safe call (invoke)
                UIThread(radar, delegate
                {
                    // Add the layers to the RadarDisplay
                    if (!radar.Layers.ContainsKey("Grinding"))
                        radar.Layers.Add("Grinding", GrindingObjects);

                    if (!radar.Layers.ContainsKey("Holocrypt"))
                        radar.Layers.Add("Holocrypt", HolocryptObjects);

                    if (!radar.Layers.ContainsKey("Vendor"))
                        radar.Layers.Add("Vendor", VendorObjects);
                });

                // Fill up the lists
                DisplayWaypointList(GrindingObjects, gs.GrindingPath, Color.Orange);
                DisplayWaypointList(HolocryptObjects, gs.HolocryptPath, Color.LightGray);
                DisplayWaypointList(VendorObjects, gs.VendorPath, Color.Lime);
            }
        }
    }
}
