﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WildBot.Modules
{
    /// <summary>
    /// Wrapper for module, used in UI to store informations.
    /// </summary>
    public class ModuleContainer
    {
        public string Name;
        public bool Enabled;
        public Module Value;

        public ModuleContainer()
        {
        }

        public ModuleContainer(string name, bool enabled, Module value)
        {
            Name = name;
            Enabled = enabled;
            Value = value;
        }
    }
}
