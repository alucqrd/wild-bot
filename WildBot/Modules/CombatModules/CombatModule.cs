﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WildBot.GUI;
using WildBot.AI;
using WildBot.GameObjects;
using WildBot.Tools;
using WildBot.GameControllers;

namespace WildBot.Modules.CombatModules
{
    /// <summary>
    /// Class that needs to be inherited when developping a script for bot custom fighting behaviour. 
    /// Please keep in mind that only one CombatModule can be loaded at a time by the BotBrain.
    /// Combat sequence scripting is writtent in FightLogic overriden method.
    /// You can override CustomRun method if you want to some code to be executed every ai loop, and not only when FightSequence is called (ie. Player has selected a target and is close enough from it).
    /// A good example is available on how to use this class while scripting, check DefaultModule.cs file from Modules\CombatModules folder.
    /// </summary>
    public class CombatModule : Module
    {
        /// <summary>
        /// Override this method if you want to have some code being executed on each AI loop on the beginning of Run method.
        /// </summary>
        protected virtual void CustomRun()
        {
            // Do nothing
        }

        /// <summary>
        /// Generic CombatModule Run method. Run CustomRun, go toward target, face it and run Fightsequence if needed.
        /// </summary>
        public override void Run()
        {
            GameBaseObject gameBase = BotBrain.GameBase;
            MainPlayerEntity player = BotBrain.GameBase.Player;

            // Run CustomRun
            CustomRun();

            // Reinit GameController's fighting flag if needed
            GameController.FightingFlag = false;

            float attackRange = -1;
            float angleTolerance = -1;
            bool autoFaceActivated = false;
            bool whitelistMode = false;
            string[] nameList = null;

            // Load settings
            WildBotObject.fMain.UIThread(delegate
            {
                angleTolerance = float.Parse(WildBotObject.fMain.txtFacingAngleTolerance.Text, System.Globalization.CultureInfo.InvariantCulture);
                attackRange = float.Parse(WildBotObject.fMain.txtAttackRange.Text, System.Globalization.CultureInfo.InvariantCulture);
                if (WildBotObject.fMain.chkAutoFace.Checked)
                    autoFaceActivated = true;
                if (WildBotObject.fMain.chkWhitelistingMode.Checked)
                    whitelistMode = true;
                nameList = WildBotObject.fMain.txtNameFilters.Lines;
            });

            // Check if any target selected
            if (StatusInformerObject.SelectedEntity != null)
            {
                Entity selectedEntity = StatusInformerObject.SelectedEntity;

                if (selectedEntity != null && player.GetDispostionTowardEntity(selectedEntity) != 2)
                {
                    bool attackTarget;
                    if (whitelistMode)
                        attackTarget = false;
                    else
                        attackTarget = true;

                    foreach(string name in nameList)
                    {
                        if (whitelistMode && name == selectedEntity.Name)
                            attackTarget = true;

                        else if (!whitelistMode && name == selectedEntity.Name)
                            attackTarget = false;
                    }

                    // Check distance to target
                    float distance = player.ComputeDistanceFromEntity(selectedEntity);
                    if (distance < attackRange && !selectedEntity.IsDead && attackTarget)
                    {
                        float entityDirection = player.ComputeDirection(selectedEntity.Position);
                        if ((!autoFaceActivated && player.IsFacingPosition(selectedEntity.Position, angleTolerance) != 0)
                            || (autoFaceActivated && player.IsFacingPosition(selectedEntity.Position, (float)(Math.PI/4)) != 0))
                        {
                            State faceState = GameController.MoveTowardPosition(selectedEntity.Position, true);
                            faceState.Priority = (float)State.Priorities.ATTACKING;
                            GameController.FightingFlag = false;
                        }

                        else
                        {
                            FightLogic();
                            GameController.FightingFlag = true;
                        }
                    }

                    else
                    {
                        GameController.FightingFlag = false;
                    }
                }

                else if(!player.IsInCombat)
                    GameController.FightingFlag = false;
            }
            
            // Rest if needed
            if (!player.IsInCombat && (player.Life / player.LifeMax) < 0.50)
            {
                // Rest
                State restState = new State();
                restState.Priority = (float)State.Priorities.RESTING;
                BotBrain.AddState(restState);
            }
        }

        /// <summary>
        /// Fight Logic, need to be overriden in child classes.
        /// This method is called if player is close enough to selected entity, and facing it if bot is configured to not use game automatic facing.
        /// </summary>
        protected virtual void FightLogic()
        {
            // By default, do nothing
        }
    }
}
