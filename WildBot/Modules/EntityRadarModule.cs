﻿// Module informations
// Name: EntityRadarModule
// Comment: Default Entity Radar Display module. Made by BlackSun
// Version: 1.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using WildBot.AI;
using WildBot.GameObjects;
using WildBot.GUI;
using WildBot.Tools;
using WildBot.Modules;

namespace WildBot.Script
{
    public class EntityRadarModule : Module
    {
        // Create the Layer holder
        List<RadarObject> EntityObjects = new List<RadarObject>();

        public EntityRadarModule()
        {
            // Run even if bot is paused
            RunIfBotPaused = true;

            // Display message that the module is loaded
            log.LogConsole("Entity Radar module loaded succesfully", Color.Green);
        }

        // Override the OnDisable to clear the entities from the Radar when user disabled the module
        public override void OnDisable()
        {
            // Get the radar display (don't forget it's from an other thread, use UIThread for safe calls)
            RadarDisplay radar = GetRadar();

            if (radar != null)
            {
                // UIThread required for thread-safe call (invoke)
                UIThread(radar, delegate
                {
                    // Clear the layers from the RadarDisplay
                    if (radar.Layers.ContainsKey("Entities"))
                        radar.Layers.Remove("Entities");

                    // Remove the layer objects
                    EntityObjects.Clear();

                    // Update the display, not neccessery, but it is possible to do it any time to force an update
                    radar.Draw();
                });
            }
        }

        public override void Run()
        {
            GameBaseObject gameBase = BotBrain.GameBase;
            MainPlayerEntity player = gameBase.Player;
            RadarDisplay radar = GetRadar();

            if (radar != null && BotBrain.IsLoggedIn())
            {
                // UIThread required for thread-safe call (invoke)
                UIThread(radar, delegate
                {
                    // Add the layers to the RadarDisplay
                    if (!radar.Layers.ContainsKey("Entities"))
                        radar.Layers.Add("Entities", EntityObjects);
                });

                // Loop through each entity
                foreach (KeyValuePair<uint, Entity> kv in gameBase.EntityList)
                {
                    // Skip if the entity is the player
                    if (kv.Key == player.GUID)
                        continue;

                    // To prevent flickering only add thoose entities that are not on the list
                    // else just update (with List.Clear() it would be faster, but it could cause flicker
                    bool needToAdd = true;

                    // Check for each object in the radar
                    foreach (RadarObject ro in EntityObjects)
                    {
                        // Check if it maches with the Entity
                        if (kv.Key == (uint)ro.ID)
                        {
                            // Object found, update the position
                            ro.Position = kv.Value.Position;

                            needToAdd = false;
                            break;
                        }
                    }

                    // Add entity if required
                    if (needToAdd)
                    {
                        // Create the Radarobject
                        RadarObject ro = new RadarObject();

                        // Setup the display
                        ro.DisplayShape = RadarObject.Shapes.Circle;
                        ro.DisplaySize = 2;

                        switch (player.GetDispostionTowardEntity(kv.Value))
                        {
                            case 0: // Enemy
                                ro.DisplayColor = Color.Red;
                                break;
                            case 1: // Natural
                                ro.DisplayColor = Color.Yellow;
                                break;
                            case 2: // Friendly
                                ro.DisplayColor = Color.Green;
                                break;
                            default: // Error?
                                ro.DisplayColor = Color.Transparent;
                                break;
                        }

                        // Setup the position
                        ro.Position = kv.Value.Position;

                        // Setup the Name / ID so the module can recognize the objects
                        ro.ID = kv.Key;

                        // Add the object to the list
                        EntityObjects.Add(ro);
                    }
                }

                // Loop through each RadarObject to remove non-required ones (more CPU usage, but less flicker)
                for (int i = 0; i < EntityObjects.Count; ) // No increment required here!
                {
                    bool deleteRequired = true;

                    // Search for the Entity
                    foreach (KeyValuePair<uint, Entity> kv in gameBase.EntityList)
                    {
                        if (kv.Key == (uint)EntityObjects[i].ID)
                        {
                            // Found the entity, increment the counter and step out from the loop
                            i++;
                            deleteRequired = false;
                            break;
                        }
                    }

                    // Remove the RadarObject if required
                    if (deleteRequired)
                        EntityObjects.RemoveAt(i);
                }
            }
        }
    }
}
