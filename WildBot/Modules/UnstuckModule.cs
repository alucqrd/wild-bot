﻿// Module informations
// Name: UnstuckModule
// Comment: Simple unstuck module. Made by Alk
// Version: 1.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using WildBot.Modules;
using WildBot.GameObjects;
using WildBot.GUI;
using WildBot.Tools;
using WildBot.GameControllers;
using WildBot.AI;

namespace WildBot.Script
{
    // This is a very simple unstucker. What it does is adding a custom action to the movement thread in GameController to make the player jump randomly every 8-13 sec).
    public class UnstuckModule : Module
    {
        private RandomizedTimer _distanceCheckTimer = new RandomizedTimer(3000);
        private WorldPosition _lastPosition = null;
        private bool _firstTime = true;

        public UnstuckModule()
        {
            log.LogConsole("Unstuck module loaded succesfully", System.Drawing.Color.Green);
        }

        public override void Run()
        {
            if(GameController.MovingFlag)
            {
                if (!_distanceCheckTimer.IsRunning() || _lastPosition == null)
                {
                    _distanceCheckTimer.Reset();
                    _lastPosition = BotBrain.GameBase.Player.Position;
                }

                if(_distanceCheckTimer.Elapsed())
                {
                    WorldPosition currentPosition = BotBrain.GameBase.Player.Position;
                    // Compute distance between last time and now
                    float distance = currentPosition.Distance(_lastPosition);

                    if(distance < 6.5)
                    {
                        string strafeLeftKey = "q";
                        string strafeRightKey = "e";
                        string jumpKey = "SPACE";

                        UIThread(WildBotObject.fMain, delegate
                        {
                            strafeLeftKey = WildBotObject.fMain.GetKey("Strafe Left");
                            strafeRightKey = WildBotObject.fMain.GetKey("Strafe Right");
                            jumpKey = WildBotObject.fMain.GetKey("Jump");
                        });

                        // Generate unstucking state/action
                        State unstuckState = new State();
                        unstuckState.AddCustomAction(delegate
                        {
                            // First time jump twice
                            if (_firstTime)
                            {
                                _firstTime = false;
                                KeySender.Send(jumpKey, 1);
                                Thread.Sleep(300);
                                KeySender.Send(jumpKey, 2);
                                Thread.Sleep(100);
                                KeySender.Send(jumpKey, 1);
                                Thread.Sleep(500);
                                KeySender.Send(jumpKey, 2);
                            }

                            // Second time Strafe Left for 1 sec then Strafe Right for 1 sec
                            else
                            {
                                KeySender.Send(strafeLeftKey, 1);
                                Thread.Sleep(1500);
                                KeySender.Send(strafeLeftKey, 2);
                                KeySender.Send(strafeRightKey, 1);
                                Thread.Sleep(1500);
                                KeySender.Send(strafeRightKey, 2);
                                _firstTime = true;
                            }

                        });
                        unstuckState.Priority = (float)State.Priorities.UNSTUCKING;
                        BotBrain.AddState(unstuckState);
                    }

                    _lastPosition = currentPosition;
                    _distanceCheckTimer.Reset();
                }
            }

            else
            {
                if (_distanceCheckTimer.IsRunning())
                    _distanceCheckTimer.Stop();
            }
        }
    }
}
