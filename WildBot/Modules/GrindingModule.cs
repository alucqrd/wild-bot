﻿// Module informations
// Name: GrindingModule
// Comment: Default grinding module. Made by Alk
// Version: 1.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

using WildBot.Modules;
using WildBot.AI;
using WildBot.GameObjects;
using WildBot.Tools;
using WildBot.GUI;
using WildBot.GameControllers;

namespace WildBot.Script
{
    // If you want to modify this script, you should first load it in the Module Creator. Then you will have all the API with auto-completion in Visual Studio.

    public class GrindingModule : Module
    {
        private TravelData _grindTravelData = new TravelData();
        private TravelData _holocryptTravelData = new TravelData();

        public GrindingModule()
        {
            log.LogConsole("Grinding module loaded succesfully", System.Drawing.Color.Green);
        }

        /// <summary>
        /// This module selects targets, moves toward them, and vacuum loot.
        /// </summary>
        public override void Run()
        {
            if (BotBrain.IsLoggedIn())
            {
                GameBaseObject gameBase = BotBrain.GameBase;
                MainPlayerEntity player = BotBrain.GameBase.Player;

                Entity selectedEntity = null;

                // Var reading from global gui
                // Get max attack distance
                float maxAttackDistance = -1;
                float moveToTargetDistance = -1;
                
                UIThread(WildBotObject.fMain, delegate
                {
                    maxAttackDistance = float.Parse(WildBotObject.fMain.txtAttackRange.Text, System.Globalization.CultureInfo.InvariantCulture);
                    moveToTargetDistance = float.Parse(WildBotObject.fMain.txtMoveToTargetDistance.Text, System.Globalization.CultureInfo.InvariantCulture);
                });

                // Use Tab to find target
                if (player.TargetGUID == 0 || (StatusInformerObject.SelectedEntity != null && StatusInformerObject.SelectedEntity.IsDead))
                {
                    if (StatusInformerObject.SelectTargetTimer.Elapsed() || !StatusInformerObject.SelectTargetTimer.IsRunning())
                    {
                        // Create state that presses tab to select target
                        State selectTargetState = new State();
                        selectTargetState.AddCustomAction(delegate
                        {
                            KeySender.Send("TAB", 0);
                        });
                        selectTargetState.TimerList.Add(StatusInformerObject.SelectTargetTimer);
                        selectTargetState.Priority = (float)State.Priorities.SELECTING_TARGET;
                        BotBrain.AddState(selectTargetState);
                    }
                }

                // Find targeted entity
                else
                {
                    if (gameBase.EntityList.ContainsKey(player.TargetGUID))
                        selectedEntity = gameBase.EntityList[player.TargetGUID];
                }

                // Set entity selected in statusinformer
                StatusInformerObject.SelectedEntity = selectedEntity;

                // Compute distance to entity
                float distance = player.ComputeDistanceFromEntity(selectedEntity);

                // Move toward entity if needed
                if (selectedEntity != null && distance < moveToTargetDistance)
                {
                    if (!player.IsInCombat && distance >= maxAttackDistance)
                    {
                        GameController.MoveTowardPosition(selectedEntity.Position);
                        _grindTravelData.ResetIndex();
                        _holocryptTravelData.ResetIndex();
                    }
                }

                // Navigate through grinding spot waypoints
                else
                    PathFinder.FollowPath(StatusInformerObject.SelectedGrindingSpot.GrindingPath, _grindTravelData);

                // If need to vacuum loot
                if (gameBase.CanVacuum())
                    GameController.VacuumLoot();

                // If player is dead, wait for automatic resurrect
                if (player.IsDead)
                {
                    State resurrectState = new State();
                    resurrectState.AddCustomAction(delegate
                    {
                        // Do nothing
                    });
                    resurrectState.Priority = (float)State.Priorities.RESURRECT;
                    BotBrain.AddState(resurrectState);
                }

                // Check if we are on holocrypt go back to start of grinding spot
                WaypointList holocryptPath = StatusInformerObject.SelectedGrindingSpot.HolocryptPath;
                WaypointList grindingPath = StatusInformerObject.SelectedGrindingSpot.GrindingPath;
                if (holocryptPath.Positions.Count > 0 && player.IsCloseFromPath(holocryptPath, 10) && !player.IsCloseFromPath(grindingPath, 10) && !player.IsInCombat)
                {
                    PathFinder.ReachWaypointFromPath(holocryptPath, holocryptPath.Positions.Count - 1, _holocryptTravelData);
                    _holocryptTravelData.SetStatesPriority((float)State.Priorities.TRAVELING);
                }
            }
        }
    }
}