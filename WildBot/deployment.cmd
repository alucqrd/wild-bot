RD /S /Q "C:\Download temp\Wild Bot Last"

C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe "C:\Developpement\Wild Bot\WildBot\WB.shfbproj"

XCOPY "C:\Developpement\Wild Bot\WildBot\bin\Release" "C:\Download temp\Wild Bot Last" /D /E /C /R /I /K /Y 
mkdir "C:\Download temp\Wild Bot Last\Logs"
mkdir "C:\Download temp\Wild Bot Last\Grinding Spots"
mkdir "C:\Download temp\Wild Bot Last\Settings"

del "C:\Download temp\Wild Bot Last\Launcher.exe.config"
del "C:\Download temp\Wild Bot Last\Launcher.pdb"
del "C:\Download temp\Wild Bot Last\Launcher.vshost.exe"
del "C:\Download temp\Wild Bot Last\Launcher.vshost.exe.config"
del "C:\Download temp\Wild Bot Last\Launcher.vshost.exe.manifest"
del "C:\Download temp\Wild Bot Last\LastBuild.log"

del "C:\Download temp\Wild Bot Last\x86\CrashReporter.NET.pdb"
del "C:\Download temp\Wild Bot Last\x86\ObjectListView.pdb"
del "C:\Download temp\Wild Bot Last\x86\ObjectListView.xml"
del "C:\Download temp\Wild Bot Last\x86\WildBot.exe.config"
del "C:\Download temp\Wild Bot Last\x86\WildBot.pdb"
del "C:\Download temp\Wild Bot Last\x86\WildBot.vshost.exe"
del "C:\Download temp\Wild Bot Last\x86\WildBot.vshost.exe.config"
del "C:\Download temp\Wild Bot Last\x86\WildBot.vshost.exe.manifest"
del "C:\Download temp\Wild Bot Last\x86\WildBot.exe"

del "C:\Download temp\Wild Bot Last\x64\CrashReporter.NET.pdb"
del "C:\Download temp\Wild Bot Last\x64\ObjectListView.pdb"
del "C:\Download temp\Wild Bot Last\x64\ObjectListView.xml"
del "C:\Download temp\Wild Bot Last\x64\WildBot.exe.config"
del "C:\Download temp\Wild Bot Last\x64\WildBot.pdb"
del "C:\Download temp\Wild Bot Last\x64\WildBot.vshost.exe"
del "C:\Download temp\Wild Bot Last\x64\WildBot.vshost.exe.config"
del "C:\Download temp\Wild Bot Last\x64\WildBot.vshost.exe.manifest"
del "C:\Download temp\Wild Bot Last\x64\WildBot.exe"

del "C:\Download temp\Wild Bot Last\Grinding Spots\*.*"
del "C:\Download temp\Wild Bot Last\Settings\*.*"
del "C:\Download temp\Wild Bot Last\Logs\*.*"

XCOPY "C:\Developpement\Wild Bot\WildBot\API changes.txt" "C:\Download temp\Wild Bot Last\" /Y
XCOPY "C:\Developpement\Wild Bot\WildBot\Readme.txt" "C:\Download temp\Wild Bot Last\" /Y
XCOPY "C:\Developpement\Wild Bot\WildBot\Patchnotes.txt" "C:\Download temp\Wild Bot Last\" /Y

XCOPY "C:\Developpement\Wild Bot\WildBot\bin\Release\Launcher.exe" "C:\Download temp\Wild Bot Last\" /Y

C:\Developpement\ConfuserEx-master\Debug\bin\Confuser.CLI.exe "c:\Developpement\Wild Bot\WildBot\WildBotx86.crproj"
C:\Developpement\ConfuserEx-master\Debug\bin\Confuser.CLI.exe "c:\Developpement\Wild Bot\WildBot\WildBotx64.crproj"

XCOPY "C:\Developpement\Wild Bot\WildBot\Confused\bin\Release\x86\WildBot.exe" "C:\Download temp\Wild Bot Last\x86\" /Y
XCOPY "C:\Developpement\Wild Bot\WildBot\Confused\bin\Release\x64\WildBot.exe" "C:\Download temp\Wild Bot Last\x64\" /Y

RD /S /Q "C:\Developpement\Wild Bot\WildBot\Confused"