﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using WildBot.GameObjects;
using WildBot.Tools;

namespace WildBot.AI
{
    /// <summary>
    /// Static class used to share informations between all the modules.
    /// </summary>
    public static class StatusInformerObject
    {
        /// <summary>
        /// Currently selected entity
        /// </summary>
        public static Entity SelectedEntity = null;
        /// <summary>
        /// Select target timer
        /// </summary>
        public static RandomizedTimer SelectTargetTimer = new RandomizedTimer(500, 300);
        /// <summary>
        /// Currently selected grinding spot
        /// </summary>
        public static GrindingSpot SelectedGrindingSpot;
    }
}
