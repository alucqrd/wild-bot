﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

using WildBot.GameObjects;
using WildBot.Tools;
using WildBot.GameControllers;

namespace WildBot.AI
{
    /// <summary>
    /// This class is used to execute code only if the priority is the highest from the list. This way, bot will know what to do. 
    /// For now priorities are enumerated in this class. (Waiting for something better).
    /// Common use is to create a State, add some code in an action and add it to the custom action list, finally set state priority.
    /// Remember to Add the state in BotBrain State list or it won't be processed.
    /// </summary>
    public class State
    {
        /// <summary>
        /// Default priority.
        /// </summary>
        public enum Priorities { IDLING, SELECTING_TARGET, MOVING, TRAVELING, SELLING, UNSTUCKING, LOOTING, ATTACKING, HEALING, RESTING, RESURRECT }

        private List<Action> _customActionList = new List<Action>();
        private List<RandomizedTimer> _timerList = new List<RandomizedTimer>();

        /// <summary>
        /// State priority. Type used is float so the module developper can "insert" custom state priority. 
        /// (Definitively not the best solution, we are open for anything better ;)
        /// </summary>
        public float Priority  = 0;

        /// <summary>
        /// Sleep time (in ms) to wait after the state has been runned.
        /// </summary>
        public int _sleep = 0;
        /// <summary>
        /// Value that will be set to MovingFlag from AI.StatusInformerObject class. By default it should be false and you should only set it to true if your state involves character moving.
        /// </summary>
        public bool MovingFlag = false;

        /// <summary>
        /// If you want to set a timer to be reset when State is runned.
        /// </summary>
        public List<RandomizedTimer> TimerList { get { return _timerList; } }

        /// <summary>
        /// If you want the bot to sleep after running State (in ms).
        /// </summary>
        public int Sleep { set { this._sleep = value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public State()
        {
        }

        /// <summary>
        /// Add an Action to the list. It will be runned when Run() method is called.
        /// </summary>
        /// <param name="customAction">Custom Acton to be runned.</param>
        public void AddCustomAction(Action customAction)
        {
            _customActionList.Add(customAction);
        }

        /// <summary>
        /// Run all the Custom Run Action previously added.
        /// </summary>
        public virtual void Run()
        {
            foreach (Action customAction in _customActionList)
            {
                customAction();
            }

            // Sleep and reset timers if needed
            if(_sleep != 0)
                Thread.Sleep(_sleep);

            foreach (RandomizedTimer timer in _timerList)
                timer.Reset();

            GameController.MovingFlag = MovingFlag;
        }
    }
}
