﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using WildBot.GameObjects;
using WildBot.Modules;
using WildBot.Modules.CombatModules;
using WildBot.Tools;
using WildBot.GameControllers;
using WildBot.GUI;

//using WildBot.Script;

namespace WildBot.AI
{
    /// <summary>
    /// Main AI class used for the bot. It's an implementation of a Finite State Machine. To summarize it's a loop running all the modules loaded. 
    /// Each module takes some decisions and may generate some states that contain code to be executed. 
    /// However, only the state with highest priority will be executed in the end of each AI loop (See State class documentation for more infos).
    /// This class stores all the game information in a static variable _gameBase (GameBaseObject class).
    /// </summary>
    public static class BotBrain
    {
        #region Offsets
#if x86
        private static int IS_LOGGED_IN_OFFSET = 0x6614;
#else
        private static int IS_LOGGED_IN_OFFSET = 0x81b8;
        #endif
#endregion
        #region Variables
        private static Logger log = new Logger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static GameBaseObject _gameBase;
        public static GameBaseObject GameBase { get { return _gameBase; } }

        /// <summary>
        /// Thread of the AI
        /// </summary>
        public static Thread _AILoopThread;
        /// <summary>
        /// -1: Not initialized, 0: Paused, 1: Running
        /// </summary>
        public static int status = -1;
        /// <summary>
        /// If status == 0 True else False;
        /// </summary>
        public static bool paused
        {
            get
            {
                return status == 0;
            }

            set
            {
                if (status >= 0)
                    status = value ? 0 : 1;
            }
        }

        private static List<ModuleContainer> _moduleList = new List<ModuleContainer>();
        /// <summary>
        /// List containing all loaded modules.
        /// </summary>
        public static List<ModuleContainer> ModuleList { get { return _moduleList; } }

        /// <summary>
        /// List containing all loaded states.
        /// </summary>
        public static List<State> StateList = new List<State>();
        #endregion

        /// <summary>
        /// Initialize the brain
        /// </summary>
        public static void Initialize()
        {
            // Add keyboard event
            KeyboardHook.OnKeyCapture += keyboardHook_OnKeyCapture;

            // Init thread
            _AILoopThread = new Thread(AILoop);
            _AILoopThread.IsBackground = true;
            _AILoopThread.Start();

            // Indicate that the bot is already initialized
            status = -1;

            // ModuleList.Add(new ModuleContainer("GrindingModule.cs", true, new GrindingModule()));

            GameController.Initialize();
        }

        /// <summary>
        /// Start the bot.
        /// </summary>
        public static void Start()
        {
            // Create gamebase object
            _gameBase = new GameBaseObject();

            WildBotObject.fMain.UIThread(delegate
            {
                if (WildBotObject.fMain.chkAutoStart.Checked)
                    status = 1;
                else
                    status = 0;

                if (paused)
                    WildBotObject.fMain.AddLog("Bot paused");
                else
                    WildBotObject.fMain.AddLog("Bot started");

                WildBotObject.fMain.tUpdate.Enabled = true;
                WildBotObject.fMain.UpdatePlayerData();
            });
        }

        /// <summary>
        /// Stop the bot.
        /// </summary>
        public static void Stop()
        {
            // Clear game objects
            _gameBase = null;

            // Remove keyboard events
            KeyboardHook.OnKeyCapture -= keyboardHook_OnKeyCapture;

            // Indicate that the bot is no longer initialized
            status = -1;

            WildBotObject.fMain.AddLog("Bot stopped");
            WildBotObject.fMain.UIThread(delegate
            {
                WildBotObject.fMain.tUpdate.Enabled = false;
            });
        }

        // On keyboard Up/Down
        static void keyboardHook_OnKeyCapture(object sender, Tools.KeyCaptureEventArgs e)
        {
            // Only read the hotkeys if bot is initialized
            if (status >= 0)
            {
                // Flag 0   -> KeyDown
                // Flag 128 -> KeyUp
                if (e.key == Keys.F12 && e.flags == 0)
                {
                    paused = !paused;

                    // Release all key
                    KeySender.ReleaseKeys();

                    // Update the GUI instantly
                    WildBotObject.fMain.UpdatePlayerData();
                }
            }
        }

        /// <summary>
        /// Returns if the character is logged in the world or not.
        /// </summary>
        /// <returns> true if character is logged in game.</returns>
        public static bool IsLoggedIn()
        {
            #if x86
            string exeName = "WildStar32.exe";
            #else
            string exeName = "WildStar64.exe";
            #endif
            long loggedinAddr = WildBotObject.MemoryReader.ReadPointer(WildBotObject.MemoryReader.GetBaseAddress(exeName) + GameObjects.GameBaseObject.BASE_OFFSET);
            int loggedin = WildBotObject.MemoryReader.Read<int>(loggedinAddr + IS_LOGGED_IN_OFFSET);

            if (loggedin == 127)
                return true;

            return false;
        }

        private static void AutoAttach()
        {
            // Select the client
            #if x86
            Process[] clients = Process.GetProcessesByName("WildStar32");
            #else
            Process[] clients = Process.GetProcessesByName("WildStar64");
            #endif

            Process client = null;
            if (clients != null && clients.Length > 0)
                client = clients.First();

            // If there is a client attach to it
            if (client != null)
            {
                // If memory reader is not opened or opened to a different PID then open it
                if (!WildBotObject.MemoryReader.IsOpen() | WildBotObject.MemoryReader.PID != client.Id)
                {
                    // Open the memory
                    if (WildBotObject.MemoryReader.Open(client.Id))
                    {
                        // Start the bot if logged in
                        if (IsLoggedIn())
                            Start();
                        else
                        {
                            WildBotObject.fMain.UIThread(delegate
                            {
                                WildBotObject.fMain.tsStatus.Text = "Game detected, waiting for login";
                            });
                        }
                    }
                }
                else if (WildBotObject.MemoryReader.IsOpen())
                {
                    // Start the bot only if required
                    if (status < 0)
                    {
                        // Start the bot if logged in
                        if (IsLoggedIn())
                            Start();
                        else
                        {
                            WildBotObject.fMain.UIThread(delegate
                            {
                                WildBotObject.fMain.tsStatus.Text = "Game detected, waiting for login";
                            });
                        }
                    }
                }
            }
            // Nothing exists
            else
            {
                if (status >= 0)
                    Stop();

                WildBotObject.fMain.UIThread(delegate
                {
                    WildBotObject.fMain.tsStatus.Text = "Waiting for the game";
                });
            }
        }

        private static void AILoop()
        {
            while (true)
            {
                // Automatically attach the memory reader to the WildStar (and setup if it's 32 or 64 bit)
                AutoAttach();

                // Wait more if bot is not initialized
                if (status < 0)
                    Thread.Sleep(1000); // Bigger sleep is enough for launcher patching and other things
                else
                {
                    // Update game object
                    _gameBase.Update();

                    // Run all subbrains if needed
                    for (int i = 0; i < ModuleList.Count; i++)
                    {
                        if (ModuleList[i].Enabled && (!paused || ModuleList[i].Value.RunIfBotPaused))
                            ModuleList[i].Value.Run();
                    }

                    // While bot is not paused
                    if (!paused)
                    {
                        // Add idling state for the case if no state has been created previously
                        State idlingState = new State();
                        StateList.Add(idlingState);

                        // Process states
                        State selectedState = null;
                        float highestPriority = 0;
                        foreach (State state in StateList)
                        {
                            if (selectedState != null)
                            {
                                if (state.Priority > highestPriority)
                                {
                                    selectedState = state;
                                    highestPriority = state.Priority;
                                }
                            }

                            else
                            {
                                selectedState = state;
                                highestPriority = state.Priority;
                            }
                        }

                        if (selectedState != null)
                            selectedState.Run();

                        // Clean state list
                        StateList.Clear();
                    }

                    // Sleep to prevent high CPU usage and keystrokes spamming
                    Thread.Sleep(10);
                }
            }
        }

        /// <summary>
        /// Add State to Finite State Machine.
        /// </summary>
        /// <param name="state">State that you want to add.</param>
        public static void AddState(State state)
        {
            StateList.Add(state);
        }

        /// <summary>
        /// Add all states from list to FSM.
        /// </summary>
        /// <param name="stateList">States that you want to add.</param>
        public static void AddStates(List<State> stateList)
        {
            foreach (State state in stateList)
                StateList.Add(state);
        }
    }
}
