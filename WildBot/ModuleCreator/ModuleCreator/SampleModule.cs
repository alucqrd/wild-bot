﻿// Module informations.
// Name: SampleModule.
// Comment: Sample Module Comments.
// Version: 1.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WildBot;
using WildBot.Modules;
using WildBot.AI;
using WildBot.GameObjects;
using WildBot.Tools;
using WildBot.GUI;

namespace WildBot.Script
{
    public class SampleModule : Module
    {
        // Default enum list from State class :
        // public enum Priorities { IDLING, SELECTING_TARGET, MOVING, TRAVELING, LOOTING, ATTACKING, HEALING, RESTING, RESURRECT }

        /// <summary>
        /// This function will be runned on each AI loop cycle.
        /// </summary>
        public override void Run()
        {
            // Load game and player informations
            GameBaseObject gameBase = BotBrain.GameBase;
            MainPlayerEntity player = BotBrain.GameBase.Player;

            log.LogConsole("Hello Nexus!");
        }
    }
}