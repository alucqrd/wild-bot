﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;

namespace WildBot.Tools
{
    public static class KeyboardHook
    {
        #region APIs
        [StructLayout(LayoutKind.Sequential)]
        private struct LASTINPUTINFO
        {
            private static readonly int SizeOf = Marshal.SizeOf(typeof(LASTINPUTINFO));

            [MarshalAs(UnmanagedType.U4)]
            public int cbSize;
            [MarshalAs(UnmanagedType.U4)]
            public int dwTime;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct KBDLLHOOKSTRUCT
        {
            public Keys key;
            public int scanCode;
            public int flags;
            public int time;
            public IntPtr extra;
        }

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int id, LowLevelKeyboardProc callback, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool UnhookWindowsHookEx(IntPtr hook);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hook, int nCode, IntPtr wp, IntPtr lp);

        [DllImport("user32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr GetModuleHandle(string name);
        #endregion

        public delegate void KeyCaptureHandler(object sender, KeyCaptureEventArgs e);
        public static event KeyCaptureHandler OnKeyCapture;


        private static LowLevelKeyboardProc objKeyboardProcess;
        private static IntPtr ptrHook;

        public static void CreateHook()
        {
            ProcessModule objCurrentModule = Process.GetCurrentProcess().MainModule;
            objKeyboardProcess = new LowLevelKeyboardProc(KeyCapture);
            ptrHook = SetWindowsHookEx(13, objKeyboardProcess, GetModuleHandle(objCurrentModule.ModuleName), 0);
        }

        public static void DeleteHook()
        {
            if (ptrHook != IntPtr.Zero)
            {
                UnhookWindowsHookEx(ptrHook);
                ptrHook = IntPtr.Zero;
            }
        }

        private static IntPtr KeyCapture(int nCode, IntPtr wp, IntPtr lp)
        {
            if (OnKeyCapture != null)
            {
                if (nCode >= 0)
                {
                    KBDLLHOOKSTRUCT objKeyInfo = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lp, typeof(KBDLLHOOKSTRUCT));

                    KeyCaptureEventArgs e = new KeyCaptureEventArgs();

                    e.key       = objKeyInfo.key;
                    e.extra     = objKeyInfo.extra;
                    e.flags     = objKeyInfo.flags;
                    e.scanCode  = objKeyInfo.scanCode;
                    e.time      = objKeyInfo.time;

                    OnKeyCapture(null, e);
                }
            }

            return CallNextHookEx(ptrHook, nCode, wp, lp);
        }
    }

    public class KeyCaptureEventArgs
    {
        public Keys key;
        public IntPtr extra;
        public int flags;
        public int scanCode;
        public int time;
    }
}
