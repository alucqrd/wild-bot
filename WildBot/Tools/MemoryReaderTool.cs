﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

// This memory-reading based on Apoc's code (most of the code and idea), others from BlackSun
// http://www.ownedcore.com/forums/world-of-warcraft/world-of-warcraft-bots-programs/wow-memory-editing/248226-c-win32-memory-reading-class-example.html#post1615549

namespace WildBot.Tools
{
    /// <summary>
    /// Generic memory reading tool. Located in WildBot static class.
    /// </summary>
    public class MemoryReaderTool : IDisposable
    {
        #region API
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        internal static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        internal static extern bool ReadProcessMemory(IntPtr handle, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out  long lpNumberOfBytesRead);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        internal static extern bool WriteProcessMemory(IntPtr handle, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out long lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        internal static extern int CloseHandle(IntPtr hObject);
        #endregion

        #region Variables
        /// <summary>
        /// Holds the handle of the process
        /// </summary>
        public IntPtr ProcessHandle { get; private set; }

        /// <summary>
        /// Holds the ID of the process
        /// </summary>
        public int PID { get; private set; }
        #endregion

        #region Construction & Destruction
        /// <summary>
        /// Constructor for empty memory reader
        /// </summary>
        public MemoryReaderTool()
        {
            ProcessHandle = IntPtr.Zero;
            PID = 0;
        }

        /// <summary>
        /// Constructor that will automatically open the given process
        /// </summary>
        /// <param name="processID"></param>
        public MemoryReaderTool(int processID)
        {
            Open(processID);
        }

        public void Dispose()
        {
            if (ProcessHandle != IntPtr.Zero)
                CloseHandle(ProcessHandle);

            GC.SuppressFinalize(this);
        }

        ~MemoryReaderTool()
        {
            Dispose();
        }
        #endregion

        #region Open & Close & IsOpen & Is64Bit
        /// <summary>
        /// Open process with the specified access level
        /// </summary>
        /// <param name="ProcessName">Name of the process (without extension)</param>
        /// <param name="iDesiredAccess">Access to open with</param>
        /// <param name="bInheritHandle"></param>
        /// <returns>True on success</returns>
        public bool Open(string ProcessName, int iDesiredAccess = 0x1F0FFF, bool bInheritHandle = true)
        {
            try
            {
                Process[] p = Process.GetProcessesByName(ProcessName);

                if (p != null && p.Length > 0)
                    return Open(p.First().Id, iDesiredAccess, bInheritHandle);
            }
            catch { }

            return false;
        }

        /// <summary>
        /// Open process with the specified access level
        /// </summary>
        /// <param name="processID">ID of the process</param>
        /// <param name="iDesiredAccess">Access to open with</param>
        /// <param name="bInheritHandle"></param>
        /// <returns>True on success</returns>
        public bool Open(int processID, int iDesiredAccess = 0x1F0FFF, bool bInheritHandle = true)
        {
            try
            {
                if (processID == 0)
                    return false;

                ProcessHandle = OpenProcess(iDesiredAccess, bInheritHandle, processID);

                if (ProcessHandle == IntPtr.Zero)
                    return false;

                PID = processID;
                Process.EnterDebugMode();

                return true;
            }
            catch { }

            Close();
            return false;
        }

        /// <summary>
        /// Close handle
        /// </summary>
        public void Close()
        {
            try
            {
                if (ProcessHandle != IntPtr.Zero)
                    CloseHandle(ProcessHandle);
            }
            catch { }

            PID = 0;
            ProcessHandle = IntPtr.Zero;
        }

        /// <summary>
        /// Checks if Memory Reader is reading on a process or not
        /// </summary>
        /// <returns>True if any process is open</returns>
        public bool IsOpen()
        {
            try
            {
                if (PID != 0 && ProcessHandle != IntPtr.Zero && Process.GetProcessById(PID) != null)
                    return true;
            }
            catch
            {
                PID = 0;
                ProcessHandle = IntPtr.Zero;
            }

            return false;
        }
        #endregion

        #region Read
        /// <summary>
        /// Read base address of the given module
        /// </summary>
        /// <param name="sModule">Module name</param>
        /// <returns>Address of the module</returns>
        public long GetBaseAddress(string sModule)
        {
            try
            {
                Process p = Process.GetProcessById(PID);

                ProcessModuleCollection pmc = p.Modules;

                foreach (ProcessModule pm in pmc)
                {
                    if (pm.ModuleName == sModule)
                        return (long)pm.BaseAddress;
                }
            }
            catch { }

            return 0;
        }

        /// <summary>
        /// Read bytes from a spcified address
        /// </summary>
        /// <param name="address">Read bytes from this address</param>
        /// <param name="count">Number of bytes to read</param>
        /// <returns>Byte array read from the memory</returns>
        public byte[] ReadBytes(IntPtr address, uint count)
        {
            var buffer = new byte[count];
            long bytesRead;

            try
            {
                ReadProcessMemory(ProcessHandle, address, buffer, count, out bytesRead);
            }
            catch {  }

            return buffer;
        }

        /// <summary>
        /// Read bytes from a spcified address
        /// </summary>
        /// <param name="address">Read bytes from this address</param>
        /// <param name="count">Number of bytes to read</param>
        /// <returns>Byte array read from the memory</returns>
        public byte[] ReadBytes(long address, uint count)
        {
            return ReadBytes(new IntPtr(address), count);
        }

        /// <summary>
        /// Read almost any type from the memory
        /// </summary>
        /// <typeparam name="T">Type what to read</typeparam>
        /// <param name="address">Read from this address</param>
        /// <returns>The data read from the memory</returns>
        public T Read<T>(IntPtr address) where T : struct
        {
            try
            {
                object ret = default(T);
                var buffer = new byte[0];

                // Marshal here, as we want the native byte count!
                buffer = ReadBytes(address, (uint)Marshal.SizeOf(typeof(T)));

                switch (Type.GetTypeCode(typeof(T)))
                {
                    case TypeCode.Object:
                        // Double check this. It *should* work. But may not. Untested
                        GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                        ret = Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
                        handle.Free();
                        break;
                    case TypeCode.Boolean:
                        ret = BitConverter.ToBoolean(buffer, 0);
                        break;
                    case TypeCode.Char:
                        ret = BitConverter.ToChar(buffer, 0);
                        break;
                    case TypeCode.Byte:
                        // Will throw an argument out of range exception if 0 bytes were read.
                        // This is on purpose!
                        ret = buffer[0];
                        break;
                    case TypeCode.Int16:
                        ret = BitConverter.ToInt16(buffer, 0);
                        break;
                    case TypeCode.UInt16:
                        ret = BitConverter.ToUInt16(buffer, 0);
                        break;
                    case TypeCode.Int32:
                        ret = BitConverter.ToInt32(buffer, 0);
                        break;
                    case TypeCode.UInt32:
                        ret = BitConverter.ToUInt32(buffer, 0);
                        break;
                    case TypeCode.Int64:
                        ret = BitConverter.ToInt64(buffer, 0);
                        break;
                    case TypeCode.UInt64:
                        ret = BitConverter.ToUInt64(buffer, 0);
                        break;
                    case TypeCode.Single:
                        ret = BitConverter.ToSingle(buffer, 0);
                        break;
                    case TypeCode.Double:
                        ret = BitConverter.ToDouble(buffer, 0);
                        break;
                    default:
                        throw new NotSupportedException(string.Format("{0} is not currently supported by this function.", typeof(T).FullName));
                }

                return (T)ret;
            }
            catch { }

            return default(T);
        }

        /// <summary>
        /// Read almost any type from the memory
        /// </summary>
        /// <typeparam name="T">Type what to read</typeparam>
        /// <param name="address">Read from this address</param>
        /// <returns>The data read from the memory</returns>
        public T Read<T>(long address) where T : struct
        {
            try
            {
                return Read<T>(new IntPtr(address));
            }
            catch { }

            return default(T);
        }

        /// <summary>
        /// Read almost any type from the memory
        /// </summary>
        /// <typeparam name="T">Type what to read</typeparam>
        /// <param name="baseAddr">Base address to start reading from</param>
        /// <param name="offsets">Array of offsets</param>
        /// <returns>Value that was read from the final address</returns>
        public T Read<T>(long baseAddr, int[] offsets) where T : struct
        {
            try
            {
                int[] ptrOffsets = new int[offsets.Count() - 1];
                for (uint i = 0; i < offsets.Count() - 1; i++)
                    ptrOffsets[i] = offsets[i];

                return Read<T>(ReadPointer(baseAddr, ptrOffsets) + offsets.Last());
            }
            catch { }

            return default(T);
        }

        /// <summary>
        /// Sums bytes (Required for StringReader)
        /// </summary>
        /// <param name="bytes">bytes to sum</param>
        /// <returns>Sum of the bytes</returns>
        private int Sum(byte[] bytes)
        {
            int buf = 0;

            foreach (byte b in bytes)
                buf += b;

            return buf;
        }

        /// <summary>
        /// Read string with the specific encoding
        /// </summary>
        /// <param name="address">Address to read from</param>
        /// <param name="encType">Encoding type (eg. Encoding.ASCII)</param>
        /// <returns>The string read from the memory</returns>
        public string ReadString(IntPtr address, Encoding encType, int bytesPerChar = 2)
        {
            try
            {
                // Unknown string size.
                var buffer = new List<byte>();

                int i = 0;
                byte[] current = ReadBytes((long)(address + i), (uint)bytesPerChar);

                while (Sum(current) != 0)
                {
                    foreach (byte b in current)
                        buffer.Add(b);

                    i += bytesPerChar;

                    current = ReadBytes((long)(address + i), (uint)bytesPerChar);
                }
                return encType.GetString(buffer.ToArray());
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Read string with the specific encoding
        /// </summary>
        /// <param name="address">Address to read from</param>
        /// <param name="encType">Encoding type (eg. Encoding.ASCII)</param>
        /// <returns>The string read from the memory</returns>
        public string ReadString(long address, Encoding encType, int bytesPerChar = 2)
        {
            return ReadString(new IntPtr(address), encType, bytesPerChar);
        }

        /// <summary>
        /// Read an ASCII string with 2 bytes / char
        /// </summary>
        /// <param name="address">Address to read from</param>
        /// <returns>The string read from the memory</returns>
        public string ReadString(long address)
        {
            return ReadString(address, Encoding.Unicode, 2);
        }

        /// <summary>
        /// Reads the address from the pointer
        /// </summary>
        /// <param name="baseAddr">Address to read</param>
        /// <returns>The final address</returns>
        public long ReadPointer(long baseAddr)
        {
            try
            {
                long address = baseAddr;
                #if x86
                address = Read<int>(address);
                #else
                address = Read<long>(address);
                #endif

                return address;
            }
            catch { }

            return 0;
        }

        /// <summary>
        /// Reads the address from the pointer
        /// </summary>
        /// <param name="baseAddr">Base address</param>
        /// <param name="offsets">Array of offsets</param>
        /// <returns>The final address</returns>
        public long ReadPointer(long baseAddr, int[] offsets)
        {
            try
            {
                long address = baseAddr;

                for (uint i = 0; i < offsets.Count(); i++)
                {
#if x86
                    address = Read<int>(address + offsets[i]);
#else
                    address = Read<long>(address + offsets[i]);
#endif
                }

                return address;
            }
            catch { }

            return 0;
        }
        #endregion

        #region Write
        /// <summary>
        /// Writes a byte array to the memory
        /// </summary>
        /// <param name="address">Address where to write</param>
        /// <param name="value">Bytes to write</param>
        public void WriteBytes(IntPtr address, byte[] value)
        {
            long bytesWritten;

            try
            {
                WriteProcessMemory(ProcessHandle, address, value, (uint)value.Length, out bytesWritten);
            }
            catch { }
        }

        /// <summary>
        /// Writes a byte array to the memory
        /// </summary>
        /// <param name="address">Address where to write</param>
        /// <param name="value">Bytes to write</param>
        public void WriteBytes(long address, byte[] value)
        {
            WriteBytes(new IntPtr(address), value);
        }

        /// <summary>
        /// Write a specific type to the memory
        /// </summary>
        /// <typeparam name="T">The type to write</typeparam>
        /// <param name="address">Address where to write</param>
        /// <param name="value">Value to write</param>
        public void Write<T>(IntPtr address, dynamic value) where T : struct
        {
            try
            {
                byte[] buf = new byte[0];

                switch (Type.GetTypeCode(typeof(T)))
                {
                    case TypeCode.Boolean:
                        buf = BitConverter.GetBytes((bool)value);
                        break;
                    case TypeCode.Char:
                        buf = BitConverter.GetBytes((char)value);
                        break;
                    case TypeCode.Byte:
                        buf = new byte[1] { (byte)value };
                        break;
                    case TypeCode.Int16:
                        buf = BitConverter.GetBytes((Int16)value);
                        break;
                    case TypeCode.UInt16:
                        buf = BitConverter.GetBytes((UInt16)value);
                        break;
                    case TypeCode.Int32:
                        buf = BitConverter.GetBytes((Int32)value);
                        break;
                    case TypeCode.UInt32:
                        buf = BitConverter.GetBytes((UInt32)value);
                        break;
                    case TypeCode.Int64:
                        buf = BitConverter.GetBytes((Int64)value);
                        break;
                    case TypeCode.UInt64:
                        buf = BitConverter.GetBytes((UInt64)value);
                        break;
                    case TypeCode.Single:
                        buf = BitConverter.GetBytes((Single)value);
                        break;
                    case TypeCode.Double:
                        buf = BitConverter.GetBytes((double)value);
                        break;
                    default:
                        throw new NotSupportedException(string.Format("{0} is not currently supported by this function.", typeof(T).FullName));
                }

                WriteBytes(address, buf);
            }
            catch { }
        }

        /// <summary>
        /// Write a specific type to the memory
        /// </summary>
        /// <typeparam name="T">The type to write</typeparam>
        /// <param name="address">Address where to write</param>
        /// <param name="value">Value to write</param>
        public void Write<T>(long address, dynamic value) where T : struct
        {
            Write<T>(new IntPtr(address), value);
        }

        /// <summary>
        /// Write a specific type to the memory (pointer version)
        /// </summary>
        /// <typeparam name="T">The type to write</typeparam>
        /// <param name="baseAddr">Base address</param>
        /// <param name="offsets">Array of offsets</param>
        /// <param name="value">Value to write</param>
        public void Write<T>(long baseAddr, int[] offsets, dynamic value) where T : struct
        {
            Write<T>(ReadPointer(baseAddr, offsets), value);
        }

        /// <summary>
        /// Writes a string to the memory
        /// </summary>
        /// <param name="address">Address where to write</param>
        /// <param name="value">Value to write</param>
        /// <param name="encType">The encoding</param>
        /// <param name="bytesPerChar">How much byte / character (eg. for Unicode it's 2)</param>
        public void WriteString(IntPtr address, string value, Encoding encType, int bytesPerChar = 2)
        {
            try
            {
                // Retrieve byte array
                byte[] buf = encType.GetBytes(value);

                // Add end of string in the end of array in a new array
                byte[] val = new byte[buf.Length + bytesPerChar];

                for (int i = 0; i < buf.Length; i++)
                    val[i] = buf[i];

                WriteBytes(address, val);
            }
            catch { }
        }

        /// <summary>
        /// Writes a string to the memory
        /// </summary>
        /// <param name="address">Address where to write</param>
        /// <param name="value">Value to write</param>
        /// <param name="encType">The encoding</param>
        /// <param name="bytesPerChar">How much byte / character (eg. for unicode it's 2)</param>
        public void WriteString(long address, string value, Encoding encType, int bytesPerChar = 2)
        {
            WriteString(new IntPtr(address), value, encType, bytesPerChar);
        }

        /// <summary>
        /// Writes a string to the memory with default values (Unicode, 2 bytes / char)
        /// </summary>
        /// <param name="address">Address where to write</param>
        /// <param name="value">Value to write</param>
        public void WriteString(long address, string value)
        {
            WriteString(address, value, Encoding.Unicode, 2);
        }
        #endregion
    }
}