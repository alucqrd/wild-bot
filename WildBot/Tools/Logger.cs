﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WildBot.Tools
{
    /// <summary>
    /// Log handler tool. From this you can log to file or console.
    /// </summary>
    public class Logger
    {
        // Init log function, how to use : log.Error(msg);
        // Output file : Logs\Error.log
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly log4net.ILog log;

        public Logger(Type type)
        {
            log = log4net.LogManager.GetLogger(type);
        }

        /// <summary>
        /// Any message logged with this method will be written in Error.log file and in the console.
        /// </summary>
        /// <param name="msg">Description of the error.</param>
        public void LogError(string msg)
        {
            log.Error(msg);
            WildBotObject.fMain.AddLog(msg, System.Drawing.Color.Red);
        }

        /// <summary>
        /// Log message to the console.
        /// </summary>
        /// <param name="msg">Message.</param>
        public void LogConsole(string msg, Color logColor)
        {
            WildBotObject.fMain.AddLog(msg, logColor);
        }

        /// <summary>
        /// Log a message to the console with the default color (black).
        /// </summary>
        /// <param name="msg">Message.</param>
        public void LogConsole(string msg)
        {
            WildBotObject.fMain.AddLog(msg, Color.Black);
        }
    }
}
