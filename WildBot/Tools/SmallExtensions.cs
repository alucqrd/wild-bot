﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

public class SmallExtensions
{
    /// <summary>
    /// Auto create the directory if needed
    /// </summary>
    /// <param name="path">Path for directory</param>
    /// <returns>Return will be the same as the input</returns>
    public static string InitDirectory(string path)
    {
        string buf = path;

        if (path.IndexOf('\\') >= 0 && path.Split('\\').Last().IndexOf('.') >= 0)
            buf = path.Substring(0, path.LastIndexOf('\\'));

        if (!File.Exists(buf))
            Directory.CreateDirectory(buf);

        return path;
    }
}
