﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace WildBot.Tools
{
    /// <summary>
    /// For obvious purpose, here simple randomized timer just give base duration and randomized duration to constructor to make a randomized duration. 
    /// This class can be used as well for non-randomized timer need (ofc).
    /// </summary>
    public class RandomizedTimer
    {
        private Stopwatch _stopwatch = new Stopwatch();
        public int BaseDuration;
        public int RandomizedDuration;
        public int Duration;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="baseDuration">Base duration that will always be used (in milliseconds).</param>
        /// <param name="randomizedDuration">Added randomized duration (in milliseconds). Default value is 0 if user doesn't want to use randomization.</param>
        public RandomizedTimer(int baseDuration, int randomizedDuration = 0)
        {
            BaseDuration = baseDuration;
            RandomizedDuration = randomizedDuration;
            ResetDuration();
        }

        /// <summary>
        /// Return true if duration elapsed.
        /// </summary>
        /// <returns></returns>
        public bool Elapsed()
        {
            if (_stopwatch.ElapsedMilliseconds > Duration)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Return true if the timer has been started.
        /// </summary>
        /// <returns></returns>
        public bool IsRunning()
        {
            if (_stopwatch.IsRunning)
                return true;
            return false;
        }

        /// <summary>
        /// Restart the timer and compute new randomized duration.
        /// </summary>
        public void Reset()
        {
            _stopwatch.Restart();
            ResetDuration();
        }

        /// <summary>
        /// Stop the timer.
        /// </summary>
        public void Stop()
        {
            _stopwatch.Stop();
        }

        private void ResetDuration()
        {
            Random rnd = new Random();
            Duration = BaseDuration + rnd.Next(0, RandomizedDuration);
        }
    }
}
