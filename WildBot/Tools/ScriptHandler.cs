﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Reflection;

namespace WildBot.Tools
{
    class ScriptHandler
    {
        public static dynamic LoadScript(string file)
        {
            // Prepare script load
            CSharpCodeProvider provider = new CSharpCodeProvider();
            CompilerParameters parameters = new CompilerParameters();

            // Add all default references
            parameters.ReferencedAssemblies.Add("Microsoft.CSharp.dll");
            parameters.ReferencedAssemblies.Add("System.dll");
            parameters.ReferencedAssemblies.Add("System.Core.dll");
            parameters.ReferencedAssemblies.Add("System.Data.dll");
            parameters.ReferencedAssemblies.Add("System.Data.DataSetExtensions.dll");
            parameters.ReferencedAssemblies.Add("System.Deployment.dll");
            parameters.ReferencedAssemblies.Add("System.Drawing.dll");
            parameters.ReferencedAssemblies.Add("System.Windows.Forms.dll");
            parameters.ReferencedAssemblies.Add("System.Xml.dll");
            parameters.ReferencedAssemblies.Add("System.Xml.Linq.dll");
            parameters.ReferencedAssemblies.Add("WildBot.exe");

            // Compile it to memory
            parameters.GenerateInMemory = true;
            parameters.GenerateExecutable = false;

            // Get the file name
            int start = file.LastIndexOf('\\');
            int end = file.LastIndexOf('.');
            string fileName = file.Substring(start + 1, end - start - 1);

            // Compile the script
            CompilerResults result = provider.CompileAssemblyFromFile(parameters, file);

            // If there is an error then make a log about it
            if (result.Errors.HasErrors)
            {
                StreamWriter sw = new StreamWriter(File.Open(SmallExtensions.InitDirectory(Application.StartupPath + @"\..\Logs\" + fileName + ".log"), FileMode.Create));
                foreach (string error in result.Output)
                    sw.WriteLine(error);
                sw.Close();

                return null;
            }

            Assembly assembly = result.CompiledAssembly;
            return assembly.CreateInstance("WildBot.Script." + fileName);
        }
    }
}
