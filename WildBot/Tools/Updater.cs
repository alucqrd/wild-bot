﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Net;
using System.Xml;
using System.Windows.Forms;

using Ionic.Zip;

using WildBot.GUI;

namespace WildBot.Tools
{
    public class Updater
    {
        #region Variables
        private static string Server = @"http://www.wild-bot.net/update";
        private static string VersionFile = @"LatestVersion.xml";
        private static string ChangelogFile = @"Patchnotes.txt";

        private static Thread tDisplay;
        private static Thread tUpdate;
        private static frmUpdateDownload fDownload;
        #endregion

        #region Events / Delegates
        public delegate void UpdateFinishedDelegate(UpdateEventArgs e);
        public static event UpdateFinishedDelegate UpdateFinished;
        #endregion

        #region Public methods
        /// <summary>
        /// Start the update threads, can be stopped by the "EndUpdate" method
        /// </summary>
        public static void BeginUpdate()
        {
            // Check the configurations, if one of them is invalid don't start the update
            if ((Server == null | (Server != null && Server.Length <= 0)) |
                (VersionFile == null | (VersionFile != null && VersionFile.Length <= 0)) |
                (ChangelogFile == null | (ChangelogFile != null && ChangelogFile.Length <= 0)))
                return;

            if (tUpdate != null)
                tUpdate.Abort();

            if (tDisplay != null)
                tDisplay.Abort();

            tUpdate = new Thread(UpdateThread);
            tUpdate.IsBackground = true;
            tUpdate.Start();
        }

        /// <summary>
        /// Stops the update threads
        /// </summary>
        /// <param name="clearTemp">If true then the method will remove the Temp folder</param>
        public static void EndUpdate(bool clearTemp = true)
        {
            // Clean the temp directory if required
            if (clearTemp && Directory.Exists(Application.StartupPath + @"\..\Temp"))
                Directory.Delete(Application.StartupPath + @"\..\Temp", true);

            // Stop the display thread
            if (tDisplay != null)
            {
                tDisplay.Abort();
                tDisplay = null;
            }

            // Stop the Update thread
            if (tUpdate != null)
            {
                tUpdate.Abort();
                tUpdate = null;
            }
        }

        /// <summary>
        /// Gets if the updater is already running
        /// </summary>
        /// <returns>Returns true if the updater already running</returns>
        public static bool IsUpdating()
        {
            if (tUpdate != null && tUpdate.ThreadState == (ThreadState.Background | ThreadState.Running))
                return true;

            return false;
        }
        #endregion

        #region Private methods
        private static string ToPrefix(long size)
        {
            string[] prefixes = { "B", "KB", "MB", "GB", "TB" };
            int prefix = 0;

            float div = size;

            while (div > 1024)
            {
                div /= 1024;
                prefix++;
            }

            return string.Format("{0:0.00}", Math.Round(div, 2)) + " " + prefixes[prefix];
        }

        private static VersionInfo GetUpdateInfo(Version currentVersion)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Server + "/" + VersionFile);

                XmlNode[] nodes = XMLHandler.GetElement(doc, "root");

                if (nodes != null && nodes.Count() > 0)
                {
                    Version v = null;
                    string file = null;

                    foreach (XmlNode node in nodes)
                    {
                        switch (node.Name)
                        {
                            case "File":
                                file = node.InnerText;
                                break;
                            case "Version":
                                v = new Version(node.InnerText);
                                break;
                        }
                    }

                    if (v != null && file != null && v > currentVersion)
                        return new VersionInfo(v, true, file);
                }
            }
            catch { }

            return new VersionInfo(currentVersion, false, "");
        }

        private static string GetChangelog()
        {
            try
            {
                // The return string
                string ret = "";

                // Used in the downloading
                byte[] buf = new byte[8192];

                // Prepare the web page we will be asking for
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Server + "/" + ChangelogFile);

                // Set the timeout to 5 seconds
                request.Timeout = 5000;

                // Execute the request
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                // Read the data through the response stream
                Stream resStream = response.GetResponseStream();

                while (true)
                {
                    // Read the data from the server
                    int numRead = resStream.Read(buf, 0, buf.Length);

                    // Add it to the string
                    if (numRead > 0)
                        // Convert bytes to ASCII and add it to the return string
                        ret += Encoding.ASCII.GetString(buf, 0, numRead);
                    else
                        break;
                }

                return ret;
            }
            catch { }

            return "";
        }

        private static bool DownloadFile(string link, string downloadLocation, frmUpdateDownload downloadDisplay = null)
        {
            try
            {
                // used on each read operation
                byte[] buf = new byte[8192];

                // prepare the web page we will be asking for
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);

                // Set the timeout to 5 seconds
                request.Timeout = 5000;

                // execute the request
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                // we will read data via the response stream
                Stream resStream = response.GetResponseStream();

                // Setup the ProgressBar
                if (downloadDisplay != null)
                {
                    downloadDisplay.UIThread(delegate
                    {
                        downloadDisplay.apbStatus.Minimum = 0;
                        downloadDisplay.apbStatus.Maximum = 1000;
                        downloadDisplay.apbStatus.Value = 0;
                        downloadDisplay.apbStatus.Update();
                    });
                }

                long currentPosition = 0;

                // Create the writer
                FileStream fs = new FileStream(SmallExtensions.InitDirectory(downloadLocation), FileMode.Create);

                // Download the file
                while (true)
                {
                    // Read the bytes from the server
                    int numRead = resStream.Read(buf, 0, buf.Length);
                    
                    // Save the current position
                    currentPosition += numRead;

                    // If there was something then write it to the file
                    if (numRead > 0)
                    {
                        // Write
                        fs.Write(buf, 0, numRead);

                        // Update display if required
                        if (downloadDisplay != null)
                        {
                            downloadDisplay.UIThread(delegate
                            {
                                int val = (int)Math.Round((float)currentPosition / (float)response.ContentLength * 1000F, 0);
                            
                                // Update only on change
                                if (val != downloadDisplay.apbStatus.Value)
                                {
                                    downloadDisplay.apbStatus.Value = val;
                                    downloadDisplay.apbStatus.Display = ToPrefix(currentPosition) + " / " + ToPrefix(response.ContentLength) + " ({3}%)";

                                    downloadDisplay.apbStatus.Update();
                                }
                            });
                        }
                    }
                    else
                    {
                        // End of the file

                        fs.Close();
                        break;
                    }
                }

                // Indicate that the file was downloaded
                return true;
            }
            catch { }

            // There was an error
            return false;
        }

        static void DisplayThread()
        {
            if (fDownload != null)
            {
                fDownload.UIThread(delegate
                {
                    fDownload.Dispose();
                });
            }

            fDownload = new frmUpdateDownload();

            if (fDownload.ShowDialog() == DialogResult.Abort)
                EndUpdate();
        }

        static void UpdateThread()
        {
            // Get newest version info
            VersionInfo vi = GetUpdateInfo(new Version(Application.ProductVersion));

            // If there is a newer version ask the user if he wants to update or not
            if (vi.IsNewer)
            {
                // Create and update the notify window
                frmUpdateNotify fNotify = new frmUpdateNotify();
                fNotify.lCurVer.Text = Application.ProductVersion;
                fNotify.lNewVer.Text = vi.Ver.ToString();
                fNotify.txtChangelog.Text = Updater.GetChangelog();

                // Ask the user if he/she wants to update
                if (fNotify.ShowDialog() == DialogResult.Yes)
                {
                    // Remove the Temp dir first to prevent coping unneeded files
                    if (Directory.Exists(Application.StartupPath + @"\..\Temp"))
                        Directory.Delete(Application.StartupPath + @"\..\Temp", true);

                    // Start the display thread
                    if (tDisplay != null)
                        tDisplay.Abort();

                    tDisplay = new Thread(DisplayThread);
                    tDisplay.IsBackground = true;
                    tDisplay.Start();

                    // Wait until the fDownload is created
                    while (fDownload == null) ;

                    // Try to download the file
                    if (DownloadFile(Server + @"/" + vi.UpdateFile, Application.StartupPath + @"\..\Temp\tmp.zip", fDownload))
                    {
                        // Update the display indicating that we are done
                        if (fDownload != null)
                        {
                            fDownload.UIThread(delegate
                            {
                                fDownload.lStatus.Text = "Download complete";
                                fDownload.apbStatus.Display = "Installing updates...";
                            });
                        }

                        // Unzip the file
                        ZipFile zip = ZipFile.Read(Application.StartupPath + @"\..\Temp\tmp.zip");
                        zip.ExtractAll(Application.StartupPath + @"\..\Temp\", ExtractExistingFileAction.OverwriteSilently);
                        zip.Dispose();

                        // Remove the zip file
                        File.Delete(Application.StartupPath + @"\..\Temp\tmp.zip");

                        // Create the bat file that will update the program and run it
                        string file = "@echo off\r\n";
                        file += "loop:\r\n";
                        file += "ping 1.1.1.1 -n 1 -w 1000\r\n";
                        file += "tasklist /fi \"imagename eq " + Application.ExecutablePath.Substring(Application.ExecutablePath.LastIndexOfAny(new char[] { '\\', '/' }) + 1) + "\" |find \":\" > nul\r\n";
                        file += "if errorlevel 1 goto loop\r\n";
                        file += "SET src=" + Application.StartupPath + "\\..\\Temp" + "\r\n";
                        file += "SET trg=" + Application.StartupPath + "\\..\\" + "\r\n";
                        file += "XCOPY /E /C /I /H /R /Y /Q \"%src%\" \"%trg%\"\r\n";
                        file += "if EXIST \"%src%\" ( rmdir /S /Q \"%src%\" )\r\n";
                        file += "cd \"%trg%\"\r\n";
                        file += "start /D " + "\"" + Application.StartupPath + "\" " + Application.ExecutablePath.Substring(Application.ExecutablePath.LastIndexOfAny(new char[] { '\\', '/' }) + 1) + "\r\n";
                        file += "( del /q /f \"%~f0\" >nul 2>&1 & exit /b 0  )";

                        if (File.Exists(Application.StartupPath + @"\..\update.bat"))
                            File.Delete(Application.StartupPath + @"\..\update.bat");
                        File.WriteAllText(Application.StartupPath + @"\..\update.bat", file);
                        
                        // Run the update helper
                        System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
                        psi.CreateNoWindow = true;
                        psi.FileName = Application.StartupPath + @"\..\update.bat";
                        psi.WorkingDirectory = Application.StartupPath;
                        psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process.Start(psi);

                        // Close all Forms to trigger it's save feature
                        for (int i = 0; i < Application.OpenForms.Count; )
                            Application.OpenForms[i].Close();

                        // Force close the program
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                        return;
                    }
                }
                else if (UpdateFinished != null)
                    UpdateFinished(new UpdateEventArgs(new Version(Application.ProductVersion), true));
            }
            else if (UpdateFinished != null)
                UpdateFinished(new UpdateEventArgs(new Version(Application.ProductVersion), false));

            EndUpdate();
        }
        #endregion
    }

    public class VersionInfo
    {
        Version ver;
        public Version Ver { get { return ver; } }

        bool isnewer;
        public bool IsNewer { get { return isnewer; } }

        string updatefile;
        public string UpdateFile { get { return updatefile; } }

        public VersionInfo(Version ver, bool isnewer, string updatefile)
        {
            this.ver = ver;
            this.isnewer = isnewer;
            this.updatefile = updatefile;
        }
    }

    public class UpdateEventArgs : EventArgs
    {
        public Version NewVersion;
        public bool Canceled;

        public UpdateEventArgs()
        {
        }
        
        public UpdateEventArgs(Version newVersion, bool canceled)
        {
            NewVersion = newVersion;
            Canceled = canceled;
        }
    }
}
