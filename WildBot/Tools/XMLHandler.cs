﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WildBot.Tools
{
    public class XMLHandler
    {
        #region Element
        #region SetElement
        public static XmlNode SetElement(XmlNode parent, string name, object value = null, bool isUnique = true)
        {
            // Create the element
            XmlNode node = parent.OwnerDocument.CreateElement(name);
            if (value != null)
                node.AppendChild(parent.OwnerDocument.CreateTextNode(value.ToString()));

            // If unique try to update the old one
            if (isUnique)
            {
                try
                {
                    foreach (XmlNode xn in parent.ChildNodes)
                    {
                        if (xn.Name == name)
                        {
                            parent.ReplaceChild(node, xn);
                            return node;
                        }
                    }
                }
                catch { }
            }

            // Element not found or not unique
            parent.AppendChild(node);
            return node;
        }

        public static XmlNode SetElement(XmlDocument doc, string parentName, string name, object value = null, bool isUnique = true)
        {
            try
            {
                // Check each node in the document
                foreach (XmlNode xn in doc.ChildNodes)
                {
                    // If node found call the above code
                    if (xn.Name == parentName)
                        return SetElement(xn, name, value, isUnique);
                    else
                    // Node not found, try to find it
                    {
                        XmlNode node = FindElement(xn, parentName);

                        if (node != null)
                            return SetElement(node, name, value, isUnique);
                    }
                }
            }
            catch { }

            // Node not found at all, return null
            return null;
        }
        #endregion

        #region GetElement
        public static XmlNode[] GetElement(XmlNode parent, string name = "")
        {
            List<XmlNode> ret = new List<XmlNode>();

            try
            {
                foreach (XmlNode xn in parent.ChildNodes)
                {
                    if (xn.Name == name | name == "")
                        ret.Add(xn);
                }
            }
            catch { }

            return ret.ToArray();
        }

        public static XmlNode[] GetElement(XmlDocument doc, string parentName, string name = "")
        {
            try
            {
                foreach (XmlNode xn in doc.ChildNodes)
                {
                    if (xn.Name == parentName)
                        return GetElement(xn, name);
                    else
                    {
                        XmlNode node = FindElement(xn, parentName);

                        if (node != null)
                            return GetElement(node, name);
                    }
                }
            }
            catch { }

            return null;
        }
        #endregion

        #region RemoveElement
        public static int RemoveElement(XmlNode parent, string name = "")
        {
            int ret = 0;

            try
            {
                int index = 0;
                while (index < parent.ChildNodes.Count)
                {
                    if (parent.ChildNodes[index].Name == name | name == "")
                    {
                        parent.RemoveChild(parent.ChildNodes[index]);
                        ret++;
                    }
                    else
                        index++;
                }
            }
            catch { }

            return ret;
        }

        public static int RemoveElement(XmlDocument doc, string parentName, string name = "")
        {
            try
            {
                foreach (XmlNode xn in doc.ChildNodes)
                {
                    if (xn.Name == parentName)
                        return RemoveElement(xn, name);
                    else
                    {
                        XmlNode node = FindElement(xn, parentName);

                        if (node != null)
                            return RemoveElement(node, name);
                    }
                }
            }
            catch { }
            
            return 0;
        }
        #endregion
        #endregion

        #region Attribute
        #region SetAttribute
        public static XmlAttribute SetAttribute(XmlNode node, string name, object value = null)
        {
            try
            {
                foreach (XmlAttribute xa in node.Attributes)
                {
                    if (xa.Name == name)
                    {
                        if (value != null)
                            xa.Value = value.ToString();

                        return xa;
                    }
                }
            }
            catch { }

            XmlAttribute attr = node.OwnerDocument.CreateAttribute(name);
            if (value != null)
                attr.Value = value.ToString();
            
            node.Attributes.Append(attr);

            return attr;
        }

        public static XmlAttribute SetAttribute(XmlDocument doc, string parentName, string name, object value = null)
        {
            try
            {
                foreach (XmlNode xn in doc.ChildNodes)
                {
                    if (xn.Name == parentName)
                        return SetAttribute(xn, name, value);
                    else
                    {
                        XmlNode node = FindElement(xn, parentName);

                        if (node != null)
                            return SetAttribute(node, name, value);
                    }
                }
            }
            catch { }

            return null;
        }
        #endregion

        #region GetAttribute
        public static string GetAttribute(XmlNode node, string name)
        {
            try
            {
                foreach (XmlAttribute xa in node.Attributes)
                {
                    if (xa.Name == name)
                        return xa.Value;
                }
            }
            catch { }

            return "";
        }

        public static string GetAttribute(XmlDocument doc, string parentName, string name)
        {
            try
            {
                foreach (XmlNode xn in doc.ChildNodes)
                {
                    if (xn.Name == parentName)
                        return GetAttribute(xn, name);
                    else
                    {
                        XmlNode node = FindElement(xn, parentName);

                        if (node != null)
                            return GetAttribute(node, name);
                    }
                }
            }
            catch { }

            return null;
        }
        #endregion

        #region RemoveAttribute
        public static int RemoveAttribute(XmlNode node, string name = "")
        {
            int ret = 0;

            try
            {
                int index = 0;
                while (index < node.Attributes.Count)
                {
                    if (node.Attributes[index].Name == name | name == "")
                    {
                        node.Attributes.RemoveAt(index);
                        ret++;
                    }
                    else
                        index++;
                }
            }
            catch { }

            return ret;
        }

        public static int RemoveAttribute(XmlDocument doc, string parentName, string name = "")
        {
            try
            {
                foreach (XmlNode xn in doc.ChildNodes)
                {
                    if (xn.Name == parentName)
                        return RemoveAttribute(xn, name);
                    else
                    {
                        XmlNode node = FindElement(xn, parentName);

                        if (node != null)
                            return RemoveAttribute(node, name);
                    }
                }
            }
            catch { }

            return 0;
        }
        #endregion
        #endregion

        #region Private helpers
        private static XmlNode FindElement(XmlNode node, string name)
        {
            try
            {
                foreach (XmlNode xn in node.ChildNodes)
                {
                    if (xn.Name == name)
                        return xn;
                    else
                    {
                        XmlNode n = FindElement(xn, name);

                        if (n != null)
                            return n;
                    }
                }
            }
            catch { }

            return null;
        }
        #endregion
    }

    public class XmlFile
    {
        public string fileName;
        public XmlDocument doc;

        public XmlFile()
        {
        }

        public XmlFile(string fileName, XmlDocument doc)
        {
            this.fileName = fileName;
            this.doc = doc;
        }
    }
}
