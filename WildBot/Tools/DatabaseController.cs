﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;
using System.Security;
using System.Runtime.InteropServices;

namespace WildBot.Tools
{
    public class DatabaseController
    {
        private static SecureString _dbAccessStr = null;

        private static void ExecuteSQLlitesScript(string path)
        {
            if (_dbAccessStr == null)
                InitDatabaseAccessStrng();

            SQLiteConnection m_dbConnection = new SQLiteConnection(SecureStringToString(_dbAccessStr));

            var result = File.ReadAllLines(path).ToArray();

            m_dbConnection.Open();
            using (var transaction = m_dbConnection.BeginTransaction())
            {
                foreach (string script in result)
                {
                    if (script.Trim() == string.Empty) continue;
                    SQLiteCommand command = new SQLiteCommand(script, m_dbConnection);
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
            }
            m_dbConnection.Close();
            return;
        }

        public static DataTable Query(string query)
        {
            if (_dbAccessStr == null)
                InitDatabaseAccessStrng();

            DataTable dt = new DataTable();

            try
            {
                SQLiteConnection m_dbConnection = new SQLiteConnection(SecureStringToString(_dbAccessStr));

                m_dbConnection.Open();
                SQLiteCommand mycommand = new SQLiteCommand(m_dbConnection);
                mycommand.CommandText = query;
                SQLiteDataReader reader = mycommand.ExecuteReader();
                dt.Load(reader);
                reader.Close();
                m_dbConnection.Close();
            }
            catch (Exception fail)
            {
                String error = "The following error has occurred:\n\n";
                error += fail.Message.ToString() + "\n\n";
                MessageBox.Show(error);
            }

            return dt;
        }

        private static String SecureStringToString(SecureString value)
        {
            IntPtr valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(value);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }

        private static void InitDatabaseAccessStrng()
        {
            _dbAccessStr = new SecureString();

            _dbAccessStr.AppendChar('D');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('t');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar(' ');
            _dbAccessStr.AppendChar('S');
            _dbAccessStr.AppendChar('o');
            _dbAccessStr.AppendChar('u');
            _dbAccessStr.AppendChar('r');
            _dbAccessStr.AppendChar('c');
            _dbAccessStr.AppendChar('e');
            _dbAccessStr.AppendChar('=');
            _dbAccessStr.AppendChar('.');
            _dbAccessStr.AppendChar('.');
            _dbAccessStr.AppendChar('\\');
            _dbAccessStr.AppendChar('D');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('t');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('b');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('s');
            _dbAccessStr.AppendChar('e');
            _dbAccessStr.AppendChar('s');
            _dbAccessStr.AppendChar('\\');
            _dbAccessStr.AppendChar('M');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('i');
            _dbAccessStr.AppendChar('n');
            _dbAccessStr.AppendChar('D');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('t');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('b');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('s');
            _dbAccessStr.AppendChar('e');
            _dbAccessStr.AppendChar(';');
            _dbAccessStr.AppendChar('V');
            _dbAccessStr.AppendChar('e');
            _dbAccessStr.AppendChar('r');
            _dbAccessStr.AppendChar('s');
            _dbAccessStr.AppendChar('i');
            _dbAccessStr.AppendChar('o');
            _dbAccessStr.AppendChar('n');
            _dbAccessStr.AppendChar('=');
            _dbAccessStr.AppendChar('3');
            _dbAccessStr.AppendChar(';');
            _dbAccessStr.AppendChar('P');
            _dbAccessStr.AppendChar('a');
            _dbAccessStr.AppendChar('s');
            _dbAccessStr.AppendChar('s');
            _dbAccessStr.AppendChar('w');
            _dbAccessStr.AppendChar('o');
            _dbAccessStr.AppendChar('r');
            _dbAccessStr.AppendChar('d');
            _dbAccessStr.AppendChar('=');

            _dbAccessStr.AppendChar('q');
            _dbAccessStr.AppendChar('s');
            _dbAccessStr.AppendChar('d');
            _dbAccessStr.AppendChar('ç');
            _dbAccessStr.AppendChar('à');
            _dbAccessStr.AppendChar('!');
            _dbAccessStr.AppendChar('é');
            _dbAccessStr.AppendChar('0');
            _dbAccessStr.AppendChar('9');
            _dbAccessStr.AppendChar('^');
            _dbAccessStr.AppendChar('$');
            _dbAccessStr.AppendChar('`');
            _dbAccessStr.AppendChar('ù');
            _dbAccessStr.AppendChar('q');
            _dbAccessStr.AppendChar('s');
        }
    }
}
