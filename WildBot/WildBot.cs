﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

using WildBot.AI;
using WildBot.Tools;
using WildBot.GUI;
using WildBot.GameObjects;
using WildBot.Modules;

using CrashReporterDotNET;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace WildBot
{
    /// <summary>
    /// Object containing bot GUI, Memory Reader tool and XML settings handler.
    /// </summary>
    public static class WildBotObject
    {
        // Variables
        /// <summary>
        /// Memory reader tool instance.
        /// </summary>
        public static MemoryReaderTool MemoryReader;
        
        /// <summary>
        /// Settings file handler.
        /// </summary>
        public static XmlFile xmlSettings;

        // Modules
        /// <summary>
        /// List of GUI windows generated for each module.
        /// </summary>
        public static Dictionary<frmBase, string> ModuleGUIs = new Dictionary<frmBase, string>();
        
        /// <summary>
        /// Loaded modules list.
        /// </summary>
        public static List<Module> ModuleList = new List<Module>();

        // Forms
        /// <summary>
        /// Main Form.
        /// </summary>
        public static frmMain fMain;

        /// <summary>
        /// Radar Form.
        /// </summary>
        public static frmRadar fRadar;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            #if !DEBUG
            Application.ThreadException += ApplicationThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            #endif

            // Init settings required for forms
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Start the updating threads
            Updater.BeginUpdate();

            // Init settings
            xmlSettings = new XmlFile(SmallExtensions.InitDirectory(Application.StartupPath + @"\..\Settings\Settings.xml"), new XmlDocument());

            // Create XML file if it does not exists
            if (!File.Exists(xmlSettings.fileName))
            {
                XmlDeclaration xDec = xmlSettings.doc.CreateXmlDeclaration("1.0", "utf-8", null);

                XmlElement rootNode = xmlSettings.doc.CreateElement("root");
                xmlSettings.doc.InsertAfter(xDec, xmlSettings.doc.DocumentElement);
                xmlSettings.doc.AppendChild(rootNode);
            }
            else
                xmlSettings.doc.Load(xmlSettings.fileName);

            // Init memory reader
            MemoryReader = new MemoryReaderTool();

            // Init keyboard hook
            KeyboardHook.CreateHook();

            // Init forms
            fRadar = new frmRadar();
            fMain = new frmMain();

            // Init the BotBrain
            BotBrain.Initialize();

            // Setup main form as main thread
            Application.Run(fMain);
        }

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            ReportCrash((Exception)unhandledExceptionEventArgs.ExceptionObject);

            // Force close the program
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private static void ApplicationThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ReportCrash(e.Exception);

            // Force close the program
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private static void ReportCrash(Exception exception)
        {
            var reportCrash = new ReportCrash()
            {
                FromEmail = "wildbotcrashreport@gmail.com",
                ToEmail = "wildbotcrashreport@gmail.com",
                SmtpHost = "smtp.gmail.com",
                Port = 587,
                UserName = "wildbotcrashreport@gmail.com",
                Password = "as97sdl05fi",
                EnableSSL = true,
            };

            reportCrash.Send(exception);
        }
    }
}
