﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

using WildBot.AI;
using WildBot.GUI;
using WildBot.GameObjects;
using WildBot.Tools;

namespace WildBot.GameControllers
{
    /// <summary>
    /// Class handling state creation to interract with the game. Methods are used to move character or cast spells. Movements are handled in a separate thread, to make them as smooth and accurate as possible.
    /// </summary>
    public class GameController
    {
        public static WorldPosition CurrentDestination = null;
        public static bool OnlyFace = false;
        public static bool MovingFlag = false;
        public static bool FightingFlag = false;

        // Logger initialization
        private static Logger log = new Logger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static Thread _MoveThread;

        /// <summary>
        /// Start Movement handling thread.
        /// </summary>
        public static void Initialize()
        {
            // Init thread
            _MoveThread = new Thread(MoveLoop);
            _MoveThread.IsBackground = true;
            _MoveThread.Start();
        }

        private static void MoveLoop()
        {
            while (true)
            {
                if (BotBrain.GameBase != null && BotBrain.GameBase.Player != null)
                {
                    MainPlayerEntity player = BotBrain.GameBase.Player;
                    player.UpdateFacingDirection();

                    if (!BotBrain.paused && CurrentDestination != null && !FightingFlag)
                    {
                        // If we are already very close, stop
                        float distance = player.ComputeDistanceFromPosition(CurrentDestination);
                        if (distance < 5)
                        {
                            CurrentDestination = null;
                            OnlyFace = false;
                            MovingFlag = false;
                        }

                        else if (MovingFlag)
                        {
                            string forwardKey = "w";
                            string backwardKey = "s";
                            string turnLeftKey = "a";
                            string turnRightKey = "d";
                            float toleranceAngle = -1;

                            WildBotObject.fMain.UIThread(delegate
                            {
                                forwardKey = WildBotObject.fMain.GetKey("Forward");
                                backwardKey = WildBotObject.fMain.GetKey("Backward");
                                turnLeftKey = WildBotObject.fMain.GetKey("Left");
                                turnRightKey = WildBotObject.fMain.GetKey("Right");

                                toleranceAngle = float.Parse(WildBotObject.fMain.txtFacingAngleTolerance.Text, System.Globalization.CultureInfo.InvariantCulture);
                            });

                            int faceEntityResult = player.IsFacingPosition(CurrentDestination, toleranceAngle);
                            // Go right
                            if (faceEntityResult == 1)
                            {
                                KeySender.Send(turnLeftKey, 2);
                                KeySender.Send(turnRightKey, 1);
                            }
                            else if (faceEntityResult == 2)
                            {
                                KeySender.Send(turnRightKey, 2);
                                KeySender.Send(turnLeftKey, 1);
                            }

                            else if (faceEntityResult == 0 && !OnlyFace)
                            {
                                KeySender.Send(turnLeftKey, 2);
                                KeySender.Send(turnRightKey, 2);
                                KeySender.Send(forwardKey, 1);
                            }

                            // If only face, make sure that forward move key is released.
                            if (OnlyFace)
                                KeySender.Send(forwardKey, 2);

                        }
                    }
                }

                // Sleep to avoid high cpu usage.
                Thread.Sleep(10);
            }
        }

        /// <summary>
        /// Move toward specified position, adds state automatically to Botbrain's list and returns it.
        /// </summary>
        /// <param name="destination">Position where you want to go.</param>
        /// <param name="onlyFace">If you only want to face location without actually moving toward it (ie. in combat).</param>
        /// <returns>Move state generated.</returns>
        public static State MoveTowardPosition(WorldPosition destination, bool onlyFace = false)
        {
            State moveState = new State();
            moveState.AddCustomAction(delegate
            {
                CurrentDestination = destination;
                OnlyFace = onlyFace;
                moveState.MovingFlag = true;
            });
            moveState.Priority = (float)State.Priorities.MOVING;
            BotBrain.AddState(moveState);
            return moveState;
        }

        /// <summary>
        /// Press key to vacuum loot and add state to BotBrain's State list.
        /// </summary>
        /// <returns>Vacuum state created and added to the list (if you want to add custom action or change priority)</returns>
        public static State VacuumLoot()
        {
            string vacuumKey = "v";

            WildBotObject.fMain.UIThread(delegate
            {
                vacuumKey = WildBotObject.fMain.GetKey("Vacuum loot");
            });

            // Press key to vacuum
            State vacuumState = new State();
            vacuumState.AddCustomAction(delegate
            {
                KeySender.ReleaseKeys();
                KeySender.Send(vacuumKey, 0);
            });
            vacuumState.Priority = (float)State.Priorities.LOOTING;
            BotBrain.AddState(vacuumState);

            return vacuumState;
        }

        /// <summary>
        /// Create a State to cast the spell passed as argument, and add it to BotBrain's State list.
        /// </summary>
        /// <param name="spell">Spell to cast.</param>
        /// <param name="sleep">Sleep timer in ms that you want to wait after pressing key.</param>
        /// <returns>Attack State generated.</returns>
        public static State Cast(Spell spell, int sleep = 0)
        {
            State attackState = new State();
            attackState.Sleep = sleep;
            attackState.AddCustomAction(delegate
            {
                KeySender.ReleaseKeys();
                KeySender.Send(spell.KeyBound, 0);
            });
            attackState.Priority =(float)State.Priorities.ATTACKING;
            BotBrain.AddState(attackState);
            return attackState;
        }

        /// <summary>
        /// Press the specified hotkey and release movements hotkeys if they are being pressed. State is automatically added to BotBrain's State list.
        /// </summary>
        /// <param name="hotkey">Hotkey of the skill we want to cast.</param>
        /// <param name="skillTimer">Eventual timer that you would like to be reseted once key has been pressed.</param>
        /// <param name="sleep">Sleep timer in ms that you want to wait after pressing key.</param>
        /// <returns>State generated.</returns>
        public static State PressHotkey(string hotkey, RandomizedTimer skillTimer = null, int sleep = 0)
        {
            State attackState = new State();
            attackState.Sleep = sleep;
            if (skillTimer != null)
                attackState.TimerList.Add(skillTimer);

            attackState.AddCustomAction(delegate
            {
                KeySender.ReleaseKeys();
                KeySender.Send(hotkey, 0);
            });
            attackState.Priority = (float)State.Priorities.ATTACKING;
            BotBrain.AddState(attackState);

            return attackState;
        }

        /// <summary>
        /// Abort movement handling thread.
        /// </summary>
        public static void AbortMoveThread()
        {
            _MoveThread.Abort();
        }
    }
}
