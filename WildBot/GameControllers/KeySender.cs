﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

using WildBot.Tools;

namespace WildBot.GameControllers
{
    /// <summary>
    /// Class used to send keyboard strokes and mouse clicks to game client.
    /// </summary>
    public class KeySender
    {
        /// <summary>
        /// If set to true, bot will send keys in background.
        /// </summary>
        public static bool BackGroundMode = false;
        private static bool _focusLost = false;
        private static Random rnd = new Random();

        private static string GameWindowName = "";
        private static SortedDictionary<string, bool> _keyPressed = new SortedDictionary<string,bool>();

        private static Logger log = new Logger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [DllImport("WBLib.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern void AU3_Send(string key, int nMode);

        [DllImport("WBLib.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern void AU3_MouseClick(string button, int x, int y, int clickNumber, int speed);

        [DllImport("WBLib.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern void AU3_ControlSend(string title, string text, string control, string keys, int nMode);

        [DllImport("WBLib.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern void AU3_ControlClick(string title, string text, string control, string button, int x, int y, int clickNumber, int speed);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        [DllImport("user32.dll")]
        private static extern bool PostMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, Int32 lParam);

        [DllImport("user32.dll")]
        private static extern IntPtr MapVirtualKey(uint uCode, uint uMapType);

        const uint WM_KEYDOWN = 0x0100;
        const uint WM_KEYUP = 0x0101;
        const uint WM_LBUTTONDOWN = 0x0201;
        const uint WM_LBUTTONUP = 0x0202;

        private static string GetGameWindowName()
        {
            if (GameWindowName == "")
            {
                #if x86
                GameWindowName = Process.GetProcessesByName("Wildstar32").First().MainWindowTitle;
                #else
                GameWindowName = Process.GetProcessesByName("Wildstar64").First().MainWindowTitle;
                #endif
            }

            return GameWindowName;
        }

        private static Int32 CreateLParam(uint key, bool extended = false)
        {
            Int32 num = (Int32)MapVirtualKey(key, 0);
            Int32 num2 = 1 | (num << 0x10);
            if (extended)
            {
                num2 |= 0x1000000;
            }
            return num2;
        }

        /// <summary>
        /// Sends mouse clicks to process.
        /// </summary>
        /// <param name="button">Mouse button that you want to send ("left", "right", "middle")</param>
        /// <param name="x">x screen coord where you want to click.</param>
        /// <param name="y">y screen coord where you want to click.</param>
        public static void Click(string button, int x, int y)
        {
            if (BackGroundMode)
                AU3_ControlClick(GetGameWindowName(), "", "", button, x, y, 1, 10);
            else
                AU3_MouseClick(button, x, y, 1, 10);

            /*var proc = Process.GetProcessesByName("WildStar32");
            IntPtr hwnd = proc[0].MainWindowHandle;

            PostMessage(hwnd, (UInt32)WM_LBUTTONDOWN, 0x1, (Int32)((y << 16) | (x & 0xFFFF))); // Code to send mouse click
            PostMessage(hwnd, WM_KEYDOWN, (int)Keys.A, CreateLParam((int)Keys.A, false));   // Code to send Key*/
       }

        /// <summary>
        /// Sends keystrokes to process.
        /// </summary>
        /// <param name="key">Key that you want to send (1 for maintain, 2 for release and 0 to just press).</param>
        /// <param name="mode">1 for maintain, 2 for release and 0 to just press.</param>
        public static void Send(string key, uint mode)
        {
            string cmdSent = "";

            // First check if game was unfocused recently for background mode
            if (BackGroundMode)
            {
                bool gameFocused = IsGameFocused();
                if (gameFocused)
                    _focusLost = false;
                else if (!gameFocused && !_focusLost)
                {
                    _keyPressed.Clear();
                    _focusLost = true;
                }
            }

            // log.LogConsole("KEY SENT : " + key + " mode : " + mode);

            if(mode == 0)
            {
                //cmdSent = "{" + key + " down}";
                //Thread.Sleep(50 + rnd.Next(50));
                //cmdSent = "{" + key + " up}";
                //Thread.Sleep(30);
                cmdSent = "{" + key + "}";
            }

            else if(mode == 1)
            {
                cmdSent = "{" + key + " down}";
                if (!_keyPressed.ContainsKey(key))
                    _keyPressed.Add(key, true);
                else
                    return;
            }

            if (mode == 2)
            {
                cmdSent = "{" + key + " up}";
                if (_keyPressed.ContainsKey(key))
                    _keyPressed.Remove(key);
                else
                    return;
            }

            if(BackGroundMode)
                AU3_ControlSend(GetGameWindowName(), "", "", cmdSent, 0);

            else
            {
                if (IsGameFocused())
                    AU3_Send(cmdSent, 0);
            }
        }

        /// <summary>
        /// Release all key pressed.
        /// </summary>
        public static void ReleaseKeys()
        {
            // Release all key
            while (_keyPressed.Count > 0)
                Send(_keyPressed.ElementAt(0).Key, 2);
        }

        /// <summary>
        /// Returns true if the game is focused.
        /// </summary>
        /// <returns>true if game is focused.</returns>
        public static bool IsGameFocused()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

#if x86
            var procId = Process.GetProcessesByName("Wildstar32").First().Id;
#else
            var procId = Process.GetProcessesByName("Wildstar64").First().Id;
#endif
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return (activeProcId == procId);

        }
    }
}
