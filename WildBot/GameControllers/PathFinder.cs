﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WildBot.GUI;
using WildBot.AI;
using WildBot.GameObjects;

namespace WildBot.GameControllers
{
    /// <summary>
    /// Class used for pathfinding and waypoint travelling computations. Methods automatically generate states and add them to the Finite State Machine, then state is returned if user want to modify it before it's runned.
    /// </summary>
    public static class PathFinder
    {
        /// <summary>
        /// Follow path passed in argument, adds state automatically to Botbrain's list. States generated are stored in travel data.
        /// </summary>
        /// <param name="path">Path to be followed.</param>
        /// <param name="travelData">Object used to keep track of informations needed to travel to position.</param>
        public static State FollowPath(WaypointList path, TravelData travelData)
        {
            MainPlayerEntity player = BotBrain.GameBase.Player;
            List<WorldPosition> waypoints = path.Positions;
            bool circlePath = path.IsCircle;

            if (path.Positions.Count < 1)
                return null;

            if (travelData == null)
                travelData = new TravelData();

            WorldPosition positionToGo = null;
            bool onlyfacePosition = false;
            float toleranceAngle = -1;
            WildBotObject.fMain.UIThread(delegate
            {
                toleranceAngle = float.Parse(WildBotObject.fMain.txtFacingAngleTolerance.Text, System.Globalization.CultureInfo.InvariantCulture);
            });

            // Select closest waypoint
            if (travelData.CurrentIndex == -1)
            {
                // First face closest waypoint
                int index = path.ComputeClosestWaypointFromEntity(player);
                positionToGo = waypoints[index];
                if (player.IsFacingPosition(positionToGo, toleranceAngle) != 0 && player.ComputeDistanceFromPosition(positionToGo) > 5)
                    onlyfacePosition = true;

                // Then run toward it
                else
                {
                    travelData.CurrentIndex = index;
                    positionToGo = waypoints[travelData.CurrentIndex];
                }
            }

            // Test again Current Index cause it may change in previous test
            if (travelData.CurrentIndex != -1)
            {
                // Follow waypoint path
                // If player is very close to current waypoint, go next
                if (player.ComputeDistanceFromPosition(waypoints[travelData.CurrentIndex]) < 5)
                {
                    // If straigth path
                    if (!circlePath)
                    {
                        if (travelData.CurrentIndex > waypoints.Count - 2)
                            travelData.ForwardDirection = false;

                        else if (travelData.CurrentIndex == 0)
                            travelData.ForwardDirection = true;

                        if (travelData.ForwardDirection)
                            travelData.CurrentIndex++;
                        else
                            travelData.CurrentIndex--;

                        // After if we are on first or last waypoint, only face the next one before running again.
                        if ((travelData.CurrentIndex == 1 && travelData.ForwardDirection) || (travelData.CurrentIndex == waypoints.Count - 1 && !travelData.ForwardDirection))
                            if (player.IsFacingPosition(waypoints[travelData.CurrentIndex], toleranceAngle) != 0)
                                onlyfacePosition = true;
                    }

                    // If circle path
                    else
                    {
                        if (travelData.CurrentIndex < waypoints.Count - 2)
                            travelData.CurrentIndex++;
                        else
                            travelData.CurrentIndex = 0;
                    }
                }

                positionToGo = waypoints[travelData.CurrentIndex];
            }

            State moveState = GameController.MoveTowardPosition(positionToGo, onlyfacePosition);
            // Add tab sent if needed
            if (StatusInformerObject.SelectTargetTimer.Elapsed() || !StatusInformerObject.SelectTargetTimer.IsRunning())
            {
                moveState.AddCustomAction(delegate
                {
                    KeySender.Send("TAB", 0);
                });
                moveState.TimerList.Add(StatusInformerObject.SelectTargetTimer);
            }


            travelData.AddState(moveState);

            return moveState;
        }

        /// <summary>
        /// Go to a waypoint by following the path, adds state automatically to Botbrain's list. States generated are stored in travel data.
        /// </summary>
        /// <param name="path">Path we want to follow.</param>
        /// <param name="positionIndex">Index of the position in path we want to reach.</param>
        /// <param name="travelData">Object used to keep track of informations needed to travel to waypoint.</param>
        /// <returns>true if we reached position, false if not.</returns>
        public static bool ReachWaypointFromPath(WaypointList path, int positionIndex, TravelData travelData)
        {
            MainPlayerEntity player = BotBrain.GameBase.Player;

            // Select closest waypoint from path
            if (travelData.CurrentIndex == -1)
            {
                // Compute closest waypoint
                travelData.CurrentIndex = path.ComputeClosestWaypointFromEntity(player);
            }

            if (travelData.CurrentIndex <= positionIndex)
                travelData.ForwardDirection = true;

            else if (travelData.CurrentIndex > positionIndex)
                travelData.ForwardDirection = false;

            // If player is very close to current waypoint, go next
            float wpdistance = player.ComputeDistanceFromPosition(path.Positions[travelData.CurrentIndex]);
            if (wpdistance < 5 && travelData.CurrentIndex != positionIndex)
            {
                if (travelData.ForwardDirection)
                    travelData.CurrentIndex++;
                else
                    travelData.CurrentIndex--;
            }

            // If we reached destination return true
            else if (wpdistance < 10 && travelData.CurrentIndex == positionIndex)
            {
                return true;
            }

            State moveState = GameController.MoveTowardPosition(path.Positions[travelData.CurrentIndex]);

            travelData.AddState(moveState);

            return false;
        }
    }
}
